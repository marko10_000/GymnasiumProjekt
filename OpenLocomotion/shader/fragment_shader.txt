#version 330 core

uniform vec4 objectColor;
uniform sampler2D objectTexture;

in vec4 vert_color;
in vec2 vert_uv;

void main(void)
{
	gl_FragColor = texture2D(objectTexture, vert_uv) * vert_color * objectColor;
}

