#version 330 core

uniform vec4 objectColor;

void main(void)
{
	gl_FragColor = objectColor;
}

