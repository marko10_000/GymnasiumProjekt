import game

class Texture():
    id = int
    
    def __init__(self, id):
        self.id = id
    
    def __del__(self):
        game.textureRemove(self.id)
    
    def getSize(self):
        return game.textureGetSize(self.id)

class ScreenElement():
    id = int
    texture = Texture
    
    def __init__(self, id, texture):
        self.id = id
        self.texture = texture
    
    def __del__(self):
        game.screen_elementRemove(self.id)
    
    def call_back(self):
        print("No call back.")

class Font():
    id = int
    
    def __init__(self, id):
        self.id = id
    
    def __del__(self):
        game.fontRemove(self.id)
    
    def renderText(self, text:str, size:int):
        return Texture(game.fontRenderText(self.id, text, size))

class Model():
    id = int
    
    def __init__(self, id):
        self.id = id
    
    def __del__(self):
        game.modelRemove(id)

class Resource():
    id = int
    name = str
    value = int
    
    def __init__(self, id, name, value):
        self.id = id
        self.name = name
        self.value = value
    
    def __del__(self):
        game.resourceRemove(self.id)

class Company():
    id = int
    
    def __init__(self, id):
        self.id = id
    
    def __del__(self):
        game.companyRemove(id)

class Train():
    id = int
    
    def __init__(self, id):
        self.id = id
    
    def __del__(self):
        game.trainRemove(id)

def loadFont(data:str):
    id = game.fontLoad(data)
    if not id == None:
        return Font(id)
    else:
        return None

def loadTexture(data:str):
    id = game.textureLoad(data)
    if id == None:
        return None
    else:
        return Texture(id)

def createScreenElement(x:int, y:int, width:int, height:int, texture:Texture):
    back = ScreenElement(0, texture)
    back.id = game.screen_elementCreate(x, y, width, height, texture.id, back)
    return back

def createResource(name:str, value:int):
    back = game.resourceCreate(name, value)
    if back == None:
        return None
    return Resource(back, name, value)

def createCompany(name:str, produce:Resource, texture:Texture, pmin:int, pmax:int):
    print("x2")
    back = game.companyCreate(texture.id, produce.id, pmin, pmax, name)
    print("x1")
    if back == None:
        return None
    return Company(id)

def loadModel(data:str):
    back = game.modelLoad(data)
    if back == None:
        return None
    else:
        return Model(back)

def createTrain(model:Model, texture:Texture, name:str, cost:int, speed:int, capacity:int):
    back = game.trainCreate(model.id, texture.id, name, cost, speed, capacity)
    return Train(back)