import game
import tools

kohle = tools.createResource("Kohle", 15)
eisen = tools.createResource("Eisen", 25)
nichts = tools.createResource("", 0)

firmen = []
textures = []
models = []
zuege = []

def game_inited():
    pass

def startGame(self):
    game.callbackGameInited(game_inited)
    startGame2(1, 1)

def startGame2(x:int, y:int):
    print("Start Game: " + str(x * 10) + "x" + str(y * 10))
    game.screen_elementRemoveAll()
    game.menuRemoveAll()
    game.runGame()
    game.menuRemoveAll()

def main():
    global start
    
    texture = tools.loadTexture("./data/company/texture1.tga")
    textures.append(texture)
    firmen.append(tools.createCompany("Kohlemine", kohle, texture, 2000, 5000))
    
    texture = tools.loadTexture("./data/company/texture2.tga")
    textures.append(texture)
    firmen.append(tools.createCompany("Eisenerzmine", eisen, texture, 1000, 3000))
    
    texture = tools.loadTexture("./data/company/texture3.tga")
    textures.append(texture)
    firmen.append(tools.createCompany("Stahlwerk", nichts, texture, 100, 300))
    
    model = tools.loadModel("./data/trains/train1/mash.obj")
    models.append(model)
    texture = tools.loadTexture("./data/trains/train1/texture.tga")
    textures.append(texture)
    zuege.append(tools.createTrain(model, texture, "Test 1", 5000, 4, 300))
    
    font = tools.loadFont("./data/fonts/DroidSansMono.ttf")
    start = font.renderText("START TEST", 20)
    
    game.menuInit()
    game.screen_elementInit()
    end = False
    while not end:
        print("Main Menu")
        game.setBasicEvents()
        
        sw, sh = start.getSize()
        screenElement = tools.createScreenElement(100, 100, sw, sh, start)
        screenElement.call_back = startGame
        
        start = False
        while not start:
            game.engineClean()
            game.menuUpdate()
            game.screen_elementUpdate()
            game.world_objectsRender()
            game.engineUpdate()