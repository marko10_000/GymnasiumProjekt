#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#include<crossGL/crossGL.h>

namespace ol
{
    extern void runGame();

    namespace game
    {
        extern void makeOrthoView();
        extern void makeFrustrumView();

        extern void setUnknowKeyCallBack(void(*)(int key, unsigned int action));

        extern void setDeactivate(void(*call_back)());
        extern void deactivateMode();
    }

    namespace map
    {
        static const unsigned char MAP_BLOCK_ALL = 1;
        static const unsigned char MAP_BLOCK_STRIGHT_X = 2;
        static const unsigned char MAP_BLOCK_STRIGHT_Y = 3;
        static const unsigned char MAP_BLOCK_CORVE_0 = 4;
        static const unsigned char MAP_BLOCK_CORVE_90 = 5;
        static const unsigned char MAP_BLOCK_CORVE_180 = 6;
        static const unsigned char MAP_BLOCK_CORVE_270 = 7;
        static const unsigned char MAP_BLOCK_RAIL = 8;
        static const unsigned char MAP_BLOCK_STATION_X = 9;
        static const unsigned char MAP_BLOCK_STATION_Y = 10;

        extern void initMap();

        extern void updateMap();

        extern unsigned char getHeight(unsigned int x, unsigned int y);
        extern void setHeight(unsigned int x, unsigned int y, unsigned char height);

        extern bool getLandscapePos(unsigned int x, unsigned int y, unsigned int* x_, unsigned int* y_);

        extern void setLandscapeClickEvent(void(*call_back)(unsigned int x, unsigned int y));

        extern bool checkIfIsBuilable(unsigned char type, unsigned int x, unsigned int y);
        extern void buildObject(unsigned char type, unsigned int x, unsigned int y);
        extern unsigned char* getObjects(unsigned int x, unsigned int y);
        extern void removeObject(unsigned char type, unsigned int x, unsigned int y);
    }

    namespace world_objects
    {
        class worldObject
        {
            public:
                crossGL::render::Object* object;
                bool menu_object;

                worldObject(crossGL::render::Object* object)
                {
                    this->object = object;
                    this->menu_object = false;
                }

                virtual ~worldObject()
                {
                    if(this->object != nullptr)
                    {
                        delete this->object;
                        this->object = nullptr;
                    }
                }

                virtual void call_back(unsigned int x, unsigned int y) = 0;
        };

        unsigned int addObject(worldObject* object);
        void removeObject(unsigned int id);
        void removeObjectCarefuly(unsigned int id);
        void removeAll();

        void render();

        void callHitObject(unsigned int x, unsigned int y);
    }

    namespace menu
    {
        class menuObject
        {
            public:
                crossGL::render::Texture* texture;

                menuObject(crossGL::render::Texture* texture)
                {
                    this->texture = texture;
                }

                virtual ~menuObject()
                {
                    if(this->texture != nullptr)
                        delete this->texture;
                }

                virtual void call_back(){};
        };

        extern void init();

        extern unsigned int addMenuButton(crossGL::render::Texture* texture, menuObject* obj);

        extern void removeAll();

        extern void update();
        extern void updateWindow(unsigned int x_real, unsigned int y_real, double x, double y);
    }

    namespace rails
    {
        static const unsigned char TYPE_STREIGHT_Z = map::MAP_BLOCK_STRIGHT_X;
        static const unsigned char TYPE_STREIGHT_X = map::MAP_BLOCK_STRIGHT_Y;
        static const unsigned char TYPE_CORVE_0 = map::MAP_BLOCK_CORVE_0;
        static const unsigned char TYPE_CORVE_90 = map::MAP_BLOCK_CORVE_90;
        static const unsigned char TYPE_CORVE_180 = map::MAP_BLOCK_CORVE_180;
        static const unsigned char TYPE_CORVE_270 = map::MAP_BLOCK_CORVE_270;

        extern void init();

        extern unsigned char* getRailsTypes(unsigned int x, unsigned int y);
        extern void setRailsClick(void(*call_back)(unsigned int, unsigned int, unsigned int));

        extern void update();
        extern void draw();
    }

    namespace station
    {
        extern bool isStation(unsigned int x, unsigned int y);

        extern void init();

        extern void update();
        extern void draw();
    }

    namespace money
    {
        extern int getMoney();
        extern void addMoney(int money);
        extern void removeMoney(int money);

        extern void init();

        extern void update(double time);
    }

    namespace screen
    {
        class ScreenObject
        {
            public:
                crossGL::render::Texture* texture;
                unsigned int x;
                unsigned int y;
                unsigned int width;
                unsigned int height;
                unsigned int id;

                ScreenObject(crossGL::render::Texture* texture)
                {
                    this->texture = texture;
                }

                virtual ~ScreenObject(){}

                virtual void call_back(){}
        };

        extern void init();

        extern void resize(unsigned int x, unsigned int y, double left, double top);
        extern void update();

        extern unsigned int addElement(ScreenObject* object);
        extern void removeElement(unsigned int id);
        extern void removeElementCarefully(unsigned int id);
        extern void removeAll();
    }
    namespace train
    {
        class Train
        {
            public:
                crossGL::render::Object* model;
                std::string name;

                unsigned int costs;
                double speed; //fields/10s
                unsigned int capacity;

                Train()
                {
                    this->model = nullptr;
                    this->speed = 0;
                    this->capacity = 100;
                    this->costs = 1000;
                }
                virtual ~Train()
                {
                    if(this->model != nullptr)
                        delete this->model;
                }
        };

        extern void init();

        extern void update(double time);

        extern unsigned int addTrain(Train* train);
        extern void removeTrain(unsigned int id);
    }
    namespace company
    {
        class Resource
        {
            public:
                std::string name;
                double value;
        };
        class Company
        {
            public:
                Resource* produce;
                unsigned int max;
                unsigned int min;
                std::string name;
                crossGL::render::Texture* texture;

                Company()
                {
                    this->texture = nullptr;
                }
                virtual ~Company()
                {
                    delete this->texture;
                }

                virtual bool canSell(Resource* resource)
                {
                    return true;
                }
        };

        extern void init();
        extern void initCompanyMap(unsigned int size);

        extern void update(double time);

        extern unsigned int addCompany(Company* comp);
        extern void removeCompany(Company* comp);

        extern bool trainSell(Resource* resource, unsigned int amount, unsigned int x, unsigned int y);
        extern bool trainBuy(Resource** resource, unsigned int* amount, unsigned int max, unsigned int x, unsigned int y);
    }

    namespace python
    {
        extern void game_inited();
    }
}

#endif // GAME_H_INCLUDED
