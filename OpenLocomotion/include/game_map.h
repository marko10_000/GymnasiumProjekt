#ifndef GAME_MAP_H_INCLUDED
#define GAME_MAP_H_INCLUDED

namespace ol
{
    namespace map
    {
        static const unsigned int MAP_BLOCK_ALL = 0;                    /** Block all */
        static const unsigned int MAP_BLOCK_STRIGHT_Y = 1;              /** Block stright rail */
        static const unsigned int MAP_BLOCK_STRIGHT_X = 2;              /** Block stright rail */
        static const unsigned int MAP_BLOCK_STRIGHT_UP_PLUS_Y = 3;      /** Block up rising rail */
        static const unsigned int MAP_BLOCK_STRIGHT_UP_PLUS_X = 4;      /** Block up rising rail */
        static const unsigned int MAP_BLOCK_STRIGHT_UP_MINUS_Y = 5;     /** Block up rising rail */
        static const unsigned int MAP_BLOCK_STRIGHT_UP_MINUS_X = 6;     /** Block up rising rail */
        static const unsigned int MAP_BLOCK_CORVE_PLUS_X_PLUS_Y = 7;    /** Block corve at this position */
        static const unsigned int MAP_BLOCK_CORVE_PLUS_Y_MINUS_X = 8;   /** Block corve at this position */
        static const unsigned int MAP_BLOCK_CORVE_MINUS_X_MINUS_Y = 9;  /** Block corve at this position */
        static const unsigned int MAP_BLOCK_CORVE_MINUS_Y_PLUS_X = 10;  /** Block corve at this position */

        static const unsigned int MAP_ALLOW_BOTTOM_RAIL = 11;           /** Allow bottom rail */
        static const unsigned int MAP_ALLOW_TOP_RAIL = 12               /** Allow top rail */

        static const unsigned int MAP_BLOCK_STATION_X = 13;             /** Block rails and station */
        static const unsigned int MAP_BLOCK_STATION_Y = 14;             /** Block rails and station */

        /**
         * Looks after the blocks oft objects
         */
        class MapBlockObject
        {
            protected:
                unsigned int x;                 /** The x size */
                unsigned int y;                 /** The y size */
                unsigned int** z;               /** The z sizes */
                unsigned int**** blocks;        /** The blocks container */
                unsigned int*** blocks_length;  /** The blocks container length */

            public:
                /**
                 * Constructor
                 */
                MapBlockObject(unsigned int x, unsigned int y)
                {
                    this->x = x;
                    this->y = y;
                    this->z = new unsigned int*[x];
                    this->blocks = new unsigned int***[x];
                    this->blocks_length = new unsigned int**[x];
                    for(unsigned int i = 0; i < x; i++)
                    {
                        this->z[i] = new unsigned int[y];
                        this->blocks[i] = new unsigned int**[y];
                        this->blocks_length[i] = new unsigned int*[y];
                        for(unsigned int j = 0; j < y; j++)
                        {
                            this->z[i][j] = 0;
                            this->blocks[i][j] = new unsigned int*[0];
                            this->blocks_length = new unsigned int[0];
                        }
                    }
                }
                /**
                 * Destructor
                 */
                virtual ~MapBlockObject()
                {
                    for(unsigned int i = 0; i < this->x; i++)
                    {
                        for(unsigned int j = 0; j < this->y; j++)
                        {
                            for(unsigned int k = 0; k < this->z[i][j]; k++)
                                delete[] this->blocks[i][j][k];
                            delete[] this->blocks[i][j];
                            delete[] this->blocks_length[i][j];
                        }
                        delete[] this->blocks[i];
                        delete[] this->blocks_length[i];
                        delete[] this->z[i];
                    }
                    delete[] this->blocks;
                    delete[] this->blocks_length;
                    delete[] this->z;
                }

                /**
                 * Check if it can be build
                 * @param[in] x The x position
                 * @param[in] y The y position
                 * @param[in] z The z position
                 * @param[in] object The object that should be tested
                 * @return True when it can be build
                 */
                bool canBuild(unsigned int x, unsigned int y, unsigned int z, MapBlockObject& object)
                {
                    return object._canBuild(x, y, z, this->x, this->y, this->z, this->blocks, this->blocks_length);
                }
                /**
                 * Check if it can be build
                 * @param[in] x The x position
                 * @param[in] y The y position
                 * @param[in] z The z position
                 * @param[in] object The object that should be tested
                 * @return True when it can be build
                 */
                bool canBuild(unsigned int x, unsigned int y, unsigned int z, MapBlockObject* object)
                {
                    return object->_canBuild(x, y, z, this->x, this->y, this->z, this->blocks, this->blocks_length);
                }

                /**
                 * The real check if it can be build
                 * @param[in] posx The x position
                 * @param[in] posy The y position
                 * @param[in] posz The z position
                 * @param[in] x The x size
                 * @param[in] y The y size
                 * @param[in] z The z sizes
                 * @param[in] blocks The block containers
                 * @param[in] blocks_length The block containers length
                 * @return True when it can be build, false when not
                 */
                bool _canBuild(unsigned int posx, unsigned int posy, unsigned int posz, int x, unsigned int y, unsigned int** z, unsigned int**** blocks, unsigned int*** blocks_length)
                {
                    // Check if it is in the area
                    if((posx => this->x) || (posy => this->y))
                        return false;
                    if((posx + x > this->x) || (posy + y > this->y))
                        return false;

                    unsigned int tmp;
                    unsigned int tmp2;
                    // Check fields
                    for(unsigned int i = 0; i < x; i++)
                        for(unsigned int j = 0; j < y; j++)
                            for(unsigned int k = 0; k < z[i][j]; k++)
                                for(unsigned int l = 0; l < blocks_length[i][j][k]; l++)
                                {
                                    // Check if z exists
                                    if(posz + k => this->z[i + posx][j + posy])
                                        continue;

                                    // Check if self blocking
                                    tmp = blocks[i][j][k][l];
                                    if(((tmp == MAP_BLOCK_ALL) || (tmp == MAP_BLOCK_STATION_X) || (tmp == MAP_BLOCK_STATION_Y)) && (this->blocks_length[i + posx][j + posy][k + posz] != 0))
                                        return false;

                                    // Check entries
                                    for(unsigned int m = 0; m < this->blocks_length[i + posx][j + posy][k + posz]; m++)
                                    {
                                        // Check if build is allowed
                                        tmp2 = this->blocks[i + posx][j + posy][k + posz][m];
                                        if((tmp2 == MAP_BLOCK_ALL) || (tmp2 == MAP_BLOCK_STATION_X) || (tmp2 == MAP_BLOCK_STATION_Y))
                                            return false;
                                        // Check top and bottom lines
                                        if(((tmp2 == MAP_ALLOW_TOP_RAIL) && (tmp == MAP_ALLOW_BOTTOM_RAIL)) || ((tmp2 == MAP_ALLOW_BOTTOM_RAIL) && (tmp == MAP_ALLOW_TOP_RAIL)))
                                            return false;
                                        // Check if it is a rail type
                                        if(tmp2 <= 10)
                                            if(tmp2 == tmp)
                                                return false;
                                    }
                                }
                }
            return true;
        };
    }
}

#endif // GAME_MAP_H_INCLUDED
