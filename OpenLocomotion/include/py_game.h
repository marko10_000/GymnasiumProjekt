#ifndef PY_GAME_H_INCLUDED
#define PY_GAME_H_INCLUDED

#include <Python.h>

namespace ol
{
    namespace python
    {
        extern PyObject* pySetBasicEvents(PyObject* self, PyObject* args);

        extern PyObject* pyRunGame(PyObject* self, PyObject* args);

        extern PyObject* pyInitMenu(PyObject* self, PyObject* args);
        extern PyObject* pyUpdateMenu(PyObject* self, PyObject* args);
        extern PyObject* pyRemoveAllMenu(PyObject* self, PyObject* args);

        extern PyObject* pyInitWoldObjects(PyObject* self, PyObject* args);
        extern PyObject* pyRenderWorldObjects(PyObject* self, PyObject* args);
        extern PyObject* pyRemoveAllWoldObjects(PyObject* self, PyObject* args);

        extern PyObject* pyEngineClean(PyObject* self, PyObject* args);
        extern PyObject* pyEngineUpdate(PyObject* self, PyObject* args);

        extern PyObject* pyLoadTexture(PyObject* self, PyObject* args);
        extern PyObject* pyGetSizeTexture(PyObject* self, PyObject* args);
        extern PyObject* pyRemoveTexture(PyObject* self, PyObject* args);

        extern PyObject* pyLoadFont(PyObject* self, PyObject* args);
        extern PyObject* pyRenderTextFont(PyObject* self, PyObject* args);
        extern PyObject* pyRemoveFont(PyObject* self, PyObject* args);

        extern PyObject* pyLoadModel(PyObject* self, PyObject* args);
        extern PyObject* pyRemoveModel(PyObject* self, PyObject* args);

        extern PyObject* pyInitScreenElement(PyObject* self, PyObject* args);
        extern PyObject* pyCreateScreenElement(PyObject* self, PyObject* args);
        extern PyObject* pyUpdateScreenElement(PyObject* self, PyObject* args);
        extern PyObject* pyRemoveScreenElement(PyObject* self, PyObject* args);
        extern PyObject* pyRemoveAllScreenElement(PyObject* self, PyObject* args);

        extern PyObject* pyCreateResouceType(PyObject* self, PyObject* args);
        extern PyObject* pyRemoveResouceType(PyObject* self, PyObject* args);

        extern PyObject* pyCreateCompany(PyObject* self, PyObject* args);
        extern PyObject* pyRemoveCompany(PyObject* self, PyObject* args);

        extern PyObject* pyCreateTrain(PyObject* self, PyObject* args);
        extern PyObject* pyRemoveTrain(PyObject* self, PyObject* args);

        extern PyObject* pySetCallBackGameInited(PyObject* self, PyObject* args);
    }
}

#endif // PY_GAME_H_INCLUDED
