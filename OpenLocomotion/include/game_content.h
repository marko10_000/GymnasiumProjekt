#ifndef GAME_CONTENT_H_INCLUDED
#define GAME_CONTENT_H_INCLUDED

#include <string>
#include <crossGL/crossGL.h>
#include <cmath>

namespace ol
{
    namespace content
    {
        /**
         * A material in the game
         */
        class Material
        {
            public:
                std::string name;               /** Name of the material */
                std::string transportType;      /** Type of the material */
                double maxPrice;                /** The maximal price of the material per t */
                double priceFalling;            /** The price fall per day in percent */

                /**
                 * Constructor of the class and set vars
                 * @param[in] name The name of the material
                 * @param[in] transportType The type of material
                 * @param[in] maxPrice The maximal price of the material per t
                 * @param[in] priceFalling The price fall of the material per day
                 */
                Material(std::string name, std::string transportType, double maxPrice, double priceFalling)
                {
                    this->name = name;
                    this->transportType = transportType;
                    this->maxPrice = maxPrice;
                    this->priceFalling = priceFalling;
                }
                /**
                 * Destructor
                 */
                virtual ~Material(){}
        };

        /**
         * A t of material that will be transported in the game
         */
        class TransportedMaterial
        {
            public:
                Material* material; /** The material type */
                double age;         /** The age of the element */

                /**
                 * Constructor
                 * @param[in] material The material that is contained
                 */
                TransportedMaterial(Material* material)
                {
                    this->material = material;
                    this->age = 0;
                }
                /**
                 * Destructor
                 */
                virtual ~TransportedMaterial(){}

                /**
                 * Check if the material is to old and has no value.
                 * @return True when should be destroyed
                 */
                virtual bool noValue()
                {
                    return pow(1 - this->material->priceFalling, this->age) * this->material->maxPrice < 1;
                }
        };

        /**
         * A train station
         */
        class TrainStation
        {
            protected:
                bool blockEcho;                 /** Blocks a call when an echo should be happen */

            public:
                TransportedMaterial** goods;    /** The stored goods in this station */
                unsigned int goods_max;         /** The maximal amount of goods in this station */

                unsigned int x;                 /** The x position of the station */
                unsigned int y;                 /** The y position of the station */
                unsigned int z;                 /** The z position of the station */
                unsigned int id;                /** The id of the station */

                /**
                 * Constructor
                 */
                TrainStation(unsigned int x, unsigned int y, unsigned id)
                {
                    this->goods = new TransportedMaterial*[200];
                    this->goods_max = 200;

                    this->x = x;
                    this->y = y;
                    this->id = id;
                }
                /**
                 * Destructor
                 */
                virtual ~TrainStation(){}
        };
    }
}

#endif // GAME_CONTENT_H_INCLUDED
