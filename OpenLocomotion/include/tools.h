#ifndef TOOLS_H_INCLUDED
#define TOOLS_H_INCLUDED

#include <crossGL/crossGL.h>

namespace tools
{
    template<class t>
    class IDManager
    {
        protected:
            unsigned int length;
            t** objects;
            unsigned int* freeIDs;
            unsigned int freeIDsLength;

        public:
            class IDManagerIterator
            {
                protected:
                    t** elements;
                    unsigned int length;
                    int corrent;

                public:
                    IDManagerIterator(t** elements, unsigned int length)
                    {
                        this->elements = elements;
                        this->length = length;
                        this->corrent = -1;
                    }
                    virtual ~IDManagerIterator(){}

                    virtual bool isDetectable()
                    {
                        return !(this->corrent >= this->length);
                    }

                    virtual t* next()
                    {
                        this->corrent++;
                        while((this->corrent < this->length) && (this->elements[this->corrent] == nullptr))
                            this->corrent++;
                        if(this->corrent < this->length)
                            return this->elements[this->corrent];
                        return nullptr;
                    }
            };

            IDManager()
            {
                this->length = 0;
                this->objects = new t*[0];
                this->freeIDs = new unsigned int[0];
                this->freeIDsLength = 0;
            }
            virtual ~IDManager()
            {
                for(unsigned int i = 0; i < this->length; i++)
                    if(this->objects[i] != nullptr)
                        delete this->objects[i];
                delete[] this->objects;
                delete[] this->freeIDs;
            }

            virtual void dropElement(unsigned int id)
            {
                delete this->objects[id];
                this->objects[id] = nullptr;

                unsigned int* freeIDs2 = new unsigned int[this->freeIDsLength + 1];
                for(unsigned int i = 0; i < this->freeIDsLength; i++)
                    freeIDs2[i] = this->freeIDs[i];
                freeIDs2[this->freeIDsLength] = id;
                this->freeIDsLength++;
                delete[] this->freeIDs;
                this->freeIDs = freeIDs2;
            }
            virtual void dropElement(t* element)
            {
                for(unsigned int i = 0; i < this->length; i++)
                    if(this->objects[i] == element)
                    {
                        this->dropElement(i);
                        return;
                    }
            }

            virtual void dropElementCarefully(unsigned int id)
            {
                this->objects[id] = nullptr;

                unsigned int* freeIDs2 = new unsigned int[this->freeIDsLength + 1];
                for(unsigned int i = 0; i < this->freeIDsLength; i++)
                    freeIDs2[i] = this->freeIDs[i];
                freeIDs2[this->freeIDsLength] = id;
                this->freeIDsLength++;
                delete[] this->freeIDs;
                this->freeIDs = freeIDs2;
            }
            virtual void dropElementCarefully(t* element)
            {
                for(unsigned int i = 0; i < this->length; i++)
                    if(this->objects[i] == element)
                    {
                        this->dropElementCarefully(i);
                        return;
                    }
            }

            virtual t* get(unsigned int id)
            {
                return this->objects[id];
            }

            virtual IDManagerIterator getIterator()
            {
                return IDManagerIterator(this->objects, this->length);
            }

            virtual unsigned int push(t* object)
            {
                if(freeIDsLength == 0)
                {
                    unsigned int back = this->length;
                    this->length++;
                    t** objects2 = new t*[this->length];
                    for(unsigned int i = 0; i < back; i++)
                        objects2[i] = this->objects[i];
                    objects2[back] = object;
                    delete[] this->objects;
                    this->objects = objects2;
                    return back;
                }
                else
                {
                    this->freeIDsLength--;
                    unsigned int back = this->freeIDs[this->freeIDsLength];
                    unsigned int* freeIDs2 = new unsigned int[this->freeIDsLength];
                    for(unsigned int i = 0; i < this->freeIDsLength; i++)
                        freeIDs2[i] = this->freeIDs[i];
                    delete[] this->freeIDs;
                    this->freeIDs = freeIDs2;
                    this->objects[back] = object;
                    return back;
                }
            }

            virtual unsigned int size()
            {
                return this->length - this->freeIDsLength;
            }
    };
}

#endif // TOOLS_H_INCLUDED
