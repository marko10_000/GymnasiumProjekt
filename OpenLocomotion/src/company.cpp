#include <game.h>
#include <tools.h>

using namespace crossGL::render;
using namespace crossGL::imports;
using namespace ol::company;
using namespace tools;

static IDManager<Company>* companies = new IDManager<Company>();

static Object* company_object = nullptr;

class SubCompany : public ol::world_objects::worldObject
{
    public:
        Company* comp;
        unsigned int produce;
        double have;

        SubCompany(Company* comp, Texture* texture) : worldObject(company_object->ownClone())
        {
            this->comp = comp;
            this->object->setTexture(texture);
            this->produce = rand() % (this->comp->max - this->comp->min) + this->comp->min;
            this->have = this->produce;
        }
        virtual ~SubCompany(){}

        virtual void call_back(unsigned int x, unsigned int y){}
};

static IDManager<SubCompany>* reald_companies = new IDManager<SubCompany>();

static SubCompany** company_map = nullptr;
static unsigned int company_map_size = 0;

static Company* comp_build;

static void landscape_click_event(unsigned int x, unsigned int y)
{
    if(ol::map::checkIfIsBuilable(ol::map::MAP_BLOCK_ALL, x, y) &&
       ol::map::checkIfIsBuilable(ol::map::MAP_BLOCK_ALL, x + 1, y) &&
       ol::map::checkIfIsBuilable(ol::map::MAP_BLOCK_ALL, x + 1, y + 1) &&
       ol::map::checkIfIsBuilable(ol::map::MAP_BLOCK_ALL, x, y + 1))
    {
        ol::map::buildObject(ol::map::MAP_BLOCK_ALL, x, y);
        ol::map::buildObject(ol::map::MAP_BLOCK_ALL, x + 1, y);
        ol::map::buildObject(ol::map::MAP_BLOCK_ALL, x + 1, y + 1);
        ol::map::buildObject(ol::map::MAP_BLOCK_ALL, x, y + 1);
        SubCompany* wobj = new SubCompany(comp_build, comp_build->texture);
        wobj->object->pos->x = x + 1;
        wobj->object->pos->z = y + 1;
        ol::world_objects::addObject(wobj);

        company_map[y * company_map_size + x] = wobj;
        company_map[y * company_map_size + x + 1] = wobj;
        company_map[(y + 1) * company_map_size + x] = wobj;
        company_map[(y + 1) * company_map_size + x + 1] = wobj;

        ol::game::deactivateMode();
    }
}

class CompanyClicker : public ol::screen::ScreenObject
{
    public:
        Company* comp;

        CompanyClicker(Texture* text) : ScreenObject(text)
        {
            this->comp = nullptr;
        }
        virtual ~CompanyClicker()
        {
            ol::screen::removeElementCarefully(this->id);
        }

        virtual void call_back()
        {
            comp_build = this->comp;
            ol::map::setLandscapeClickEvent(landscape_click_event);
        }
};

static IDManager<CompanyClicker>* clickers = nullptr;

static void disable()
{
    ol::map::setLandscapeClickEvent(nullptr);
    if(clickers == nullptr)
        return;
    delete clickers;
    clickers = nullptr;
}

static Font* font = nullptr;

class CompanyMenuObject : public ol::menu::menuObject
{
    public:
        CompanyMenuObject(Texture* texture) : menuObject(texture){}
        virtual ~CompanyMenuObject(){}

        virtual void call_back()
        {
            ol::game::setDeactivate(disable);
            IDManager<Company>::IDManagerIterator iter = companies->getIterator();
            unsigned int y = 50;
            clickers = new IDManager<CompanyClicker>();
            for(Company* i = iter.next(); iter.isDetectable(); i = iter.next())
            {
                Texture* text = font->createTexture(i->name, 20);
                text->references++;
                cglCommitTexture(text);
                CompanyClicker* clicker = new CompanyClicker(text);
                clicker->x = 0;
                clicker->y = y;
                clicker->width = text->width;
                clicker->height = text->height;
                clicker->comp = i;
                ol::screen::addElement(clicker);
                clickers->push(clicker);
                y += text->height;
            }
        }
};

void ol::company::init()
{
    Texture* texture = cglImportImage("./data/menu/company.tga");
    cglCommitTexture(texture);
    ol::menu::addMenuButton(texture, new CompanyMenuObject(texture));
    if(font == nullptr)
        font = cglImportTTF("./data/fonts/DroidSansMono.ttf");
    if(company_object == nullptr)
        company_object = new Object(cglCreateRenderObject(cglImportWavefront("data/company/company.obj"), CGL_RENDER_TYPE_POLYGON));
}

void ol::company::update(double time)
{
    time /= 60;
    IDManager<SubCompany>::IDManagerIterator iter = reald_companies->getIterator();
    for(SubCompany* i = iter.next(); iter.isDetectable(); i = iter.next())
    {
        i->have += (double)(i->produce) * time;
        if(i->have > i->produce)
            i->have = i->produce;
    }
}

unsigned int ol::company::addCompany(Company* comp)
{
    return companies->push(comp);
}

void ol::company::removeCompany(Company* comp)
{
    companies->dropElement(comp);
}

void ol::company::initCompanyMap(unsigned int size)
{
    if(company_map != nullptr)
        delete[] company_map;
    unsigned int j = size * size;
    company_map = new SubCompany*[j];
    for(unsigned int i = 0; i < j; i++)
        company_map[i] = nullptr;
    company_map_size = size;
}

bool ol::company::trainBuy(Resource** resource, unsigned int* amount, unsigned int max, unsigned int x, unsigned int y)
{
    if((x >= company_map_size) || (y >= company_map_size))
        return false;
    if(x != 0)
    {
        unsigned int pos = y * company_map_size + x - 1;
        if(company_map[pos] != nullptr)
        {
            SubCompany* comp = company_map[pos];
            if(comp->comp->produce->value != 0)
            {
                if(comp->have >= 1)
                {
                    *(resource) = comp->comp->produce;
                    if(comp->have > max)
                    {
                        *(amount) = max;
                        comp->have -= max;
                    }
                    else
                    {
                        (*amount) = (unsigned int)(comp->have);
                        comp->have -= *(amount);
                    }
                    return true;
                }
            }
        }
    }
    if(y != 0)
    {
        unsigned int pos = (y - 1) * company_map_size + x;
        if(company_map[pos] != nullptr)
        {
            SubCompany* comp = company_map[pos];
            if(comp->comp->produce->value != 0)
            {
                if(comp->have >= 1)
                {
                    *(resource) = comp->comp->produce;
                    if(comp->have > max)
                    {
                        *(amount) = max;
                        comp->have -= max;
                    }
                    else
                    {
                        (*amount) = (unsigned int)(comp->have);
                        comp->have -= *(amount);
                    }
                    return true;
                }
            }
        }
    }
    if(x != company_map_size - 1)
    {
        unsigned int pos = y * company_map_size + x + 1;
        if(company_map[pos] != nullptr)
        {
            SubCompany* comp = company_map[pos];
            if(comp->comp->produce->value != 0)
            {
                if(comp->have >= 1)
                {
                    *(resource) = comp->comp->produce;
                    if(comp->have > max)
                    {
                        *(amount) = max;
                        comp->have -= max;
                    }
                    else
                    {
                        (*amount) = (unsigned int)(comp->have);
                        comp->have -= *(amount);
                    }
                    return true;
                }
            }
        }
    }
    if(y != company_map_size - 1)
    {
        unsigned int pos = (y + 1) * company_map_size + x;
        if(company_map[pos] != nullptr)
        {
            SubCompany* comp = company_map[pos];
            if(comp->comp->produce->value != 0)
            {
                if(comp->have >= 1)
                {
                    *(resource) = comp->comp->produce;
                    if(comp->have > max)
                    {
                        *(amount) = max;
                        comp->have -= max;
                    }
                    else
                    {
                        (*amount) = (unsigned int)(comp->have);
                        comp->have -= *(amount);
                    }
                    return true;
                }
            }
        }
    }
    return false;
}

bool ol::company::trainSell(Resource* resource, unsigned int amount, unsigned int x, unsigned int y)
{
    if((x >= company_map_size) || (y >= company_map_size))
        return false;

    if(x != 0)
    {
        unsigned int pos = y * company_map_size + x - 1;
        if(company_map[pos] != nullptr)
        {
            SubCompany* comp = company_map[pos];
            if(comp->comp->canSell(resource))
            {
                ol::money::addMoney(resource->value * amount);
                return true;
            }
        }
    }
    if(y != 0)
    {
        unsigned int pos = (y - 1) * company_map_size + x;
        if(company_map[pos] != nullptr)
        {
            SubCompany* comp = company_map[pos];
            if(comp->comp->canSell(resource))
            {
                ol::money::addMoney(resource->value * amount);
                return true;
            }
        }
    }
    if(x != company_map_size - 1)
    {
        unsigned int pos = y * company_map_size + x + 1;
        if(company_map[pos] != nullptr)
        {
            SubCompany* comp = company_map[pos];
            if(comp->comp->canSell(resource))
            {
                ol::money::addMoney(resource->value * amount);
                return true;
            }
        }
    }
    if(y != company_map_size - 1)
    {
        unsigned int pos = (y + 1) * company_map_size + x;
        if(company_map[pos] != nullptr)
        {
            SubCompany* comp = company_map[pos];
            if(comp->comp->canSell(resource))
            {
                ol::money::addMoney(resource->value * amount);
                return true;
            }
        }
    }
    return false;
}
