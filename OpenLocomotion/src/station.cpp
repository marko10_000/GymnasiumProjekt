#include <game.h>
#include <tools.h>
#include <crossGL/crossGL.h>

using namespace ol::world_objects;
using namespace ol::menu;
using namespace ol::rails;
using namespace ol::game;
using namespace ol::map;
using namespace crossGL::events;
using namespace crossGL::render;
using namespace crossGL::imports;
using namespace tools;

//static IDManager<TrainStation>* stations = nullptr;

static bool build_active = false;
static Object* station_object = nullptr;

static Texture* menuStation = nullptr;
static Texture* menuStationActive = nullptr;

class StationWorlObject : public worldObject
{
    public:
        unsigned int id;

        StationWorlObject(Object* object) : worldObject(object)
        {
            this->id = 0;
        }
        virtual ~StationWorlObject(){}

        virtual void call_back(unsigned int x, unsigned int y)
        {
            //rail_click(this->x, this->y, this->z);
        }
};

static void station_disable()
{
    build_active = false;
}

inline void build_station(unsigned int x, unsigned int y)
{
    if(checkIfIsBuilable(MAP_BLOCK_STATION_X, x, y))
    {
        buildObject(MAP_BLOCK_STATION_X, x, y);
        StationWorlObject* object = new StationWorlObject(station_object->ownClone());
        object->object->pos->x = (double)x + 0.5f;
        object->object->pos->z = (double)y + 0.5f;
        object->object->pos->y = 0;
        object->object->roat->y = 90;
        object->id = addObject(object);
    }
    else if(checkIfIsBuilable(MAP_BLOCK_STATION_Y, x, y))
    {
        buildObject(MAP_BLOCK_STATION_Y, x, y);
        StationWorlObject* object = new StationWorlObject(station_object->ownClone());
        object->object->pos->x = (double)x + 0.5f;
        object->object->pos->z = (double)y + 0.5f;
        object->object->pos->y = 0;
        object->id = addObject(object);
    }
}

static void map_click_intern(unsigned int x, unsigned int y)
{
    if(build_active)
        build_station(x, y);
}

static void rail_click_intern(unsigned int x, unsigned int y, unsigned int z)
{
    map_click_intern(x, y);
}

class StationMenuButton : public ol::menu::menuObject
{
    public:
        StationMenuButton(Texture* texture) : menuObject(texture){}
        virtual ~StationMenuButton(){}

        virtual void call_back()
        {
            if(!build_active)
            {
                build_active = true;
                deactivateMode();
                setDeactivate(station_disable);
                setRailsClick(rail_click_intern);
                setLandscapeClickEvent(map_click_intern);
            }
            else
            {
                deactivateMode();
            }
        }
};

void ol::station::init()
{
    build_active = false;
    menuStation = cglImportImage("data/menu/station.tga");
    menuStationActive = cglImportImage("data/menu/station_active.tga");
    cglCommitTexture(menuStation);
    cglCommitTexture(menuStationActive);
    addMenuButton(menuStation, new StationMenuButton(menuStation));
    if(station_object == nullptr)
    {
        station_object = new Object(cglCreateRenderObject(cglImportWavefront("data/rails/station/mash.obj") ,CGL_RENDER_TYPE_POLYGON));
        Texture* texture = cglImportImage("data/rails/station/texture.tga");
        cglCommitTexture(texture);
        station_object->setTexture(texture);
    }
}

static bool isBuildAble = false;
static unsigned int buildX = 0, buildY = 0;

void ol::station::update()
{
    if(build_active)
    {
        unsigned int x, y;
        cglGetMousePos(x, y);
        isBuildAble = getLandscapePos(x, y, &buildX, &buildY);
    }
}

void ol::station::draw()
{
    if(build_active)
    {
        Object* obj = nullptr;
        if(checkIfIsBuilable(MAP_BLOCK_STATION_X, buildX, buildY))
        {
            obj = station_object->ownClone();
            obj->pos->x = (double)buildX + 0.5f;
            obj->pos->z = (double)buildY + 0.5f;
            obj->roat->y = 90;
        }
        else if(checkIfIsBuilable(MAP_BLOCK_STATION_Y, buildX, buildY))
        {
            obj = station_object->ownClone();
            obj->pos->x = (double)buildX + 0.5f;
            obj->pos->z = (double)buildY + 0.5f;
        }

        if(obj != nullptr)
        {
            obj->a = 1.0f / 3.0f;
            cglRender(obj);
            delete obj;
        }
    }
}
