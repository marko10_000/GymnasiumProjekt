#include <game.h>

using namespace ol::menu;
using namespace ol::world_objects;
using namespace crossGL::render;
using namespace crossGL::imports;
using namespace crossGL::math;

static menuObject** objects = new menuObject*[0];
static unsigned int length = 0;

static double pix_size_x = 1;
static double pix_size_y = 1;
static double pix_pos_x = 1;
static double pix_pos_y = 1;
static ObjectRender* basic = nullptr;

class MenuWorldObject : public worldObject
{
    public:
        MenuWorldObject** home_pointer;
        unsigned int id;
        unsigned int system_id;
        bool active;

        MenuWorldObject(Object* object, unsigned int sys_id) : worldObject(object)
        {
            this->id = 0;
            this->system_id = sys_id;
            this->menu_object = true;
            this->active = true;
            this->home_pointer = nullptr;
        }
        virtual ~MenuWorldObject()
        {
            if(home_pointer != nullptr)
                (*home_pointer) = nullptr;
        }

        virtual void call_back(unsigned int x, unsigned int y)
        {
            if(this->active)
                objects[this->system_id]->call_back();
        }
};

static MenuWorldObject* menuBar = nullptr;

static MenuWorldObject** world_objects_ = new MenuWorldObject*[0];

void ol::menu::init()
{
    if(basic == nullptr)
    {
        ObjectMash* om_basic = new ObjectMash();
        Polygon* p_basic = new Polygon();
        om_basic->add(p_basic);
        p_basic->add(new Vertex(0, 0, 0, 0, 0));
        p_basic->add(new Vertex(1, 0, 0, 1, 0));
        p_basic->add(new Vertex(1, -1, 0, 1, 1));
        p_basic->add(new Vertex(0, -1, 0, 0, 1));
        basic = cglCreateRenderObject(om_basic, CGL_RENDER_TYPE_POLYGON);
        basic->references++;
    }
    if(menuBar == nullptr)
    {
        Texture* texture = cglImportImage("data/menu/menu_bar.tga");
        texture->repeatX = true;
        cglCommitTexture(texture);
        menuBar = new MenuWorldObject(new Object(basic), 0);
        menuBar->active = false;
        menuBar->object->setTexture(texture);
        menuBar->home_pointer = &menuBar;
        ol::world_objects::addObject(menuBar);
    }
}

unsigned int ol::menu::addMenuButton(Texture* texture, menuObject* menu_obj)
{
    Object* obj = new Object(basic);
    obj->setTexture(texture);

    MenuWorldObject* m_obj = new MenuWorldObject(obj, length);
    m_obj->id = addObject(m_obj);

    menuObject** objects2 = new menuObject*[length + 1];
    MenuWorldObject** world_objects2 = new MenuWorldObject*[length + 1];
    for(unsigned int i = 0; i < length; i++)
    {
        objects2[i] = objects[i];
        world_objects2[i] = world_objects_[i];
    }
    objects2[length] = menu_obj;
    world_objects2[length] = m_obj;
    delete[] objects;
    delete[] world_objects_;
    objects = objects2;
    world_objects_ = world_objects2;
    unsigned int back = length;
    length++;
    return back;
}

void ol::menu::removeAll()
{
    for(unsigned int i = 0; i < length; i++)
    {
        delete objects[i];
        removeObject(world_objects_[i]->id);
        delete world_objects_[i];
    }
    delete[] objects;
    delete[] world_objects_;
    objects = new menuObject*[0];
    world_objects_ = new MenuWorldObject*[0];
    length = 0;
}

void ol::menu::update()
{
    double pix_pos_x2 = pix_pos_x + (5 * pix_size_x);
    for(unsigned int i = 0; i < length; i++)
    {
        world_objects_[i]->object->texture = objects[i]->texture;
        world_objects_[i]->object->pos->x = pix_pos_x2;
        world_objects_[i]->object->pos->y = pix_pos_y;
        world_objects_[i]->object->scale->x = (double)(world_objects_[i]->object->texture->width) * pix_size_x;
        world_objects_[i]->object->scale->y = (double)(world_objects_[i]->object->texture->height) * pix_size_y;
        pix_pos_x2 += (double)(world_objects_[i]->object->texture->width + 5) * pix_size_x;
    }

    menuBar->object->pos->x = pix_pos_x;
    menuBar->object->pos->y = pix_pos_y;
    menuBar->object->scale->x = -2 * pix_pos_x;
    menuBar->object->scale->y = 30 * pix_size_y;
}

void ol::menu::updateWindow(unsigned int x_real, unsigned int y_real, double x, double y)
{
    pix_size_x = (x * 2.0) / (double)(x_real);
    pix_size_y = (y * 2.0) / (double)(y_real);
    pix_pos_x = -x;
    pix_pos_y = y;
}
