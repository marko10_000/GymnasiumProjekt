#include <game.h>
#include <iostream>

using namespace ol::world_objects;
using namespace ol::menu;
using namespace ol::map;
using namespace ol::game;
using namespace ol::rails;
using namespace crossGL::render;
using namespace crossGL::imports;
using namespace crossGL::math;
using namespace crossGL::events;
using namespace std;

static bool rails_enabled = false;
void landscape_click_event(unsigned int x, unsigned int y, unsigned int z);

static void(*rail_click)(unsigned int, unsigned int, unsigned int);

class RailWorlObject : public worldObject
{
    public:
        unsigned int id;
        unsigned int x;
        unsigned int y;
        unsigned int z;

        RailWorlObject(Object* object) : worldObject(object)
        {
            this->id = 0;
        }
        virtual ~RailWorlObject(){}

        virtual void call_back(unsigned int x, unsigned int y)
        {
            rail_click(this->x, this->y, this->z);
        }
};

static RailWorlObject** objects = new RailWorlObject*[0];
static unsigned char* objects_type = new unsigned char[0];
static unsigned int* objects_x = new unsigned int[0];
static unsigned int* objects_y = new unsigned int[0];
static unsigned int* objects_z = new unsigned int[0];
static unsigned int length = 0;

static const unsigned char BUILD_NONE = 0;
static const unsigned char BUILD_DIRECTION_PLUS_X = 1;
static const unsigned char BUILD_DIRECTION_PLUS_Z = 2;
static const unsigned char BUILD_DIRECTION_MINUS_X = 3;
static const unsigned char BUILD_DIRECTION_MINUS_Z = 4;

static unsigned char build_direction = BUILD_DIRECTION_PLUS_X;

static const unsigned char BUILD_NEXT_FRONT = 0;
static const unsigned char BUILD_NEXT_LEFT = 1;
static const unsigned char BUILD_NEXT_RIGHT = 2;

static unsigned char build_next = BUILD_NEXT_FRONT;

static bool build_set = false;
static int build_x = 0;
static int build_y = 0;
static int build_z = 0;

static Object* rail_streight = nullptr;
static Object* rail_corve = nullptr;

bool buildRail(unsigned int x, unsigned int y, unsigned int z, unsigned char type)
{
    if((type == TYPE_STREIGHT_X) || (type == TYPE_STREIGHT_Z))
    {
        if(ol::money::getMoney() >= 125)
            ol::money::removeMoney(125);
        else
            return false;
    }
    else if((type == TYPE_CORVE_0) || (type == TYPE_CORVE_90) || (type == TYPE_CORVE_180) || (type == TYPE_CORVE_270))
    {
        if(ol::money::getMoney() >= 350)
            ol::money::removeMoney(350);
        else
            return false;
    }

    if(!checkIfIsBuilable(type, x, y))
        return false;
    buildObject(type, x, y);

    RailWorlObject* obj = nullptr;
    if(type == TYPE_STREIGHT_X)
        obj = new RailWorlObject(rail_streight->ownClone());
    else if(type == TYPE_STREIGHT_Z)
    {
        obj = new RailWorlObject(rail_streight->ownClone());
        obj->object->roat->y = 90;
    }
    else if(type == TYPE_CORVE_0)
        obj = new RailWorlObject(rail_corve->ownClone());
    else if(type == TYPE_CORVE_90)
    {
        obj = new RailWorlObject(rail_corve->ownClone());
        obj->object->roat->y = 90;
    }
    else if(type == TYPE_CORVE_180)
    {
        obj = new RailWorlObject(rail_corve->ownClone());
        obj->object->roat->y = 180;
    }
    else if(type == TYPE_CORVE_270)
    {
        obj = new RailWorlObject(rail_corve->ownClone());
        obj->object->roat->y = 270;
    }

    if(obj == nullptr)
        return false;
    obj->id = addObject(obj);
    obj->x = x;
    obj->y = y;
    obj->z = z;
    Object* object = obj->object;
    if(type == TYPE_CORVE_0)
    {
        object->pos->x = (double)(x) + 1;
        object->pos->z = (double)(y) + 1;
    }
    else if(type == TYPE_CORVE_90)
    {
        object->pos->x = (double)(x) + 1;
        object->pos->z = (double)(y) + 1;
    }
    else if(type == TYPE_CORVE_180)
    {
        object->pos->x = (double)(x) + 1;
        object->pos->z = (double)(y);
    }
    else if(type == TYPE_CORVE_270)
    {
        object->pos->x = (double)(x);
        object->pos->z = (double)(y) + 1;
    }
    else
    {
        object->pos->x = (double)(x) + 0.5;
        object->pos->z = (double)(y) + 0.5;
    }
    object->pos->y = (double)z;

    RailWorlObject** objects2 = new RailWorlObject*[length + 1];
    unsigned char* objects_type2 = new unsigned char[length + 1];
    unsigned int* objects_x2 = new unsigned int[length + 1];
    unsigned int* objects_y2 = new unsigned int[length + 1];
    unsigned int* objects_z2 = new unsigned int[length + 1];
    for(unsigned int i = 0; i < length; i++)
    {
        objects2[i] = objects[i];
        objects_type2[i] = objects_type[i];
        objects_x2[i] = objects_x[i];
        objects_y2[i] = objects_y[i];
        objects_z2[i] = objects_z[i];
    }
    objects2[length] = obj;
    objects_type2[length] = type;
    objects_x2[length] = x;
    objects_y2[length] = y;
    objects_z2[length] = z;
    delete[] objects;
    delete[] objects_type;
    delete[] objects_x;
    delete[] objects_y;
    delete[] objects_z;
    objects = objects2;
    objects_type = objects_type2;
    objects_x = objects_x2;
    objects_y = objects_y2;
    objects_z = objects_z2;
    length++;

    build_set = true;
    return true;
}

void buildToDirection(int& x, int& y, unsigned char& buildDirection, unsigned char& buildNext)
{
    if(buildDirection == BUILD_DIRECTION_PLUS_X)
    {
        if(buildNext == BUILD_NEXT_FRONT)
        {
            if(buildRail(build_x, build_y, build_z, TYPE_STREIGHT_X))
                x++;
        }
        else if(buildNext == BUILD_NEXT_LEFT)
        {
            if(buildRail(build_x, build_y, build_z, TYPE_CORVE_180))
            {
                x++;
                y -= 2;
                buildDirection = BUILD_DIRECTION_MINUS_Z;
                buildNext = BUILD_NEXT_FRONT;
            }
        }
        else if(buildNext == BUILD_NEXT_RIGHT)
        {
            if(buildRail(build_x, build_y, build_z, TYPE_CORVE_90))
            {
                x++;
                y += 2;
                buildDirection = BUILD_DIRECTION_PLUS_Z;
                buildNext = BUILD_NEXT_FRONT;
            }
        }
    }
    else if(buildDirection == BUILD_DIRECTION_MINUS_Z)
    {
        if(buildNext == BUILD_NEXT_FRONT)
        {
            if(buildRail(build_x, build_y, build_z, TYPE_STREIGHT_Z))
                y--;
        }
        else if(buildNext == BUILD_NEXT_LEFT)
        {
            if(buildRail(build_x - 1, build_y - 1, build_z, TYPE_CORVE_90))
            {
                x -= 2;
                y--;
                buildDirection = BUILD_DIRECTION_MINUS_X;
                buildNext = BUILD_NEXT_FRONT;
            }
        }
        else if(buildNext == BUILD_NEXT_RIGHT)
        {
            if(buildRail(build_x, build_y - 1, build_z, TYPE_CORVE_0))
            {
                x += 2;
                y--;
                buildDirection = BUILD_DIRECTION_PLUS_X;
                buildNext = BUILD_NEXT_FRONT;
            }
        }
    }
    else if(buildDirection == BUILD_DIRECTION_MINUS_X)
    {
        if(buildNext == BUILD_NEXT_FRONT)
        {
            if(buildRail(build_x, build_y, build_z, TYPE_STREIGHT_X))
                x--;
        }
        else if(buildNext == BUILD_NEXT_LEFT)
        {
            if(buildRail(build_x - 1, build_y, build_z, TYPE_CORVE_0))
            {
                x--;
                y += 2;
                buildDirection = BUILD_DIRECTION_PLUS_Z;
                buildNext = BUILD_NEXT_FRONT;
            }
        }
        else if(buildNext == BUILD_NEXT_RIGHT)
        {
            if(buildRail(build_x, build_y - 1, build_z, TYPE_CORVE_270))
            {
                x--;
                y -= 2;
                buildDirection = BUILD_DIRECTION_MINUS_Z;
                buildNext = BUILD_NEXT_FRONT;
            }
        }
    }
    else if(buildDirection == BUILD_DIRECTION_PLUS_Z)
    {
        if(buildNext == BUILD_NEXT_FRONT)
        {
            if(buildRail(build_x, build_y, build_z, TYPE_STREIGHT_Z))
                y++;
        }
        else if(buildNext == BUILD_NEXT_LEFT)
        {
            if(buildRail(build_x + 1, build_y, build_z, TYPE_CORVE_270))
            {
                x += 2;
                y++;
                buildDirection = BUILD_DIRECTION_PLUS_X;
                buildNext = BUILD_NEXT_FRONT;
            }
        }
        else if(buildNext == BUILD_NEXT_RIGHT)
        {
            if(buildRail(build_x - 1, build_y + 1, build_z, TYPE_CORVE_180))
            {
                x -= 2;
                y++;
                buildDirection = BUILD_DIRECTION_MINUS_X;
                buildNext = BUILD_NEXT_FRONT;
            }
        }
    }
}

void landscape_click_event(unsigned int x, unsigned int y, unsigned int z)
{
    build_x = x;
    build_y = y;
    buildToDirection(build_x, build_y, build_direction, build_next);
}

void landscape_click_event(unsigned int x, unsigned int y)
{
    unsigned int z = getHeight(x, y);
    if((z == getHeight(x, y)) && (z == getHeight(x + 1, y)) && (z == getHeight(x + 1, y + 1)) && (z == getHeight(x, y + 1)))
        landscape_click_event(x, y, z);
}

void key_event(int key, unsigned int action)
{
    if(!rails_enabled)
        return;
    if((key == CGL_KEY_R) && (action == CGL_EVENT_KEY_DOWN))
    {
        if(build_direction == BUILD_DIRECTION_PLUS_X)
            build_direction = BUILD_DIRECTION_PLUS_Z;
        else if(build_direction == BUILD_DIRECTION_PLUS_Z)
            build_direction = BUILD_DIRECTION_MINUS_X;
        else if(build_direction == BUILD_DIRECTION_MINUS_X)
            build_direction = BUILD_DIRECTION_MINUS_Z;
        else
            build_direction = BUILD_DIRECTION_PLUS_X;
    }
    else if((key == CGL_KEY_Q) && (action == CGL_EVENT_KEY_DOWN))
    {
        if(build_next == BUILD_NEXT_RIGHT)
            build_next = BUILD_NEXT_FRONT;
        else if(build_next == BUILD_NEXT_FRONT)
            build_next = BUILD_NEXT_LEFT;
    }
    else if((key == CGL_KEY_E) && (action == CGL_EVENT_KEY_DOWN))
    {
        if(build_next == BUILD_NEXT_LEFT)
            build_next = BUILD_NEXT_FRONT;
        else if(build_next == BUILD_NEXT_FRONT)
            build_next = BUILD_NEXT_RIGHT;
    }
    else if((key == CGL_KEY_SPACE) && ((action == CGL_EVENT_KEY_DOWN) || (action == CGL_EVENT_KEY_REPEAT)))
    {
        if(build_set)
            buildToDirection(build_x, build_y, build_direction, build_next);
    }
}

static void rail_click_intern(unsigned int x, unsigned int y, unsigned int z)
{
    if(rails_enabled)
        landscape_click_event(x, y, z);
}

static void rail_disable_call_back()
{
    rails_enabled = false;
    setLandscapeClickEvent(nullptr);
    setUnknowKeyCallBack(nullptr);
}

class RailsMenuObject : public menuObject
{
    public:
        RailsMenuObject(Texture* texture) : menuObject(texture){}
        virtual ~RailsMenuObject(){}

        virtual void call_back()
        {
            if(rails_enabled)
                deactivateMode();
            else
            {
                deactivateMode();
                rails_enabled = true;
                build_set = false;
                build_direction = BUILD_DIRECTION_PLUS_X;
                build_next = BUILD_NEXT_FRONT;
                setLandscapeClickEvent(landscape_click_event);
                setUnknowKeyCallBack(key_event);
                rail_click = rail_click_intern;
                setDeactivate(rail_disable_call_back);
            }
        }
};

void ol::rails::init()
{
    Texture* menu = cglImportImage("data/menu/rails.tga");
    cglCommitTexture(menu);
    addMenuButton(menu, new RailsMenuObject(menu));
    if(rail_streight == nullptr)
    {
        ObjectMash* mash = cglImportWavefront("data/rails/streight/mash.obj");
        rail_streight = new Object(cglCreateRenderObject(mash, CGL_RENDER_TYPE_POLYGON));
        Texture* texture = cglImportImage("data/rails/streight/texture.tga");
        cglCommitTexture(texture);
        rail_streight->setTexture(texture);
    }
    if(rail_corve == nullptr)
    {
        ObjectMash* mash = cglImportWavefront("data/rails/corve/mash.obj");
        rail_corve = new Object(cglCreateRenderObject(mash, CGL_RENDER_TYPE_POLYGON));
        Texture* texture = cglImportImage("data/rails/corve/texture.tga");
        cglCommitTexture(texture);
        rail_corve->setTexture(texture);
    }
}

inline Object* getVirtualBildObject(unsigned char buildDirection, unsigned char buildNext, double x, double y)
{
    Object* object = nullptr;
    if(buildDirection == BUILD_DIRECTION_PLUS_X)
    {
        if(buildNext == BUILD_NEXT_FRONT)
        {
            object = rail_streight->ownClone();
            object->pos->x = x + 0.5;
            object->pos->z = y + 0.5;
            object->pos->y = 0;
            object->a = 0.25;
        }
        else if(buildNext == BUILD_NEXT_LEFT)
        {
            object = rail_corve->ownClone();
            object->pos->x = x + 1;
            object->pos->z = y;
            object->pos->y = 0;
            object->roat->y = 180;
            object->a = 0.25;
        }
        else if(buildNext == BUILD_NEXT_RIGHT)
        {
            object = rail_corve->ownClone();
            object->pos->x = x + 1;
            object->pos->z = y + 1;
            object->pos->y = 0;
            object->roat->y = 90;
            object->a = 0.25;
        }
    }
    else if(buildDirection == BUILD_DIRECTION_MINUS_Z)
    {
        if(buildNext == BUILD_NEXT_FRONT)
        {
            object = rail_streight->ownClone();
            object->pos->x = x + 0.5;
            object->pos->z = y + 0.5;
            object->pos->y = 0;
            object->roat->y = 90;
            object->a = 0.25;
        }
        else if(buildNext == BUILD_NEXT_LEFT)
        {
            object = rail_corve->ownClone();
            object->pos->x = x;
            object->pos->z = y;
            object->pos->y = 0;
            object->roat->y = 90;
            object->a = 0.25;
        }
        else if(buildNext == BUILD_NEXT_RIGHT)
        {
            object = rail_corve->ownClone();
            object->pos->x = x + 1;
            object->pos->z = y;
            object->pos->y = 0;
            object->roat->y = 0;
            object->a = 0.25;
        }
    }
    else if(buildDirection == BUILD_DIRECTION_MINUS_X)
    {
        if(buildNext == BUILD_NEXT_FRONT)
        {
            object = rail_streight->ownClone();
            object->pos->x = x + 0.5;
            object->pos->z = y + 0.5;
            object->pos->y = 0;
            object->a = 0.25;
        }
        else if(buildNext == BUILD_NEXT_LEFT)
        {
            object = rail_corve->ownClone();
            object->pos->x = x;
            object->pos->z = y + 1;
            object->pos->y = 0;
            object->roat->y = 0;
            object->a = 0.25;
        }
        else if(buildNext == BUILD_NEXT_RIGHT)
        {
            object = rail_corve->ownClone();
            object->pos->x = x;
            object->pos->z = y;
            object->pos->y = 0;
            object->roat->y = 270;
            object->a = 0.25;
        }
    }
    else if(buildDirection == BUILD_DIRECTION_PLUS_Z)
    {
        if(buildNext == BUILD_NEXT_FRONT)
        {
            object = rail_streight->ownClone();
            object->pos->x = x + 0.5;
            object->pos->z = y + 0.5;
            object->pos->y = 0;
            object->roat->y = 90;
            object->a = 0.25;
        }
        else if(buildNext == BUILD_NEXT_LEFT)
        {
            object = rail_corve->ownClone();
            object->pos->x = x + 1;
            object->pos->z = y + 1;
            object->pos->y = 0;
            object->roat->y = 270;
            object->a = 0.25;
        }
        else if(buildNext == BUILD_NEXT_RIGHT)
        {
            object = rail_corve->ownClone();
            object->pos->x = x;
            object->pos->z = y + 1;
            object->pos->y = 0;
            object->roat->y = 180;
            object->a = 0.25;
        }
    }
    return object;
}

static unsigned int cursorMapX = 1;
static unsigned int cursorMapY = 1;
static bool cursorMap;

void ol::rails::update()
{
    if(rails_enabled)
    {
        unsigned int x, y;
        cglGetMousePos(x, y);
        cursorMap = getLandscapePos(x, y, &cursorMapX, &cursorMapY);
    }
}

void ol::rails::draw()
{
    if(rails_enabled)
    {
        if(build_set)
        {
            Object* object = getVirtualBildObject(build_direction, build_next, (double)build_x, (double)build_y);
            cglRender(object);
            if(object != nullptr)
                delete object;
        }
        else
        {
            Object* object = nullptr;
            if(cursorMap)
                object = getVirtualBildObject(build_direction, build_next, (double)cursorMapX, (double)cursorMapY);
            cglRender(object);
            if(object != nullptr)
                delete object;
        }
    }
}

unsigned char* ol::rails::getRailsTypes(unsigned int x, unsigned int y)
{
    unsigned int length = 0;
    for(unsigned int i = 0; i < length; i++)
        if((objects_x[i] == x) && (objects_y[i] == y))
            length++;

    unsigned char* back = new unsigned char[length];

    unsigned int pos = 0;
    for(unsigned int i = 0; i < length; i++)
        if((objects_x[i] == x) && (objects_y[i] == y))
        {
            back[pos] = objects_type[i];
            pos++;
        }

    return back;
}

void ol::rails::setRailsClick(void(*call_back)(unsigned int, unsigned int, unsigned int))
{
    rail_click = call_back;
}
