#include <crossGL/crossGL.h>
#include <game.h>
#include <iostream>
#include <math.h>

using namespace crossGL::screen;
using namespace crossGL::events;
using namespace crossGL::render;
using namespace ol::map;
using namespace ol::game;
using namespace ol;
using namespace std;

static const float nears = 0.001f;

static const unsigned int move_front = 1;
static const unsigned int move_back = 2;
static const unsigned int move_left = 4;
static const unsigned int move_right = 8;

static unsigned int moving = 0;
static bool moveing_roat = false;

static bool run;

static void(*unknown_key_call_back)(int key, unsigned int action) = nullptr;
static void(*mode_deactivate)() = nullptr;

static void key_pressed(int key, unsigned int action)
{
    if((key == CGL_KEY_W) && (action == CGL_EVENT_KEY_DOWN))
        moving = moving | move_front;
    else if((key == CGL_KEY_W) && (action == CGL_EVENT_KEY_UP))
        moving = moving & ((unsigned int)(-1) ^ move_front);
    else if((key == CGL_KEY_S) && (action == CGL_EVENT_KEY_DOWN))
        moving = moving | move_back;
    else if((key == CGL_KEY_S) && (action == CGL_EVENT_KEY_UP))
        moving = moving & ((unsigned int)(-1) ^ move_back);
    else if((key == CGL_KEY_A) && (action == CGL_EVENT_KEY_DOWN))
        moving = moving | move_left;
    else if((key == CGL_KEY_A) && (action == CGL_EVENT_KEY_UP))
        moving = moving & ((unsigned int)(-1) ^ move_left);
    else if((key == CGL_KEY_D) && (action == CGL_EVENT_KEY_DOWN))
        moving = moving | move_right;
    else if((key == CGL_KEY_D) && (action == CGL_EVENT_KEY_UP))
        moving = moving & ((unsigned int)(-1) ^ move_right);
    else if(unknown_key_call_back != nullptr)
        unknown_key_call_back(key, action);
}

static void mouse_pressed(unsigned int x, unsigned int y, unsigned int button, unsigned int action)
{
    if((button == CGL_MOUSE_LEFT) && (action == CGL_EVENT_MOUSE_DOWN))
        ol::world_objects::callHitObject(x, y);
    else if((button == CGL_MOUSE_CENTER) && (action == CGL_EVENT_MOUSE_DOWN))
        moveing_roat = true;
    else if((button == CGL_MOUSE_CENTER) && (action == CGL_EVENT_MOUSE_UP))
        moveing_roat = false;
}

static void window_resize(unsigned int x, unsigned y)
{
    cglViewPort(0, 0, x, y);
    makeFrustrumView();
    menu::updateWindow(x, y, (double)x / (double)y, 1);
    screen::resize(x, y, - (double)x / (double)y, 1);
}

double mod(double base, double by)
{
    return base - (floor(base / by) * by);
}

#define MOUSE_ROAT_SLOW 1

static void mouse_move(double x, double y)
{
    if(!moveing_roat)
        return;
    double roat_x, roat_y, roat_z;
    cglWorldGetRoat(roat_x, roat_y, roat_z);
    roat_y = mod(roat_y - (x * MOUSE_ROAT_SLOW) , 360.0);
    roat_x -= y * MOUSE_ROAT_SLOW;
    if(roat_x > 90)
        roat_x = 90;
    else if(roat_x < -90)
        roat_x = -90;
    cglWorldSetRoat(roat_x, roat_y, roat_z);
}

void moveView(double time)
{
    double pos_x, pos_y, pos_z;
    double roat_x, roat_y, roat_z;
    cglWorldGetPos(pos_x, pos_y, pos_z);
    cglWorldGetRoat(roat_x, roat_y, roat_z);

    if(((moving & move_front) != 0) && ((moving & move_back) == 0))
    {
        pos_z -= time * cos(M_PI / 180 * roat_y);
        pos_x -= time * sin(M_PI / 180 * roat_y);
    }
    else if(((moving & move_back) != 0) && ((moving & move_front) == 0))
    {
        pos_z += time * cos(M_PI / 180 * roat_y);
        pos_x += time * sin(M_PI / 180 * roat_y);
    }
    else if(((moving & move_left) != 0) && ((moving & move_right) == 0))
    {
        pos_z += time * cos(M_PI / 180 * (roat_y - 90));
        pos_x += time * sin(M_PI / 180 * (roat_y - 90));
    }
    else if(((moving & move_right) != 0) && ((moving & move_left) == 0))
    {
        pos_z -= time * cos(M_PI / 180 * (roat_y - 90));
        pos_x -= time * sin(M_PI / 180 * (roat_y - 90));
    }

    cglWorldSetPos(pos_x, pos_y, pos_z);
}

void ol::runGame()
{
    run = true;
    world_objects::removeAll();
    initMap();
    cout << "Map created." << endl;
    makeFrustrumView();
    menu::init();
    screen::init();
    money::init();
    money::addMoney(40000);
    rails::init();
    station::init();
    company::init();
    train::init();
    python::game_inited();
    cglEventSetKeys(key_pressed);
    cglEventSetMouse(mouse_pressed);
    cglEventSetMouseMove(mouse_move);
    cglEventSetResizeWindow(window_resize);
    window_resize(800, 600);
    cglWorldSetPos(0, 0.5, 1);
    cglWorldSetScale(0.2f, 0.2f, 0.2f);
    cglWorldSetRoat(0, 0, 0);
    double time = 0;

    while(run)
    {
        cglClean();
        moveView(time);
        updateMap();
        rails::update();
        station::update();
        company::update(time);
        train::update(time);
        menu::update();
        screen::update();
        money::update(time);
        world_objects::render();
        rails::draw();
        station::draw();
        time = cglWindowUpdate();
    }
}

void ol::game::makeOrthoView()
{
    unsigned int x, y;
    cglGetWindowSize(x, y);
    cglViewOrtho((double)x / (double)y, 1, 0, 100);
}

void ol::game::makeFrustrumView()
{
    unsigned int x, y;
    cglGetWindowSize(x, y);
    cglViewFrustum(((double)x / (double)y) * nears, nears, nears, 100);
}

void ol::game::setUnknowKeyCallBack(void(*call_back)(int key, unsigned int action))
{
    unknown_key_call_back = call_back;
}

void ol::game::setDeactivate(void(*call_back)())
{
    if(mode_deactivate != nullptr)
        ol::game::deactivateMode();
    mode_deactivate = call_back;
}

void ol::game::deactivateMode()
{
    if(mode_deactivate != nullptr)
    {
        mode_deactivate();
        mode_deactivate = nullptr;
    }
}
