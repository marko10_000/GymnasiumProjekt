#include <game.h>
#include <crossGL/crossGL.h>
#include <string>
#include <sstream>
#include <iostream>

using namespace ol::money;
using namespace ol::menu;
using namespace crossGL::render;
using namespace crossGL::imports;
using namespace std;

static int money_amount = 0;
static bool money_update = true;
static menuObject* money_menu = nullptr;
static Font* font = nullptr;

void ol::money::init()
{
    money_amount = 0;
    money_update = true;

    if(font == nullptr)
        font = cglImportTTF("./data/fonts/DroidSansMono.ttf");
    if(money_menu == nullptr)
    {
        Texture* texture = new Texture();
        money_menu = new menuObject(texture);
        addMenuButton(texture, money_menu);
    }
}

void ol::money::update(double time)
{
    if(money_update)
    {
        money_update = false;
        delete money_menu->texture;
        stringstream money;
        money << money_amount << " $";
        money_menu->texture = font->createTexture(money.str(), 20);
        money_menu->texture->references++;
        cglCommitTexture(money_menu->texture);
    }
}

int ol::money::getMoney()
{
    return money_amount;
}

void ol::money::addMoney(int money)
{
    money_amount += money;
    money_update = true;
}

void ol::money::removeMoney(int money)
{
    money_amount -= money;
    money_update = true;
}
