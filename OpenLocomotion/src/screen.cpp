#include <game.h>
#include <tools.h>
#include <crossGL/crossGL.h>

using namespace tools;
using namespace crossGL::render;
using namespace ol::screen;

class ScreenWorldObject : public ol::world_objects::worldObject
{
    public:
        unsigned int id;
        ScreenObject* sobject;
        bool carefully;

        ScreenWorldObject(Object* obj, ScreenObject* object) : worldObject(obj)
        {
            this->id = 0;
            this->sobject = object;
            this->menu_object = true;
            this->carefully = false;
        }
        ~ScreenWorldObject()
        {
            if(!this->carefully)
                delete this->sobject;
            ol::world_objects::removeObjectCarefuly(this->id);
        }

        virtual void call_back(unsigned int x, unsigned int y)
        {
            this->sobject->call_back();
        }
};

static ObjectRender* basic = nullptr;
static IDManager<ScreenWorldObject>* objects = nullptr;
static double x_pixel;
static double x_left;
static double y_pixel;
static double y_top;

void ol::screen::init()
{
    if(basic == nullptr)
    {
        ObjectMash* om_basic = new ObjectMash();
        Polygon* p_basic = new Polygon();
        om_basic->add(p_basic);
        p_basic->add(new Vertex(0, 0, 0, 0, 0));
        p_basic->add(new Vertex(1, 0, 0, 1, 0));
        p_basic->add(new Vertex(1, -1, 0, 1, 1));
        p_basic->add(new Vertex(0, -1, 0, 0, 1));
        basic = cglCreateRenderObject(om_basic, CGL_RENDER_TYPE_POLYGON);
        basic->references++;
    }
    if(objects == nullptr)
        objects = new IDManager<ScreenWorldObject>();
}

void ol::screen::resize(unsigned int x, unsigned int y, double left, double top)
{
    x_left = left;
    x_pixel = (left * (-2)) / ((double)x);
    y_top = top;
    y_pixel = (top * 2) / ((double)y);
}
void ol::screen::update()
{
    if(objects == nullptr)
        return;
    IDManager<ScreenWorldObject>::IDManagerIterator iter = objects->getIterator();
    for(ScreenWorldObject* obj = iter.next(); iter.isDetectable(); obj = iter.next())
    {
        obj->object->scale->x = x_pixel * ((double)obj->sobject->width);
        obj->object->scale->y = y_pixel * ((double)obj->sobject->height);
        obj->object->pos->x = x_pixel * ((double)obj->sobject->x) + x_left;
        obj->object->pos->y = -y_pixel * ((double)obj->sobject->y) + y_top;
        obj->object->pos->z = 1;
        obj->object->setTexture(obj->sobject->texture);
    }
}

unsigned int ol::screen::addElement(ScreenObject* object)
{
    ScreenWorldObject* object2 = new ScreenWorldObject(new Object(basic), object);

    object2->object->setTexture(object->texture);
    object2->id = ol::world_objects::addObject(object2);

    object->id = objects->push(object2);
    return object->id;
}
void ol::screen::removeElement(unsigned int id)
{
    objects->dropElement(id);
    ol::world_objects::removeObject(objects->get(id)->id);
}
void ol::screen::removeElementCarefully(unsigned int id)
{
    objects->get(id)->carefully = true;
    objects->dropElement(id);
}
void ol::screen::removeAll()
{
    if(objects != nullptr)
        delete objects;
    objects = new IDManager<ScreenWorldObject>();
}
