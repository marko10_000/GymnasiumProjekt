#include <crossGL/crossGL.h>
#include <game.h>
#include <tools.h>
#include <math.h>

using namespace crossGL::render;
using namespace crossGL::imports;
using namespace tools;
using namespace ol::train;

static const unsigned char TARGET_TYPE_MOVE = 0;
static const unsigned char TARGET_TYPE_STATIN = 1;

static const unsigned char MOVE_NONE = 0;
static const unsigned char MOVE_X_PLUS = 1;
static const unsigned char MOVE_X_MINUS = 2;
static const unsigned char MOVE_Y_PLUS = 4;
static const unsigned char MOVE_Y_MINUS = 8;

class IngameTrain : public ol::world_objects::worldObject
{
    public:
        Train* train;
        unsigned int id;

        unsigned int posx;
        unsigned int posy;
        unsigned char rail_type;
        unsigned char driving_way;
        double done;
        double time;
        unsigned int modus;

        ol::company::Resource* loaded;
        unsigned int loaded_amount;

        IngameTrain(Train* train, unsigned int posx, unsigned int posy, unsigned char rail_type, unsigned char driving_way) : worldObject(train->model->ownClone())
        {
            this->train = train;
            this->posx = posx;
            this->posy = posy;
            this->rail_type = rail_type;
            this->driving_way = driving_way;
            this->done = 0.5;
            this->time = 0;
            this->modus = TARGET_TYPE_MOVE;
            this->loaded = nullptr;
            this->loaded_amount = 0;
        }

        virtual ~IngameTrain()
        {
            ol::world_objects::removeObjectCarefuly(this->id);
        }

        virtual void call_back(unsigned int x, unsigned int y)
        {

        }
};

static Font* font = nullptr;

static IDManager<IngameTrain>* trains = new IDManager<IngameTrain>();

static Train* choosen_train = nullptr;

static IDManager<Train>* trains_buildable = new IDManager<Train>();

static void rail_clicked(unsigned int x, unsigned int y, unsigned int z)
{
    unsigned char* datas = ol::map::getObjects(x, y);
    unsigned char data;
    unsigned int i = 0;
    while(datas[i] != 0)
        if(datas[i] != ol::map::MAP_BLOCK_RAIL)
        {
            data = datas[i];
            break;
        }
    IngameTrain* train = new IngameTrain(choosen_train, x, y, data, MOVE_X_PLUS);
    trains->push(train);
    train->id = ol::world_objects::addObject(train);
    delete[] datas;
}

static void disable2()
{
    ol::rails::setRailsClick(nullptr);
    ol::money::addMoney(choosen_train->costs);
}

class ScreenChooser : public ol::screen::ScreenObject
{
    public:
        Train* train;

        ScreenChooser(Texture* texture) : ScreenObject(texture){}
        virtual ~ScreenChooser()
        {
            ol::screen::removeElementCarefully(this->id);
        }

        virtual void call_back()
        {
            if(ol::money::getMoney() < (int)(this->train->costs))
            {
                ol::game::deactivateMode();
                return;
            }
            choosen_train = this->train;
            ol::money::removeMoney(this->train->costs);
            ol::rails::setRailsClick(rail_clicked);
            ol::game::setDeactivate(disable2);
        }
};

static IDManager<ScreenChooser>* train_choosers = nullptr;

static void disable()
{
    delete train_choosers;
}

class TrainMenuObject : public ol::menu::menuObject
{
    public:
        TrainMenuObject(Texture* texture) : menuObject(texture){}
        virtual ~TrainMenuObject(){}

        virtual void call_back()
        {
            ol::game::setDeactivate(disable);
            IDManager<Train>::IDManagerIterator iter = trains_buildable->getIterator();
            unsigned int y = 50;
            train_choosers = new IDManager<ScreenChooser>();
            for(Train* i = iter.next(); iter.isDetectable(); i = iter.next())
            {
                Texture* text = font->createTexture(i->name, 20);
                text->references++;
                cglCommitTexture(text);
                ScreenChooser* clicker = new ScreenChooser(text);
                clicker->x = 0;
                clicker->y = y;
                clicker->width = text->width;
                clicker->height = text->height;
                clicker->train = i;
                ol::screen::addElement(clicker);
                train_choosers->push(clicker);
                y += text->height;
            }
        }
};

void ol::train::init()
{
    Texture* texture = cglImportImage("./data/menu/train.tga");
    cglCommitTexture(texture);
    ol::menu::addMenuButton(texture, new TrainMenuObject(texture));
    if(font == nullptr)
        font = cglImportTTF("./data/fonts/DroidSansMono.ttf");
}

inline unsigned char canDriveLeft(unsigned int x, unsigned int y)
{
    unsigned char back = 0;
    if(x != 0)
    {
        unsigned char* datas = ol::map::getObjects(x - 1, y);
        unsigned int i = 0;
        while(datas[i] != 0)
        {
            unsigned char data = datas[i];
            if(data == ol::map::MAP_BLOCK_STRIGHT_Y)
                back |= MOVE_X_MINUS;
            i++;
        }
        delete[] datas;
    }
    if((x >= 2) && (y != 0))
    {
        unsigned char* datas = ol::map::getObjects(x - 1, y - 1);
        unsigned int i = 0;
        while(datas[i] != 0)
        {
            unsigned char data = datas[i];
            if(data == ol::map::MAP_BLOCK_CORVE_270)
                back |= MOVE_Y_MINUS;
            i++;
        }
        delete[] datas;
    }
    if(x >= 2)
    {
        unsigned char* datas = ol::map::getObjects(x - 1, y - 1);
        unsigned int i = 0;
        while(datas[i] != 0)
        {
            unsigned char data = datas[i];
            if(data == ol::map::MAP_BLOCK_CORVE_0)
                back |= MOVE_Y_PLUS;
            i++;
        }
        delete[] datas;
    }
    return back;
}

inline unsigned char canDriveRight(unsigned int x, unsigned int y)
{
    unsigned char back = 0;
    if(x != 0)
    {
        unsigned char* datas = ol::map::getObjects(x + 1, y);
        unsigned int i = 0;
        while(datas[i] != 0)
        {
            unsigned char data = datas[i];
            if(data == ol::map::MAP_BLOCK_STRIGHT_Y)
                back |= MOVE_X_PLUS;
            i++;
        }
        delete[] datas;
    }

    unsigned char* datas = ol::map::getObjects(x + 1, y);
    unsigned int i = 0;
    while(datas[i] != 0)
    {
        unsigned char data = datas[i];
        if(data == ol::map::MAP_BLOCK_CORVE_90)
            back |= MOVE_Y_PLUS;
        i++;
    }
    delete[] datas;

    {
        datas = ol::map::getObjects(x + 1, y);
        unsigned int i = 0;
        while(datas[i] != 0)
        {
            unsigned char data = datas[i];
            if(data == ol::map::MAP_BLOCK_CORVE_180)
                back |= MOVE_Y_MINUS;
            i++;
        }
        delete[] datas;
    }
    return back;
}

bool isStation(unsigned int x, unsigned int y)
{
    unsigned char* datas = ol::map::getObjects(x, y);
    unsigned int i = 0;
    while(datas[i] != 0)
    {
        unsigned char data = datas[i];
        if((data == ol::map::MAP_BLOCK_STATION_X) || (data == ol::map::MAP_BLOCK_STATION_Y))
        {
            delete[] datas;
            return true;
        }
        i++;
    }
    delete[] datas;
    return false;
}

void ol::train::update(double time)
{
    time /= 10;
    IDManager<IngameTrain>::IDManagerIterator iter = trains->getIterator();
    for(IngameTrain* train = iter.next(); iter.isDetectable(); train = iter.next())
    {
        // Update Movement
        double used = time + train->time;
        while(used != 0)
        {
            if(train->modus == TARGET_TYPE_MOVE)
            {
                if(train->done == 1)
                {
                    if(train->driving_way == MOVE_X_MINUS)
                    {
                        unsigned char move = 0;
                        if(train->rail_type == ol::map::MAP_BLOCK_STRIGHT_Y)
                            move = canDriveLeft(train->posx, train->posy);

                        if(move == 0)
                        {
                            train->done = 0;
                            if(train->rail_type == ol::map::MAP_BLOCK_CORVE_270)
                                train->driving_way = MOVE_Y_MINUS;
                            else if(train->rail_type == ol::map::MAP_BLOCK_STRIGHT_Y)
                                train->driving_way = MOVE_X_PLUS;
                            else if(train->rail_type == ol::map::MAP_BLOCK_CORVE_0)
                                train->driving_way = MOVE_Y_PLUS;
                        }
                        else if((move & MOVE_X_MINUS) != 0)
                        {
                            train->modus = TARGET_TYPE_STATIN;
                            train->done = 0;
                            if(train->rail_type == ol::map::MAP_BLOCK_STRIGHT_Y)
                                train->posx--;
                        }

                    }
                    else if(train->driving_way == MOVE_X_PLUS)
                    {
                        unsigned char move = 0;
                        if(train->rail_type == ol::map::MAP_BLOCK_STRIGHT_Y)
                            move = canDriveRight(train->posx, train->posy);

                        if(move == 0)
                        {
                            train->done = 0;
                            if(train->rail_type == ol::map::MAP_BLOCK_CORVE_270)
                                train->driving_way = MOVE_Y_MINUS;
                            else if(train->rail_type == ol::map::MAP_BLOCK_STRIGHT_Y)
                                train->driving_way = MOVE_X_MINUS;
                            else if(train->rail_type == ol::map::MAP_BLOCK_CORVE_0)
                                train->driving_way = MOVE_Y_PLUS;
                        }
                        else if((move & MOVE_X_PLUS) != 0)
                        {
                            train->modus = TARGET_TYPE_STATIN;
                            train->done = 0;
                            if(train->rail_type == ol::map::MAP_BLOCK_STRIGHT_Y)
                                train->posx++;
                        }
                    }
                }
                else
                {
                    if((train->rail_type == ol::map::MAP_BLOCK_STRIGHT_X) || (train->rail_type == ol::map::MAP_BLOCK_STRIGHT_Y) || (train->rail_type == ol::map::MAP_BLOCK_STATION_X) || (train->rail_type == ol::map::MAP_BLOCK_STATION_Y))
                    {
                        double tmp = train->done + (train->train->speed * used);
                        if(tmp > 1)
                        {
                            used = (tmp - 1) / train->train->speed;
                            train->done = 1;
                        }
                        else
                        {
                            used = 0;
                            train->done = tmp;
                        }
                    }
                    else if((train->rail_type == ol::map::MAP_BLOCK_CORVE_0) || (train->rail_type == ol::map::MAP_BLOCK_CORVE_90) || (train->rail_type == ol::map::MAP_BLOCK_CORVE_180) || (train->rail_type == ol::map::MAP_BLOCK_CORVE_270))
                    {
                        double tmp = train->done + (train->train->speed * used / 3);
                        if(tmp > 1)
                        {
                            used = (tmp - 1) / train->train->speed * 3;
                            train->done = 1;
                        }
                        else
                        {
                            used = 0;
                            train->done = tmp;
                        }
                    }
                }
            }
            if(train->modus == TARGET_TYPE_STATIN)
            {
                train->modus = TARGET_TYPE_MOVE;
                if(!isStation(train->posx, train->posy))
                    continue;
                if(train->loaded != nullptr)
                {
                    if(ol::company::trainSell(train->loaded, train->loaded_amount, train->posx, train->posy))
                    {
                        train->loaded = nullptr;
                        train->loaded_amount = 0;
                    }
                }
                if(train->loaded == nullptr)
                {
                    ol::company::trainBuy(&(train->loaded), &(train->loaded_amount), train->train->capacity, train->posx, train->posy);
                }
            }
        }
        train->time = used;

        // Update Position
        Object* obj = train->object;
        if(train->rail_type == ol::rails::TYPE_STREIGHT_X)
        {
            // move in x
            if(train->driving_way == MOVE_X_MINUS)
            {
                obj->pos->x = (double)(train->posx) + (1 - train->done);
                obj->roat->y = 0;
            }
            else
            {
                obj->pos->x = (double)(train->posx) + train->done;
                obj->roat->y = 180;
            }
            obj->pos->z = (double)(train->posy) + 0.5;
            obj->pos->y = 0.2;
        }
        else if(train->rail_type == ol::rails::TYPE_STREIGHT_Z)
        {
            // move in y
            if(train->driving_way == MOVE_Y_MINUS)
            {
                obj->pos->y = (double)(train->posy) + (1 - train->done);
                obj->roat->y = 270;
            }
            else
            {
                obj->pos->z = (double)(train->posy) + train->done;
                obj->roat->y = 90;
            }
            obj->pos->x = (double)(train->posx) + 0.5;
            obj->pos->y = 0.2;
        }
        else if(train->rail_type == ol::rails::TYPE_CORVE_0)
        {
            if(train->driving_way == MOVE_Y_PLUS)
            {
                obj->pos->x = (double)(train->posx) + 0.5 + (1.5 * cos(train->done * M_PI_2));
                obj->pos->z = (double)(train->posy) + 0.5 + (1.5 * sin(train->done * M_PI_2));
                obj->roat->y = 90 * (1 - train->done) + 270;
            }
            else // MOVE_X_PLUS
            {
                obj->pos->x = (double)(train->posx) + 0.5 + (1.5 * sin(train->done * M_PI_2));
                obj->pos->z = (double)(train->posy) + 0.5 + (1.5 * cos(train->done * M_PI_2));
                obj->roat->y = 90 * train->done + 90;
            }
            obj->pos->y = 0.2;
        }
        else if(train->rail_type == ol::rails::TYPE_CORVE_90)
        {
            if(train->driving_way == MOVE_X_MINUS)
            {
                obj->pos->x = (double)(train->posx) + (1.5f * cos(train->done * M_PI_2));
                obj->pos->z = (double)(train->posy) + 0.5f + (1.5f * cos(train->done * M_PI_2));
                obj->roat->y = 90 * (1 - train->done) + 180;
            }
            else // MOVE_Y_PLUS
            {
                obj->pos->x = (double)(train->posx) + (1.5f * sin(train->done * M_PI_2));
                obj->pos->z = (double)(train->posy) + 0.5f + (1.5f * sin(train->done * M_PI_2));
                obj->roat->y = 90 * train->done;
            }
            obj->pos->y = 0.2;
        }
        else if(train->rail_type == ol::rails::TYPE_CORVE_180)
        {
            if(train->driving_way == MOVE_Y_MINUS)
            {
                obj->pos->x = (double)(train->posx) + 0.5 + (1.5 * sin(train->done * M_PI_2));
                obj->pos->z = (double)(train->posy) + 0.5 - (1.5 * sin(train->done * M_PI_2));
                obj->roat->y = 90 * (1 - train->done) + 90;
            }
            else // MOVE_X_MINUS
            {
                obj->pos->x = (double)(train->posx) + 0.5 + (1.5 * sin(train->done * M_PI_2));
                obj->pos->z = (double)(train->posy) + 0.5 - (1.5 * sin(train->done * M_PI_2));
                obj->roat->y = 90 * (1 - train->done) + 270;
            }
            obj->pos->y = 0.2;
        }
        else if(train->rail_type == ol::rails::TYPE_CORVE_270)
        {
            if(train->driving_way == MOVE_Y_MINUS)
            {
                obj->pos->x = (double)(train->posx) - (1.5 * sin(train->done * M_PI_2));
                obj->pos->z = (double)(train->posy) - (1.5 * cos(train->done * M_PI_2));
                obj->roat->y = 90 * (1 - train->done) + 180;
            }
            else // MOVE_X_PLUS
            {
                obj->pos->x = (double)(train->posx) - (1.5 * cos(train->done * M_PI_2));
                obj->pos->z = (double)(train->posy) - (1.5 * sin(train->done * M_PI_2));
                obj->roat->y = 90 * (1 - train->done);
            }
            obj->pos->y = 0.2;
        }
    }
}

unsigned int ol::train::addTrain(Train* train)
{
    return trains_buildable->push(train);
}

void ol::train::removeTrain(unsigned int id)
{
    trains_buildable->dropElement(id);
}
