#include <iostream>
#include <fstream>
#include <string>
#include <crossGL/crossGL.h>
#include <game.h>
#include <py_game.h>
#include <Python.h>

using namespace crossGL::screen;
using namespace crossGL::events;
using namespace crossGL::render;
using namespace ol;
using namespace ol::python;
using namespace std;

static PyMethodDef pyMethods[] =
{
    {"setBasicEvents",  pySetBasicEvents, METH_VARARGS, "Set the basic events."},
    {"runGame",  pyRunGame, METH_VARARGS, "Run the game."},
    {"menuInit",  pyInitMenu, METH_VARARGS, "Init the menu."},
    {"menuUpdate",  pyUpdateMenu, METH_VARARGS, "Update the menu."},
    {"menuRemoveAll",  pyRemoveAllMenu, METH_VARARGS, "Remove all menu items."},
    {"world_objectsRender",  pyRenderWorldObjects, METH_VARARGS, "Render world_objects."},
    {"engineClean",  pyEngineClean, METH_VARARGS, "Clean render area."},
    {"engineUpdate",  pyEngineUpdate, METH_VARARGS, "Push rendered to the screen."},
    {"textureLoad",  pyLoadTexture, METH_VARARGS, "Load a texture."},
    {"textureGetSize",  pyGetSizeTexture, METH_VARARGS, "Get the size of a texture."},
    {"textureRemove",  pyRemoveTexture, METH_VARARGS, "Remove a texture."},
    {"fontLoad",  pyLoadFont, METH_VARARGS, "Load a font."},
    {"fontRenderText",  pyRenderTextFont, METH_VARARGS, "Render a text from a font."},
    {"fontRemove",  pyRemoveFont, METH_VARARGS, "Remove a font."},
    {"modelLoad",  pyLoadModel, METH_VARARGS, "Load a model."},
    {"modelRemove",  pyRemoveModel, METH_VARARGS, "Remove a model."},
    {"screen_elementInit",  pyInitScreenElement, METH_VARARGS, "Init screen elements."},
    {"screen_elementCreate",  pyCreateScreenElement, METH_VARARGS, "Create a screen element."},
    {"screen_elementUpdate",  pyUpdateScreenElement, METH_VARARGS, "Update screen elements."},
    {"screen_elementRemove",  pyRemoveFont, METH_VARARGS, "Remove a screen element."},
    {"screen_elementRemoveAll",  pyRemoveAllScreenElement, METH_VARARGS, "Remove remove all screen elements."},
    {"resourceCreate",  pyCreateResouceType, METH_VARARGS, "Create a resource."},
    {"resourceRemove",  pyRemoveResouceType, METH_VARARGS, "Remove a resource."},
    {"companyCreate",  pyCreateCompany, METH_VARARGS, "Create a company."},
    {"companyRemove",  pyRemoveCompany, METH_VARARGS, "Remove a company."},
    {"trainCreate",  pyCreateTrain, METH_VARARGS, "Create a train."},
    {"trainRemove",  pyRemoveTrain, METH_VARARGS, "Remove a train."},
    {"callbackGameInited",  pySetCallBackGameInited, METH_VARARGS, "Set the game inited callback."},
    {NULL, NULL, 0, NULL}
};
static struct PyModuleDef pyGameModul = {PyModuleDef_HEAD_INIT, "game", nullptr, -1, pyMethods};

PyMODINIT_FUNC pyInitGameModul()
{
    return PyModule_Create(&pyGameModul);
}

static void exit_game()
{
    cglWindowClose();
    cglTerminate();
    exit(-1);
}

int main(int argc, char* argv[])
{
    float x = 0;
    unsigned char* y_ = (unsigned char*)((void*)&x);
    unsigned int size = static_cast<unsigned int>(sizeof(x));
    y_[size - 1] = 1;
    cout << x << endl;
    if(!cglInit(CGL_RENDER_DEFAULT))
    {
        cerr << "Init was not possible." << endl;
        return 1;
    }
    if(!cglWindowOpen(800, 600, "TEST!!!", false))
    {
        cerr << "Open window was not possible." << endl;
        return 2;
    }
    cglEventSetCloseWindow(exit_game);
    cglSetRenderFunc(CGL_RENDER_FUNC_NORMAL);

    #ifdef RELEASE_BUILD
        string py_path = string(argv[0]) + "/data/script";
    #else
        string py_path = "./data/script";
    #endif // RELEASE_BUILD

    string load_main =  "import sys\n";
           load_main += "sys.path.append('" + py_path + "');\n";
           load_main += "import main";

    PyImport_AppendInittab("game", pyInitGameModul);
    Py_Initialize();
    PyRun_SimpleString(load_main.c_str());
    PyRun_SimpleString("main.main()");
    Py_Finalize();

    cglWindowClose();
    cglTerminate();
    return 0;
}
