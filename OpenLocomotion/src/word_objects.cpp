#include <crossGL/crossGL.h>
#include <game.h>
#include <math.h>
#include <tools.h>

using namespace std;
using namespace crossGL::render;
using namespace tools;

static IDManager<ol::world_objects::worldObject>* objects = new IDManager<ol::world_objects::worldObject>();

unsigned int ol::world_objects::addObject(ol::world_objects::worldObject* object)
{
    return objects->push(object);
}

void ol::world_objects::removeObject(unsigned int id)
{
    objects->dropElement(id);
}

void ol::world_objects::removeObjectCarefuly(unsigned int id)
{
    objects->dropElementCarefully(id);
}

void ol::world_objects::removeAll()
{
    delete objects;
    objects = new IDManager<ol::world_objects::worldObject>();
}

void ol::world_objects::render()
{
    double pos_x, pos_y, pos_z;
    double scale_x, scale_y, scale_z;
    double roat_x, roat_y, roat_z;
    cglWorldGetPos(pos_x, pos_y, pos_z);
    cglWorldGetScale(scale_x, scale_y, scale_z);
    cglWorldGetRoat(roat_x, roat_y, roat_z);

    IDManager<ol::world_objects::worldObject>::IDManagerIterator iter = objects->getIterator();

    for(ol::world_objects::worldObject* object = iter.next(); iter.isDetectable(); object = iter.next())
    {
        if(object->menu_object)
        {
            ol::game::makeOrthoView();
            cglWorldSetPos(0, 0, 0);
            cglWorldSetScale(1, 1, 1);
            cglWorldSetRoat(0, 0, 0);
        }
        crossGL::render::cglRender(object->object);
        if(object->menu_object)
        {
            ol::game::makeFrustrumView();
            cglWorldSetPos(pos_x, pos_y, pos_z);
            cglWorldSetScale(scale_x, scale_y, scale_z);
            cglWorldSetRoat(roat_x, roat_y, roat_z);
        }
    }
}

void ol::world_objects::callHitObject(unsigned int x, unsigned int y)
{
    crossGL::render::cglSetRenderFunc(crossGL::render::CGL_RENDER_FUNC_OBJECT_COLOR);
    crossGL::render::cglClean();

    double pos_x, pos_y, pos_z;
    double scale_x, scale_y, scale_z;
    double roat_x, roat_y, roat_z;
    cglWorldGetPos(pos_x, pos_y, pos_z);
    cglWorldGetScale(scale_x, scale_y, scale_z);
    cglWorldGetRoat(roat_x, roat_y, roat_z);

    double steps = 1.0f / (double)(objects->size());
    double pos = 0.0f;

    IDManager<ol::world_objects::worldObject>::IDManagerIterator iter = objects->getIterator();
    for(ol::world_objects::worldObject* object = iter.next(); iter.isDetectable(); object = iter.next())
    {
        if(object->menu_object)
        {
            ol::game::makeOrthoView();
            cglWorldSetPos(0, 0, 0);
            cglWorldSetScale(1, 1, 1);
            cglWorldSetRoat(0, 0, 0);
        }
        crossGL::render::Object* obj = object->object;
        double r = obj->r;
        double g = obj->g;
        double b = obj->b;
        double a = obj->a;
        crossGL::render::cglSetColorID(pos, obj->r, obj->g, obj->b);
        obj->a = 1.0f;
        crossGL::render::cglRender(obj);
        obj->r = r;
        obj->g = g;
        obj->b = b;
        obj->a = a;
        pos += steps;
        if(object->menu_object)
        {
            ol::game::makeFrustrumView();
            cglWorldSetPos(pos_x, pos_y, pos_z);
            cglWorldSetScale(scale_x, scale_y, scale_z);
            cglWorldSetRoat(roat_x, roat_y, roat_z);
        }
    }

    double id = crossGL::render::cglGetPixelID(x, y);
    crossGL::render::cglSetRenderFunc(crossGL::render::CGL_RENDER_FUNC_NORMAL);
    crossGL::render::cglClean();

    if(id == -1.0f)
        return;
    unsigned int object_id = (unsigned int)(floor(id / steps + 0.5f));
    iter = objects->getIterator();
    for(ol::world_objects::worldObject* obj = iter.next(); iter.isDetectable(); obj = iter.next())
    {
        if(object_id == 0)
            obj->call_back(x, y);
        object_id--;
    }
}
