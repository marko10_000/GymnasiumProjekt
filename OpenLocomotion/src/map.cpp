#include<crossGL/crossGL.h>
#include<game.h>
#include<math.h>
#include<fstream>
#include<iostream>
#include<vector>

#define MAP_SIZE 11

using namespace crossGL::render;
using namespace crossGL::imports;
using namespace std;
using namespace ol::world_objects;

static void (*landscape_click_call_back)(unsigned int x, unsigned int y) = nullptr;

class MapObject : public worldObject
{
    public:
        unsigned int id;
        bool landscape;

        MapObject(Object* object) : worldObject(object)
        {
            id = 0;
            landscape = false;
        }
        virtual ~MapObject(){}

        virtual void call_back(unsigned int x, unsigned int y)
        {
            if(this->landscape)
            {
                if(landscape_click_call_back == nullptr)
                    return;
                unsigned int x_, y_;
                ol::map::getLandscapePos(x, y, &x_, &y_);
                landscape_click_call_back(x_, y_);
            }
        }
};

static Texture* gras = nullptr;
static unsigned char** map_height = nullptr;
static MapObject* map_object = new MapObject(nullptr);
static Object* map_object_colors = nullptr;

static bool update = true;

static unsigned char*** worldBlocks = nullptr;
static unsigned int** worldBlocksLength = 0;

void updateMap_()
{
    if(map_object->object != nullptr)
        delete map_object->object;
    if(map_object_colors != nullptr)
        delete map_object_colors;
    ObjectMash* world = new ObjectMash();
    Polygon* polygon;
    bool side;
    for(unsigned int x = 0; x < MAP_SIZE - 1; x++)
        for(unsigned int y = 0; y < MAP_SIZE - 1; y++)
        {
            side = abs(map_height[x][y] - map_height[x + 1][y + 1]) <= abs(map_height[x][y + 1] - map_height[x + 1][y]);
            polygon = new Polygon();
            if(side)
            {
                polygon->add(new Vertex((double)x, ((double)(map_height[x][y])) * 0.5f, (double)y, (double)x / (double)MAP_SIZE, (double)y / (double)MAP_SIZE));
                polygon->add(new Vertex(((double)x) + 1.0f, ((double)(map_height[x + 1][y])) * 0.5f, (double)y, (double)(x + 1) / (double)MAP_SIZE, (double)y / (double)MAP_SIZE));
                polygon->add(new Vertex(((double)x) + 1.0f, ((double)(map_height[x + 1][y + 1])) * 0.5f, ((double)y) + 1.0f, (double)(x + 1) / (double)MAP_SIZE, (double)(y + 1) / (double)MAP_SIZE));
                polygon->add(new Vertex(((double)x), ((double)(map_height[x][y + 1])) * 0.5f, ((double)y) + 1.0f, (double)x / (double)MAP_SIZE, (double)(y + 1) / (double)MAP_SIZE));
            }
            else
            {
                polygon->add(new Vertex(((double)x) + 1.0f, ((double)(map_height[x + 1][y])) * 0.5f, ((double)y), (double)(x + 1) / (double)MAP_SIZE, (double)y / (double)MAP_SIZE));
                polygon->add(new Vertex(((double)x) + 1.0f, ((double)(map_height[x + 1][y + 1])) * 0.5f, ((double)y) + 1.0f, (double)(x + 1) / (double)MAP_SIZE, (double)(y + 1) / (double)MAP_SIZE));
                polygon->add(new Vertex(((double)x), ((double)(map_height[x][y + 1])) * 0.5f, ((double)y) + 1.0f, (double)x / (double)MAP_SIZE, (double)(y + 1) / (double)MAP_SIZE));
                polygon->add(new Vertex((double)x, ((double)(map_height[x][y])) * 0.5f, ((double)y), (double)x / (double)MAP_SIZE, (double)y / (double)MAP_SIZE));
            }
            world->add(polygon);
        }
    map_object->object = new Object(cglCreateRenderObject(world, CGL_RENDER_TYPE_POLYGON));
    map_object->object->setTexture(gras);
    double color_pos = 0;
    unsigned int max_id = (MAP_SIZE - 1) * (MAP_SIZE - 1) + 1;
    double distance = 1.0f / (double)max_id;
    for(unsigned int x = 0; x < MAP_SIZE - 1; x++)
        for(unsigned int y = 0; y < MAP_SIZE - 1; y++)
        {
            polygon = world->content[x * (MAP_SIZE - 1) + y];
            cglSetColorID(color_pos, polygon->r, polygon->g, polygon->b);
            color_pos += distance;
        }
    map_object_colors = new Object(cglCreateRenderObject(world, CGL_RENDER_TYPE_POLYGON));
    update = false;
}

void ol::map::initMap()
{
    if(gras == nullptr)
    {
        gras = cglImportImage("data/texture/gras.tga");
        cglCommitTexture(gras);
        gras->references++;
    }

    if(map_height != nullptr)
    {
        for(unsigned int i = 0; i < MAP_SIZE; i++)
            delete[] map_height[i];
        delete[] map_height;
    }
    map_height = new unsigned char*[MAP_SIZE];
    for(unsigned int i = 0; i < MAP_SIZE; i++)
    {
        map_height[i] = new unsigned char[MAP_SIZE];
        for(unsigned int j = 0; j < MAP_SIZE; j++)
            map_height[i][j] = 0;
    }
    map_object->id = addObject(map_object);
    map_object->landscape = true;

    if(worldBlocks != nullptr)
    {
        for(unsigned int i = 0; i < MAP_SIZE; i++)
        {
            for(unsigned int j = 0; j < MAP_SIZE; j++)
                delete[] worldBlocks[i][j];
            delete[] worldBlocks[i];
            delete[] worldBlocksLength[i];
        }
        delete[] worldBlocks;
        delete[] worldBlocksLength;
    }
    worldBlocks = new unsigned char**[MAP_SIZE];
    worldBlocksLength = new unsigned int*[MAP_SIZE];
    for(unsigned int i = 0; i < MAP_SIZE; i++)
    {
        worldBlocks[i] = new unsigned char*[MAP_SIZE];
        worldBlocksLength[i] = new unsigned int[MAP_SIZE];
        for(unsigned int j = 0; j < MAP_SIZE; j++)
        {
            worldBlocks[i][j] = new unsigned char[0];
            worldBlocksLength[i][j] = 0;
        }
    }

    updateMap();
    ol::company::initCompanyMap(MAP_SIZE - 1);
}

void ol::map::updateMap()
{
    if(update)
        updateMap_();
}

unsigned char ol::map::getHeight(unsigned int x, unsigned int y)
{
    return map_height[x][y];
}
void ol::map::setHeight(unsigned int x, unsigned int y, unsigned char height)
{
    map_height[x][y] = height;
    update = true;
}

bool ol::map::getLandscapePos(unsigned int x, unsigned int y, unsigned int* x_, unsigned int* y_)
{
    cglClean();
    cglSetRenderFunc(CGL_RENDER_FUNC_POLYGON_COLOR);
    cglRender(map_object_colors);
    double id = cglGetPixelID(x, y);
    cglSetRenderFunc(crossGL::render::CGL_RENDER_FUNC_NORMAL);
    cglClean();

    if(id == -1)
        return false;

    unsigned int max_id = (MAP_SIZE - 1) * (MAP_SIZE - 1) + 1;
    double distance = 1.0f / (double)max_id;
    unsigned int pol_id = (unsigned int)(floor(id / distance + 0.5f));

    (*x_) = pol_id / (MAP_SIZE - 1);
    (*y_) = pol_id % (MAP_SIZE - 1);

    return true;
}

void ol::map::setLandscapeClickEvent(void(*call_back)(unsigned int, unsigned int))
{
    landscape_click_call_back = call_back;
}

bool ol::map::checkIfIsBuilable(unsigned char type, unsigned int x, unsigned int y)
{
    if(x >= MAP_SIZE - 1)
        return false;
    if(y >= MAP_SIZE - 1)
        return false;

    // Check if it is a block
    if(type == MAP_BLOCK_ALL)
    {
        if(worldBlocksLength[x][y] == 0)
            return true;
        else
            return false;
    }

    // Check stations
    if(type == MAP_BLOCK_STATION_X)
    {
        if(worldBlocksLength[x][y] == 1)
            if(worldBlocks[x][y][0] == MAP_BLOCK_STRIGHT_X)
                return true;
        return false;
    }
    if(type == MAP_BLOCK_STATION_Y)
    {
        if(worldBlocksLength[x][y] == 1)
            if(worldBlocks[x][y][0] == MAP_BLOCK_STRIGHT_Y)
                return true;
        return false;
    }

    // Check if it set
    for(unsigned int i = 0; i < worldBlocksLength[x][y]; i++)
        if(worldBlocks[x][y][i] == type)
            return false;

    // Check space
    if(type == MAP_BLOCK_CORVE_0)
    {
        if(y == MAP_SIZE - 2)
            return false;
        for(unsigned int i = 0; i < worldBlocksLength[x + 1][y]; i++)
        {
            if(worldBlocks[x + 1][y][i] == MAP_BLOCK_ALL)
                return false;
            else if(worldBlocks[x + 1][y][i] == MAP_BLOCK_STATION_X)
                return false;
            else if(worldBlocks[x + 1][y][i] == MAP_BLOCK_STATION_Y)
                return false;
        }
        for(unsigned int i = 0; i < worldBlocksLength[x][y]; i++)
        {
            if(worldBlocks[x][y][i] == MAP_BLOCK_ALL)
                return false;
            else if(worldBlocks[x][y][i] == MAP_BLOCK_STATION_X)
                return false;
            else if(worldBlocks[x][y][i] == MAP_BLOCK_STATION_Y)
                return false;
        }
        for(unsigned int i = 0; i < worldBlocksLength[x][y + 1]; i++)
        {
            if(worldBlocks[x][y + 1][i] == MAP_BLOCK_ALL)
                return false;
            else if(worldBlocks[x][y + 1][i] == MAP_BLOCK_STATION_X)
                return false;
            else if(worldBlocks[x][y + 1][i] == MAP_BLOCK_STATION_Y)
                return false;
        }
    }
    else if(type == MAP_BLOCK_CORVE_90)
    {
        if(x == MAP_SIZE - 2)
            return false;
        if(y == MAP_SIZE - 2)
            return false;
        for(unsigned int i = 0; i < worldBlocksLength[x][y]; i++)
        {
            if(worldBlocks[x][y][i] == MAP_BLOCK_ALL)
                return false;
            else if(worldBlocks[x][y][i] == MAP_BLOCK_STATION_X)
                return false;
            else if(worldBlocks[x][y][i] == MAP_BLOCK_STATION_Y)
                return false;
        }
        for(unsigned int i = 0; i < worldBlocksLength[x + 1][y]; i++)
        {
            if(worldBlocks[x + 1][y][i] == MAP_BLOCK_ALL)
                return false;
            else if(worldBlocks[x + 1][y][i] == MAP_BLOCK_STATION_X)
                return false;
            else if(worldBlocks[x + 1][y][i] == MAP_BLOCK_STATION_Y)
                return false;
        }
        for(unsigned int i = 0; i < worldBlocksLength[x + 1][y + 1]; i++)
        {
            if(worldBlocks[x + 1][y + 1][i] == MAP_BLOCK_ALL)
                return false;
            else if(worldBlocks[x + 1][y + 1][i] == MAP_BLOCK_STATION_X)
                return false;
            else if(worldBlocks[x + 1][y + 1][i] == MAP_BLOCK_STATION_Y)
                return false;
        }
    }
    else if(type == MAP_BLOCK_CORVE_180)
    {
        if(x == MAP_SIZE - 2)
            return false;
        if(y == 0)
            return false;
        for(unsigned int i = 0; i < worldBlocksLength[x][y]; i++)
        {
            if(worldBlocks[x][y][i] == MAP_BLOCK_ALL)
                return false;
            else if(worldBlocks[x][y][i] == MAP_BLOCK_STATION_X)
                return false;
            else if(worldBlocks[x][y][i] == MAP_BLOCK_STATION_Y)
                return false;
        }
        for(unsigned int i = 0; i < worldBlocksLength[x + 1][y]; i++)
        {
            if(worldBlocks[x + 1][y][i] == MAP_BLOCK_ALL)
                return false;
            else if(worldBlocks[x + 1][y][i] == MAP_BLOCK_STATION_X)
                return false;
            else if(worldBlocks[x + 1][y][i] == MAP_BLOCK_STATION_Y)
                return false;
        }
        for(unsigned int i = 0; i < worldBlocksLength[x + 1][y - 1]; i++)
        {
            if(worldBlocks[x + 1][y - 1][i] == MAP_BLOCK_ALL)
                return false;
            else if(worldBlocks[x + 1][y - 1][i] == MAP_BLOCK_STATION_X)
                return false;
            else if(worldBlocks[x + 1][y - 1][i] == MAP_BLOCK_STATION_Y)
                return false;
        }
    }
    else if(type == MAP_BLOCK_CORVE_270)
    {
        if(x == 0)
            return false;
        for(unsigned int i = 0; i < worldBlocksLength[x - 1][y]; i++)
        {
            if(worldBlocks[x - 1][y][i] == MAP_BLOCK_ALL)
                return false;
            else if(worldBlocks[x - 1][y][i] == MAP_BLOCK_STATION_X)
                return false;
            else if(worldBlocks[x - 1][y][i] == MAP_BLOCK_STATION_Y)
                return false;
        }
        for(unsigned int i = 0; i < worldBlocksLength[x - 1][y + 1]; i++)
        {
            if(worldBlocks[x - 1][y + 1][i] == MAP_BLOCK_ALL)
                return false;
            else if(worldBlocks[x - 1][y + 1][i] == MAP_BLOCK_STATION_X)
                return false;
            else if(worldBlocks[x - 1][y + 1][i] == MAP_BLOCK_STATION_Y)
                return false;
        }
        for(unsigned int i = 0; i < worldBlocksLength[x][y + 1]; i++)
        {
            if(worldBlocks[x][y + 1][i] == MAP_BLOCK_ALL)
                return false;
            else if(worldBlocks[x][y + 1][i] == MAP_BLOCK_STATION_X)
                return false;
            else if(worldBlocks[x][y + 1][i] == MAP_BLOCK_STATION_Y)
                return false;
        }
    }

    std::cout << (unsigned int)type << "|" << x << "|" << y << std::endl;
    return true;
}

inline void inlineBuildObject(unsigned char type, unsigned int x, unsigned int y)
{
    if(x >= MAP_SIZE)
        return;
    if(y >= MAP_SIZE)
        return;

    unsigned char* worldBlocks2 = new unsigned char[worldBlocksLength[x][y] + 1];
    for(unsigned int i = 0; i < worldBlocksLength[x][y]; i++)
        worldBlocks2[i] = worldBlocks[x][y][i];
    worldBlocks2[worldBlocksLength[x][y]] = type;
    delete[] worldBlocks[x][y];
    worldBlocks[x][y] = worldBlocks2;
    worldBlocksLength[x][y]++;
}

void ol::map::buildObject(unsigned char type, unsigned int x, unsigned int y)
{
    inlineBuildObject(type, x, y);
}

unsigned char* ol::map::getObjects(unsigned int x, unsigned int y)
{
    unsigned char* datas = worldBlocks[x][y];
    unsigned int length = worldBlocksLength[x][y];
    unsigned char* back = new unsigned char[length + 1];
    for(unsigned int i = 0; i < length; i++)
        back[i] = datas[i];
    back[length] = 0;
    return back;
}

void inline inlineRemoveObject(unsigned char type, unsigned int x, unsigned int y)
{
    if(x >= MAP_SIZE)
        return;
    if(y >= MAP_SIZE)
        return;

    unsigned int id;
    for(id = 0; id < worldBlocksLength[x][y]; id++)
        if(worldBlocks[x][y][id] == type)
            break;

    if(id == worldBlocksLength[x][y])
        return;

    unsigned char* worldBlocks2 = new unsigned char[worldBlocksLength[x][y] - 1];
    for(unsigned int i = 0; i < id; i++)
        worldBlocks2[i] = worldBlocks[x][y][i];
    for(unsigned int i = id + 1; i < worldBlocksLength[x][y]; i++)
        worldBlocks2[i - 1] = worldBlocks[x][y][i];
    delete[] worldBlocks[x][y];
    worldBlocks[x][y] = worldBlocks2;
    worldBlocksLength[x][y]--;
}

void ol::map::removeObject(unsigned char type, unsigned int x, unsigned int y)
{
    inlineRemoveObject(type, x, y);
    if(type == MAP_BLOCK_CORVE_0)
    {
        inlineRemoveObject(MAP_BLOCK_RAIL, x - 1, y);
        inlineRemoveObject(MAP_BLOCK_RAIL, x - 1, y + 1);
    }
    else if(type == MAP_BLOCK_CORVE_90)
    {
        inlineRemoveObject(MAP_BLOCK_RAIL, x - 1, y);
        inlineRemoveObject(MAP_BLOCK_RAIL, x - 1, y + 1);
    }
}
