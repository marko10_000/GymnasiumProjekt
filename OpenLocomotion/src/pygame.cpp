#include <Python.h>
#include <py_game.h>
#include <game.h>
#include <crossGL/crossGL.h>
#include <tools.h>
#include <string>
#include <iostream>

using namespace crossGL::render;
using namespace crossGL::screen;
using namespace crossGL::events;
using namespace crossGL::imports;
using namespace tools;
using namespace std;

static void window_resize(unsigned int x, unsigned y)
{
    cglViewPort(0, 0, x, y);
    ol::menu::updateWindow(x, y, (double)x / (double)y, 1);
    ol::screen::resize(x, y, - (double)x / (double)y, 1);
}

static void mouse_pressed(unsigned int x, unsigned int y, unsigned int button, unsigned int action)
{
    if((button == CGL_MOUSE_LEFT) && (action == CGL_EVENT_MOUSE_DOWN))
        ol::world_objects::callHitObject(x, y);
}

PyObject* ol::python::pySetBasicEvents(PyObject* self, PyObject* args)
{
    unsigned int x = 0, y = 0;
    cglGetWindowSize(x, y);
    window_resize(x, y);
    cglEventSetResizeWindow(window_resize);
    cglEventSetMouse(mouse_pressed);
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject* ol::python::pyRunGame(PyObject* self, PyObject* args)
{
    ol::runGame();
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject* ol::python::pyInitMenu(PyObject* self, PyObject* args)
{
    ol::menu::init();
    Py_INCREF(Py_None);
    return Py_None;
}
PyObject* ol::python::pyUpdateMenu(PyObject* self, PyObject* args)
{
    ol::menu::update();
    Py_INCREF(Py_None);
    return Py_None;
}
PyObject* ol::python::pyRemoveAllMenu(PyObject* self, PyObject* args)
{
    ol::menu::removeAll();
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject* ol::python::pyRenderWorldObjects(PyObject* self, PyObject* args)
{
    ol::world_objects::render();
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject* ol::python::pyEngineClean(PyObject* self, PyObject* args)
{
    cglClean();
    Py_INCREF(Py_None);
    return Py_None;
}
PyObject* ol::python::pyEngineUpdate(PyObject* self, PyObject* args)
{
    cglWindowUpdate();
    Py_INCREF(Py_None);
    return Py_None;
}

static IDManager<Texture>* textures = new IDManager<Texture>();

PyObject* ol::python::pyLoadTexture(PyObject* self, PyObject* args)
{
    const char* texture_file;
    if (!PyArg_ParseTuple(args, "s", &texture_file))
        return nullptr;
    Texture* texture = cglImportImage(string(texture_file));
    if(texture == nullptr)
    {
        Py_INCREF(Py_None);
        return Py_None;
    }
    texture->references++;
    cglCommitTexture(texture);
    return Py_BuildValue("i", textures->push(texture));
}
PyObject* ol::python::pyRemoveTexture(PyObject* self, PyObject* args)
{
    int texture_id;
    if (!PyArg_ParseTuple(args, "i", &texture_id))
        return nullptr;
    textures->dropElement(texture_id);
    Py_INCREF(Py_None);
    return Py_None;
}
PyObject* ol::python::pyGetSizeTexture(PyObject* self, PyObject* args)
{
    int texture_id;
    if (!PyArg_ParseTuple(args, "i", &texture_id))
        return nullptr;
    Texture* tex = textures->get(texture_id);
    PyObject* result = Py_BuildValue("ii", tex->width, tex->height);
    return result;
}

static IDManager<Font>* fonts = new IDManager<Font>();

PyObject* ol::python::pyLoadFont(PyObject* self, PyObject* args)
{
    const char* ffile_;
    if (!PyArg_ParseTuple(args, "s", &ffile_))
        return nullptr;
    string ffile = ffile_;
    Font* font = cglImportTTF(ffile);
    if(font == nullptr)
    {
        Py_INCREF(Py_None);
        return Py_None;
    }
    return Py_BuildValue("i", fonts->push(font));
}
PyObject* ol::python::pyRenderTextFont(PyObject* self, PyObject* args)
{
    int font_id, size;
    const char* text_;
    if (!PyArg_ParseTuple(args, "isi", &font_id, &text_, &size))
        return nullptr;
    string text = text_;
    Texture* texture = fonts->get(font_id)->createTexture(text, size);
    texture->references++;
    cglCommitTexture(texture);
    return Py_BuildValue("i", textures->push(texture));
}
PyObject* ol::python::pyRemoveFont(PyObject* self, PyObject* args)
{
    int font_id;
    if (!PyArg_ParseTuple(args, "i", &font_id))
        return nullptr;
    fonts->dropElement(font_id);
    Py_INCREF(Py_None);
    return Py_None;
}

static IDManager<ObjectMash>* mashes = new IDManager<ObjectMash>();

PyObject* ol::python::pyLoadModel(PyObject* self, PyObject* args)
{
    const char* file_;
    if (!PyArg_ParseTuple(args, "s", &file_))
        return nullptr;
    string file = file_;
    ObjectMash* mash = cglImportWavefront(file);
    if(mash == nullptr)
    {
        Py_INCREF(Py_None);
        return Py_None;
    }
    mash->references++;
    return Py_BuildValue("i", mashes->push(mash));
}
PyObject* ol::python::pyRemoveModel(PyObject* self, PyObject* args)
{
    int mash_id;
    if (!PyArg_ParseTuple(args, "i", &mash_id))
        return nullptr;

    mashes->dropElement(mash_id);
    Py_INCREF(Py_None);
    return Py_None;
}

class pyScreenElement : public ol::screen::ScreenObject
{
    public:
        PyObject* obj;

        pyScreenElement(unsigned int x, unsigned int y, unsigned int width, unsigned int height, Texture* texture, PyObject* obj) : ScreenObject(texture)
        {
            this->x = x;
            this->y = y;
            this->width = width;
            this->height = height;
            this->obj = obj;
            Py_IncRef(this->obj);
        }
        virtual ~pyScreenElement()
        {
            Py_DecRef(this->obj);
        }

        virtual void call_back()
        {
            PyObject* func = PyObject_GetAttrString(this->obj, "call_back");
            if(obj != nullptr)
            {
                PyObject* result = PyObject_CallFunction(func, "O", this->obj);
                Py_DecRef(result);
            }
        }
};

static IDManager<pyScreenElement>* screenElements = new IDManager<pyScreenElement>();

PyObject* ol::python::pyInitScreenElement(PyObject* self, PyObject* args)
{
    ol::screen::init();
    Py_INCREF(Py_None);
    return Py_None;
}
PyObject* ol::python::pyCreateScreenElement(PyObject* self, PyObject* args)
{
    int x, y, width, height, texture;
    PyObject* obj;
    if (!PyArg_ParseTuple(args, "iiiiiO", &x, &y, &width, &height, &texture, &obj))
        return nullptr;
    pyScreenElement* element = new pyScreenElement(x, y, width, height, textures->get(texture), obj);
    ol::screen::addElement(element);
    return Py_BuildValue("i", screenElements->push(element));
}
PyObject* ol::python::pyUpdateScreenElement(PyObject* self, PyObject* args)
{
    ol::screen::update();
    Py_INCREF(Py_None);
    return Py_None;
}
PyObject* ol::python::pyRemoveScreenElement(PyObject* self, PyObject* args)
{
    int id;
    if(!PyArg_ParseTuple(args, "i", &id))
        return nullptr;
    ol::screen::removeElement(id);
    Py_INCREF(Py_None);
    return Py_None;
}
PyObject* ol::python::pyRemoveAllScreenElement(PyObject* self, PyObject* args)
{
    ol::screen::removeAll();
    Py_INCREF(Py_None);
    return Py_None;
}

static IDManager<ol::company::Resource>* resources = new IDManager<ol::company::Resource>();

PyObject* ol::python::pyCreateResouceType(PyObject* self, PyObject* args)
{
    const char* name;
    int value;
    if (!PyArg_ParseTuple(args, "si", &name, &value))
        return nullptr;
    ol::company::Resource* resource = new ol::company::Resource();
    resource->name = string(name);
    resource->value = value;
    return Py_BuildValue("i", resources->push(resource));
}
PyObject* ol::python::pyRemoveResouceType(PyObject* self, PyObject* args)
{
    int id;
    if(!PyArg_ParseTuple(args, "i", &id))
        return nullptr;
    resources->dropElement(id);
    Py_INCREF(Py_None);
    return Py_None;
}

static IDManager<ol::company::Company>* companies = new IDManager<ol::company::Company>();

PyObject* ol::python::pyCreateCompany(PyObject* self, PyObject* args)
{
    int texture, resouce, min, max;
    const char* name;
    if(!PyArg_ParseTuple(args, "iiiis", &texture, &resouce, &min, &max, &name))
        return nullptr;
    ol::company::Company* comp = new ol::company::Company();
    comp->name = string(name);
    comp->min = min;
    comp->max = max;
    comp->produce = resources->get(resouce);
    comp->texture = textures->get(texture);
    ol::company::addCompany(comp);
    return Py_BuildValue("i", companies->push(comp));
}
PyObject* ol::python::pyRemoveCompany(PyObject* self, PyObject* args)
{
    int id;
    if(!PyArg_ParseTuple(args, "i", &id))
        return nullptr;
    ol::company::removeCompany(companies->get(id));
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject* ol::python::pyCreateTrain(PyObject* self, PyObject* args)
{
    ol::train::Train* train = new ol::train::Train();
    int mash, texture, costs, speed, contains;
    const char* name;
    if(!PyArg_ParseTuple(args, "iisiii", &mash, &texture, &name, &costs, &speed, &contains))
        return nullptr;
    train->model = new Object(cglCreateRenderObject(mashes->get(mash), CGL_RENDER_TYPE_POLYGON));
    train->model->setTexture(textures->get(texture));
    train->name = string(name);
    train->costs = costs;
    train->speed = speed;
    train->capacity = contains;
    return Py_BuildValue("i", ol::train::addTrain(train));
}
PyObject* ol::python::pyRemoveTrain(PyObject* self, PyObject* args)
{
    int id;
    if(!PyArg_ParseTuple(args, "i", &id))
        return nullptr;
    ol::train::removeTrain(id);
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject* game_init_callback = Py_None;

PyObject* ol::python::pySetCallBackGameInited(PyObject* self, PyObject* args)
{
    if(!PyArg_ParseTuple(args, "O", &game_init_callback))
        return nullptr;
    Py_INCREF(Py_None);
    return Py_None;
}

void ol::python::game_inited()
{
    if(game_init_callback == Py_None)
        cout << "Wrong!!!" << endl;
    Py_INCREF(Py_None);
    Py_DecRef(PyObject_CallFunction(game_init_callback, ""));
}
