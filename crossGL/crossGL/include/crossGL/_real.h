/*
 * _functions.h
 *
 *  Created on: 12.02.2014
 *      Author: marko
 */

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#include<string>

namespace crossGL
{
	namespace _funcs
	{
		// --------------------------------------------------
		// -                 Real Constants                 -
		// --------------------------------------------------

		static const unsigned int CGL_0_1_RENDER_DEFAULT = 0;
		static const unsigned int CGL_0_1_RENDER_OPENGL = 1000;
		static const unsigned int CGL_0_1_RENDER_OPENGL_2_1 = 1001; // Perhaps not in this project
		static const unsigned int CGL_0_1_RENDER_OPENGL_3_3 = 1002; // <-- Current! Default
		static const unsigned int CGL_0_1_RENDER_OPENGL_4_3 = 1003; // Sourly not in this project
		// DirectX comes later! (not in this Project)

		static const unsigned int CGL_0_1_OBJECT_RENDER_TYPE_POINT = 3000;
		static const unsigned int CGL_0_1_OBJECT_RENDER_TYPE_LINE = 3001;
		static const unsigned int CGL_0_1_OBJECT_RENDER_TYPE_POLYGON = 3002;

		static const unsigned int CGL_0_1_RENDER_FUNC_NORMAL = 3100;
		static const unsigned int CGL_0_1_RENDER_FUNC_OBJECT_COLOR = 3101;
		static const unsigned int CGL_0_1_RENDER_FUNC_POLYGON_COLOR = 3102;

		static const unsigned int CGL_0_1_EVENT_KEY_DOWN = 4001;
		static const unsigned int CGL_0_1_EVENT_KEY_REPEAT = 4002;
		static const unsigned int CGL_0_1_EVENT_KEY_UP = 4003;

		static const unsigned int CGL_0_1_EVENT_MOUSE_CLICK_LEFT = 4101;
		static const unsigned int CGL_0_1_EVENT_MOUSE_CLICK_CENTER = 4102;
		static const unsigned int CGL_0_1_EVENT_MOUSE_CLICK_RIGHT = 4103;

		static const unsigned int CGL_0_1_EVENT_MOUSE_BUTTON_DOWN = 4198;
		static const unsigned int CGL_0_1_EVENT_MOUSE_BUTTON_UP = 4199;

		// --------------------------------------------------
		// -                   Real Types                   -
		// --------------------------------------------------

		class cgl_0_1_cloner
		{
			public:
				unsigned int references;

				cgl_0_1_cloner()
				{
					this->references = 0;
				}
				virtual ~cgl_0_1_cloner(){}

				virtual cgl_0_1_cloner* ownClone() = 0;
				virtual cgl_0_1_cloner* depthClone() = 0;
		};

		template<class t>
		class cgl_0_1_basic_array : public virtual cgl_0_1_cloner
		{
			public:
				unsigned int length;
				t** content;

				cgl_0_1_basic_array() : cgl_0_1_cloner()
				{
					this->length = 0;
					this->content = new t*[0];
				}
				virtual ~cgl_0_1_basic_array()
				{
					for(int i = 0; i < this->length; i++)
						if(this->content[i] != nullptr)
						{
							this->content[i]->references--;
							if(this->content[i]->references == 0)
								delete this->content[i];
						}
					delete[] this->content;
				}

				virtual unsigned int add(t* element)
				{
					t** content2 = new t*[this->length + 1];
					for(unsigned int i = 0; i < this->length; i++)
						content2[i] = this->content[i];
					content2[this->length] = element;
					if(element != nullptr)
						element->references++;
					this->length++;
					delete[] this->content;
					this->content = content2;
					return this->length - 1;
				}

				virtual unsigned int add(t** elements, unsigned int length)
				{
					t** content2 = new t*[this->length + length];
					for(unsigned int i = 0; i < this->length; i++)
						content2[i] = this->content[i];
					for(unsigned int i = 0; i < length; i++)
					{
						content2[this->length + i] = elements[i];
						if(elements[i] != nullptr)
							elements[i]->references++;
					}
					unsigned int pos = this->length;
					this->length += length;
					delete[] this->content;
					this->content = content2;
					return pos;
				}

				virtual void remove(unsigned int pos)
				{
					if(pos >= this->length)
						return;
					t** content2 = new t*[this->length - 1];
					for(unsigned int i = 0; i < pos; i++)
						content2[i] = this->content[i];
					if(this->content[pos] != nullptr)
					{
						this->content[pos]->references--;
						if(this->content[pos]->references == 0)
							delete this->content[pos];
					}
					for(unsigned int i = pos + 1; i < this->length; i++)
						content2[i - 1] = this->content[i];
					delete[] this->content;
					this->content = content2;
				}

				virtual cgl_0_1_basic_array<t>* ownClone()
				{
					cgl_0_1_basic_array<t>* back = new cgl_0_1_basic_array<t>();
					back->add(this->content, this->length);
					return back;
				}

				virtual cgl_0_1_basic_array<t>* depthClone()
				{
					cgl_0_1_basic_array<t>* back = new cgl_0_1_basic_array<t>();
					for(unsigned int i = 0; i < this->length; i++)
						if(this->content[i] == nullptr)
							back->add(nullptr);
						else
							back->add(this->content[i]->depthClone());
					return back;
				}
		};

		class cgl_0_1_vertex : public virtual cgl_0_1_cloner
		{
			public:
				double x;
				double y;
				double z;
				double u;
				double v;

				cgl_0_1_vertex()
				{
					this->x = 0.0f;
					this->y = 0.0f;
					this->z = 0.0f;
					this->u = 0.0f;
					this->v = 0.0f;
				}
				cgl_0_1_vertex(double x, double y, double z)
				{
					this->x = x;
					this->y = y;
					this->z = z;
					this->u = 0.0f;
					this->v = 0.0f;
				}
				cgl_0_1_vertex(double x, double y, double z, double u, double v)
				{
					this->x = x;
					this->y = y;
					this->z = z;
					this->u = u;
					this->v = v;
				}

				virtual ~cgl_0_1_vertex(){}

				virtual cgl_0_1_vertex* ownClone()
				{
					return new cgl_0_1_vertex(this->x, this->y, this->y, this->u, this->v);
				}

				virtual cgl_0_1_vertex* depthClone()
				{
					return new cgl_0_1_vertex(this->x, this->y, this->z, this->u, this->v);
				}
		};

		class cgl_0_1_polygon : public virtual cgl_0_1_basic_array<cgl_0_1_vertex>
		{
			public:
				double r;
				double g;
				double b;
				double a;

				cgl_0_1_polygon() : cgl_0_1_basic_array()
				{
					this->r = 1;
					this->g = 1;
					this->b = 1;
					this->a = 1;
				}
				virtual ~cgl_0_1_polygon(){}

				virtual cgl_0_1_polygon* ownClone()
				{
					cgl_0_1_polygon* back = new cgl_0_1_polygon();
					back->add(this->content, this->length);
					back->r = this->r;
					back->g = this->g;
					back->b = this->b;
					back->a = this->a;
					return back;
				}

				virtual cgl_0_1_polygon* depthClone()
				{
					cgl_0_1_polygon* back = new cgl_0_1_polygon();
					for(unsigned int i = 0; i < this->length; i++)
						back->add(this->content[i]->depthClone());
					back->r = this->r;
					back->g = this->g;
					back->b = this->b;
					back->a = this->a;
					return back;
				}
		};

		typedef cgl_0_1_basic_array<cgl_0_1_polygon> cgl_0_1_objectmash;

		class cgl_0_1_renderobject : public virtual cgl_0_1_cloner
		{
			public:
				cgl_0_1_objectmash* mash;
				unsigned int render_type;
				unsigned int object_id;

				cgl_0_1_renderobject(cgl_0_1_objectmash* mash, unsigned int render_type, unsigned int object_id)
				{
					this->mash = mash;
					if(mash != nullptr)
						mash->references++;
					this->render_type = render_type;
					this->object_id = object_id;
				}
				virtual ~cgl_0_1_renderobject()
				{
					if(this->mash != nullptr)
					{
						this->mash->references--;
						if(this->mash->references == 0)
							delete this->mash;
					}
				}

				virtual cgl_0_1_renderobject* ownClone();
				virtual cgl_0_1_renderobject* depthClone();
		};

		class cgl_0_1_texture : public virtual cgl_0_1_cloner
		{
            public:
                unsigned int id;
                unsigned char* colorbits;
                unsigned int width;
                unsigned int height;
                bool repeatX;
                bool repeatY;

                cgl_0_1_texture()
                {
                    this->id = 0;
                    this->colorbits = new unsigned char[4];
                    this->colorbits[0] = 255;
                    this->colorbits[1] = 255;
                    this->colorbits[2] = 255;
                    this->colorbits[3] = 255;
                    this->width = 1;
                    this->height = 1;
                    this->repeatX = false;
                    this->repeatY = false;
                }

                virtual ~cgl_0_1_texture();

                cgl_0_1_texture* ownClone()
                {
                    cgl_0_1_texture* back = new cgl_0_1_texture();
                    back->width = this->width;
                    back->height = this->height;
                    delete[] back->colorbits;
                    unsigned int j = this->height * this->width * 4;
                    back->colorbits = new unsigned char[j];
                    for(unsigned int i = 0; i < j; i++)
                        back->colorbits[i] = this->colorbits[i];
                    return back;
                }

                cgl_0_1_texture* depthClone()
                {
                    cgl_0_1_texture* back = new cgl_0_1_texture();
                    back->width = this->width;
                    back->height = this->height;
                    delete[] back->colorbits;
                    unsigned int j = this->height * this->width * 4;
                    back->colorbits = new unsigned char[j];
                    for(unsigned int i = 0; i < j; i++)
                        back->colorbits[i] = this->colorbits[i];
                    return back;
                }
		};

		class cgl_0_1_object : public virtual cgl_0_1_cloner
		{
			public:
				cgl_0_1_vertex* pos;
				cgl_0_1_vertex* roat;
				cgl_0_1_vertex* scale;

				double r;
				double g;
				double b;
				double a;

				cgl_0_1_texture* texture;

				cgl_0_1_renderobject* object;

				cgl_0_1_object(cgl_0_1_renderobject* object)
				{
					this->object = object;
					if(object != nullptr)
						this->object->references++;
					this->pos = new cgl_0_1_vertex();
					this->roat = new cgl_0_1_vertex();
					this->scale = new cgl_0_1_vertex(1.0, 1.0, 1.0);
					this->r = 1;
					this->g = 1;
					this->b = 1;
					this->a = 1;
					this->texture = nullptr;
				}
				virtual ~cgl_0_1_object()
				{
					delete this->pos;
					delete this->scale;
					delete this->roat;

					if(this->object != nullptr)
					{
						this->object->references--;
						if(this->object->references == 0)
							delete this->object;
					}

					if(this->texture != nullptr)
                    {
                        this->texture->references--;
                        if(this->texture->references == 0)
                            delete this->texture;
                    }
				}

				virtual void setTexture(cgl_0_1_texture* texture)
				{
				    if(this->texture != nullptr)
                    {
                        this->texture->references--;
                        if(this->texture->references == 0)
                            delete this->texture;
                    }
                    this->texture = texture;
                    if(this->texture != nullptr)
                        this->texture->references++;
				}

				virtual cgl_0_1_object* ownClone()
				{
					cgl_0_1_object* back = new cgl_0_1_object(this->object);
					delete back->pos;
					delete back->scale;
					delete back->roat;
					back->pos = this->pos->ownClone();
					back->scale = this->scale->ownClone();
					back->roat = this->roat->ownClone();
					if(this->texture != nullptr)
                    {
                        this->texture->references++;
                        back->texture = texture;
                    }
					return back;
				}
				virtual cgl_0_1_object* depthClone()
				{
					cgl_0_1_object* back = new cgl_0_1_object(this->object->depthClone());
					delete back->pos;
					delete back->scale;
					delete back->roat;
					back->pos = this->pos->depthClone();
					back->scale = this->scale->depthClone();
					back->roat = this->roat->depthClone();
					if(this->texture != nullptr)
                    {
                        back->texture = this->texture->depthClone();
                        back->texture->references++;
                    }
					return back;
				}
		};

		class cgl_0_1_font
		{
            public:
                cgl_0_1_font(){}
                virtual ~cgl_0_1_font(){}

                virtual cgl_0_1_texture* createTexture(std::string text, unsigned int size) = 0;
		};

		// --------------------------------------------------
		// -                 Real Functions                 -
		// --------------------------------------------------

		// Version 0_1
		bool cgl_0_1_init(unsigned int id);
		void cgl_0_1_terminate();

        void cgl_0_1_get_screen_size(unsigned int* x, unsigned int* y);

		bool cgl_0_1_window_open(unsigned int width, unsigned int height, std::string title, bool fullscreen);
		void cgl_0_1_window_get_size(unsigned int* x, unsigned int* y);
		void cgl_0_1_window_set_size(unsigned int x, unsigned int y);
		double cgl_0_1_window_update();
		void cgl_0_1_window_close();

		cgl_0_1_renderobject* cgl_0_1_create_render_object(cgl_0_1_objectmash* object, unsigned int type);

		void cgl_0_1_world_get_pos(double* x, double* y, double* z);
		void cgl_0_1_world_get_roat(double* x, double* y, double* z);
		void cgl_0_1_world_get_scale(double* x, double* y, double* z);

		void cgl_0_1_world_set_pos(double x, double y, double z);
		void cgl_0_1_world_set_roat(double x, double y, double z);
		void cgl_0_1_world_set_scale(double x, double y, double z);

        void cgl_0_1_view_port(unsigned int x, unsigned int y, unsigned int width, unsigned int height);
		void cgl_0_1_view_ortho(double x, double y, double front, double back);
		void cgl_0_1_view_frustum(double x, double y, double front, double back);

		void cgl_0_1_set_render_program(unsigned int type);

		void cgl_0_1_clear();
		void cgl_0_1_render(cgl_0_1_object* object);

        cgl_0_1_texture* cgl_0_1_import_image(std::string file);
        cgl_0_1_objectmash* cgl_0_1_import_wavefront(std::string file);
        cgl_0_1_font* cgl_0_1_import_ttf(std::string file);

		void cgl_0_1_commit_texture(cgl_0_1_texture* texture);
		void cgl_0_1_remove_texture(cgl_0_1_texture* texture);

        void cgl_0_1_get_object_color(double id, double* r, double* g, double* b);
		double cgl_0_1_get_pixel_color(unsigned int x, unsigned int y);

		int cgl_0_1_event_get_key_code(std::string name);

		void cgl_0_1_event_set_close_window(void(*call_back)());
		void cgl_0_1_event_set_keys(void(*call_back)(int key, unsigned int action));
		void cgl_0_1_event_set_mouse(void(*call_back)(unsigned int x, unsigned int y, unsigned int button, unsigned int action));
		void cgl_0_1_event_set_mouse_move(void(*call_back)(double x, double y));
		void cgl_0_1_event_set_window_resize(void(*call_back)(unsigned int x, unsigned int y));

		void cgl_0_1_event_call_close_window();
		void cgl_0_1_event_call_keys(int key, unsigned int action);
		void cgl_0_1_event_call_mouse(unsigned int x, unsigned int y, unsigned int button, unsigned int action);
		void cgl_0_1_event_call_mouse_move(double x, double y);
		void cgl_0_1_event_call_window_resize(unsigned int x, unsigned int y);

		void cgl_0_1_math_roat_vertex(cgl_0_1_vertex* vertex, double x, double y, double z);
		void cgl_0_1_math_scale_mash(cgl_0_1_objectmash* mash, double x, double y, double z);

		void cgl_0_1_get_mouse_pos(unsigned int*, unsigned int*);

		// --------------------------------------------------
		// -                  CHEATING!!!                   -
		// --------------------------------------------------

		// Version 0_1
		inline cgl_0_1_renderobject* cgl_0_1_renderobject::ownClone()
		{
			return cgl_0_1_create_render_object(this->mash, this->render_type);
		}
		inline cgl_0_1_renderobject* cgl_0_1_renderobject::depthClone()
		{
			return cgl_0_1_create_render_object(this->mash->depthClone(), this->render_type);
		}

		inline cgl_0_1_texture::~cgl_0_1_texture()
		{
		    delete[] this->colorbits;
		    if(this->id != 0)
                cgl_0_1_remove_texture(this);
		}

		static const int CGL_0_1_KEY_A = cgl_0_1_event_get_key_code("A");
		static const int CGL_0_1_KEY_D = cgl_0_1_event_get_key_code("D");
		static const int CGL_0_1_KEY_E = cgl_0_1_event_get_key_code("E");
		static const int CGL_0_1_KEY_Q = cgl_0_1_event_get_key_code("Q");
		static const int CGL_0_1_KEY_R = cgl_0_1_event_get_key_code("R");
		static const int CGL_0_1_KEY_S = cgl_0_1_event_get_key_code("S");
		static const int CGL_0_1_KEY_W = cgl_0_1_event_get_key_code("W");

		static const int CGL_0_1_KEY_SPACE = cgl_0_1_event_get_key_code("SPACE");

		static const int CGL_0_1_KEY_F1 = cgl_0_1_event_get_key_code("F1");
		static const int CGL_0_1_KEY_F2 = cgl_0_1_event_get_key_code("F2");
		static const int CGL_0_1_KEY_F3 = cgl_0_1_event_get_key_code("F3");
		static const int CGL_0_1_KEY_F4 = cgl_0_1_event_get_key_code("F4");
		static const int CGL_0_1_KEY_F5 = cgl_0_1_event_get_key_code("F5");
		static const int CGL_0_1_KEY_F6 = cgl_0_1_event_get_key_code("F6");
		static const int CGL_0_1_KEY_F7 = cgl_0_1_event_get_key_code("F7");
		static const int CGL_0_1_KEY_F8 = cgl_0_1_event_get_key_code("F8");
		static const int CGL_0_1_KEY_F9 = cgl_0_1_event_get_key_code("F9");
		static const int CGL_0_1_KEY_F10 = cgl_0_1_event_get_key_code("F10");
		static const int CGL_0_1_KEY_F11 = cgl_0_1_event_get_key_code("F11");
		static const int CGL_0_1_KEY_F12 = cgl_0_1_event_get_key_code("F12");
	}
}


#endif /* FUNCTIONS_H_ */
