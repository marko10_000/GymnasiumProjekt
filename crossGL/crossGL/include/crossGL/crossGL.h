/*
 * screen.h
 *
 *  Created on: 12.02.2014
 *      Author: marko
 */

#ifndef SCREEN_H_
#define SCREEN_H_

#include<crossGL/_real.h>

/**
 * This namespace contains everything of the lib crossGL.
 */
namespace crossGL
{
    /**
     * This namespace contains the important functions and vars to start/stop the lib and to manage the window.
     */
	namespace screen
	{
		// --------------------------------------------------
		// -                   Constances                   -
		// --------------------------------------------------

		static const unsigned int CGL_RENDER_DEFAULT = crossGL::_funcs::CGL_0_1_RENDER_DEFAULT; /** Default render mechanism. (OpenGL 3.3) */
		static const unsigned int CGL_RENDER_OPENGL = crossGL::_funcs::CGL_0_1_RENDER_OPENGL; /** Default OpenGL render mechanism. (OpenGL 3.3) */
		static const unsigned int CGL_RENDER_OPENGL_2_1 = crossGL::_funcs::CGL_0_1_RENDER_OPENGL_2_1; /** Render mechanism OpenGL 2.1 */
		static const unsigned int CGL_RENDER_OPENGL_3_3 = crossGL::_funcs::CGL_0_1_RENDER_OPENGL_3_3; /** Render mechanism OpenGL 3.3 */
		static const unsigned int CGL_RENDER_OPENGL_4_3 = crossGL::_funcs::CGL_0_1_RENDER_OPENGL_4_3; /** Render mechanism OpenGL 4.3 */
		// DirectX comes later! (not in this Project)

		// --------------------------------------------------
		// -                      Alias                     -
		// --------------------------------------------------

        /**
         * Init the lib.
         * @param[in] render Set the render mechanism in the lib.
         * @return Returns true when the lib was successfully inited, false when not.
         */
		inline bool cglInit(unsigned int render) { return crossGL::_funcs::cgl_0_1_init(render); };
		/**
		 * Terminate the lib.
		 */
		inline void cglTerminate() { crossGL::_funcs::cgl_0_1_terminate(); }

        /**
         * Find the screen size of the monitor out.
         * @param[out] x The x size
         * @param[out] y The y size
         */
        inline void cglGetScreenSize(unsigned int& x, unsigned int& y) { crossGL::_funcs::cgl_0_1_get_screen_size(&x, &y); }

        /**
         * Open the window.
         * @param[in] width X-size of the window.
         * @param[in] height Y-size of the window.
         * @param[in] title Name of the window.
         * @param[in] fullscreen True when the window should be open in fullscreen mode, false when it should be a normal window.
         * @return True when window was open successfully, false when not.
         */
		inline bool cglWindowOpen(unsigned int width, unsigned int height, std::string title, bool fullscreen) { return crossGL::_funcs::cgl_0_1_window_open(width, height, title, fullscreen); }
		/**
         * Find the window size out.
         * @param[out] x The x size
         * @param[out] y The y size
         */
		inline void cglGetWindowSize(unsigned int& x, unsigned int& y) { crossGL::_funcs::cgl_0_1_window_get_size(&x, &y); }
		/**
         * Resize the window.
         * @param[in] x The x size
         * @param[in] y The y size
         */
		inline void cglSetWindowSize(unsigned int x, unsigned int y) { crossGL::_funcs::cgl_0_1_window_set_size(x, y); }
		/**
		 * Push the rendered image to the screen and updates the events.
		 * @return the time in seconds when the last update was.
		 */
		inline double cglWindowUpdate() { return crossGL::_funcs::cgl_0_1_window_update(); }
		/**
		 * Close the window.
		 */
		inline void cglWindowClose() { crossGL::_funcs::cgl_0_1_window_close(); }
	}

    /**
     * This namespace contains important functions and vars to render objects in the GPU.
     */
	namespace render
	{
		static const unsigned int CGL_RENDER_TYPE_DEFAULT = crossGL::_funcs::CGL_0_1_OBJECT_RENDER_TYPE_POLYGON; /** Normal mode for creating a model (surfaces) */
		static const unsigned int CGL_RENDER_TYPE_POINT = crossGL::_funcs::CGL_0_1_OBJECT_RENDER_TYPE_POINT; /** Create a model just with points at the corners */
		static const unsigned int CGL_RENDER_TYPE_LINE = crossGL::_funcs::CGL_0_1_OBJECT_RENDER_TYPE_LINE; /** Create a model just with lines at the edges */
		static const unsigned int CGL_RENDER_TYPE_POLYGON = crossGL::_funcs::CGL_0_1_OBJECT_RENDER_TYPE_POLYGON; /** Create a model with surfaces */

		static const unsigned int CGL_RENDER_FUNC_NORMAL = crossGL::_funcs::CGL_0_1_RENDER_FUNC_NORMAL; /** Normal render function **/
		static const unsigned int CGL_RENDER_FUNC_OBJECT_COLOR = crossGL::_funcs::CGL_0_1_RENDER_FUNC_OBJECT_COLOR; /** This function only draw the color of the object **/
		static const unsigned int CGL_RENDER_FUNC_POLYGON_COLOR = crossGL::_funcs::CGL_0_1_RENDER_FUNC_POLYGON_COLOR; /** This function only draw the color of the polygons **/

		typedef crossGL::_funcs::cgl_0_1_vertex Vertex; /** A point */
		typedef crossGL::_funcs::cgl_0_1_polygon Polygon; /** A array of points (polygon) */
		typedef crossGL::_funcs::cgl_0_1_objectmash ObjectMash; /** A array of polygons (the mash of the object) */
		typedef crossGL::_funcs::cgl_0_1_renderobject ObjectRender; /** A mash that is uploaded to the GPU */
		typedef crossGL::_funcs::cgl_0_1_texture Texture; /** The texture of an object */
		typedef crossGL::_funcs::cgl_0_1_object Object; /** The object that you can see in the game */

        /**
         * Clean the render screen.
         * (Reset of the picture that will be shown in the window)
         */
		inline void cglClean() { crossGL::_funcs::cgl_0_1_clear(); }
		/**
		 * Draw a object.
		 * @param[in] object The object that should be drawn.
		 */
		inline void cglRender(Object* object) { crossGL::_funcs::cgl_0_1_render(object); }

        /**
         * Upload the mash of a object to the GPU.
         * @param[in] mash The mash of the object that should the uploaded to the GPU.
         * @param[in] type The type that mash should get, like surfaces, edges or points
         * @return Return the uploaded ObjectMash
         */
		inline ObjectRender* cglCreateRenderObject(ObjectMash* mash, unsigned int type) { return crossGL::_funcs::cgl_0_1_create_render_object(mash, type); }

        /**
         * Returns the position of the camera to the given var
         * @param[out] x The x-position.
         * @param[out] y The y-position.
         * @param[out] z The z-position.
         */
		inline void cglWorldGetPos(double& x, double& y, double& z) { crossGL::_funcs::cgl_0_1_world_get_pos(&x, &y, &z); }
		/**
         * Returns the rotation of the camera to the given var
         * @param[out] x The x-rotation.
         * @param[out] y The y-rotation.
         * @param[out] z The z-rotation.
         */
		inline void cglWorldGetRoat(double& x, double& y, double& z) { crossGL::_funcs::cgl_0_1_world_get_roat(&x, &y, &z); }
		/**
         * Returns the scaling of the camera to the given var
         * @param[out] x The x-scaling.
         * @param[out] y The y-scaling.
         * @param[out] z The z-scaling.
         */
		inline void cglWorldGetScale(double& x, double& y, double& z) { crossGL::_funcs::cgl_0_1_world_get_scale(&x, &y, &z); }

        /**
         * Set the position of the camera
         * @param[in] x The x-position
         * @param[in] y The y-position
         * @param[in] z The z-position
         */
		inline void cglWorldSetPos(double x, double y, double z) { crossGL::_funcs::cgl_0_1_world_set_pos(x, y, z); }
		/**
         * Set the rotation of the camera
         * @param[in] x The x-rotation
         * @param[in] y The y-rotation
         * @param[in] z The z-rotation
         */
		inline void cglWorldSetRoat(double x, double y, double z) { crossGL::_funcs::cgl_0_1_world_set_roat(x, y, z); }
		/**
         * Set the scaling of the camera
         * @param[in] x The x-scaling
         * @param[in] y The y-scaling
         * @param[in] z The z-scaling
         */
		inline void cglWorldSetScale(double x, double y, double z) { crossGL::_funcs::cgl_0_1_world_set_scale(x, y, z); }

        /**
         * Set the render position.
         * @param[in] x The x position (from the left border)
         * @param[in] y The y position (from the top border)
         * @param[in] width The x size
         * @param[in] height The y size
         */
        inline void cglViewPort(unsigned int x, unsigned int y, unsigned int width, unsigned int height) { crossGL::_funcs::cgl_0_1_view_port(x, y, width, height); }
        /**
         * Make a direct view.
         * @param[in] x The x-Size
         * @param[in] y The y-Size
         * @param[in] front The start of the clipping layer
         * @param[in] back The end of the clipping layer
         */
		inline void cglViewOrtho(double x, double y, double front, double back) { crossGL::_funcs::cgl_0_1_view_ortho(x, y, front, back); }
		/**
		 * Make a perspective view.
		 * @param[in] x The x-Size
		 * @param[in] y The y-Size
		 * @param[in] front The start of the clipping layer
		 * @param[in] back The end of the clipping layer
		 */
		inline void cglViewFrustum(double x, double y, double front, double back) { crossGL::_funcs::cgl_0_1_view_frustum(x, y, front, back); }

        /**
         * Set the function that runs the render commands on the GPU.
         * @param[in] func The function type
         */
		inline void cglSetRenderFunc(unsigned int func) { crossGL::_funcs::cgl_0_1_set_render_program(func); }

        /**
         * Create or update a texture in the GPU memory
         * @param[in,out] texture The texture that should be created or updated
         */
		inline void cglCommitTexture(Texture* texture) { crossGL::_funcs::cgl_0_1_commit_texture(texture); }
		/**
		 * Remove a texture from the GPU memory
		 * @param[in,out] texture The texture that should be removed
		 */
		inline void cglRemoveTexture(Texture* texture) { crossGL::_funcs::cgl_0_1_remove_texture(texture); }

		/**
		 * Get the ID of a pixel.
		 * @param[in] x The x position (from left)
		 * @param[in] y The y position (from top)
		 * @return Return the color id of the object (0 <= id < 1)
		 */
		inline double cglGetPixelID(unsigned int x, unsigned int y) { return crossGL::_funcs::cgl_0_1_get_pixel_color(x, y); }
		/**
		 * Set the color id of an object, polygon, ...
		 * @param[in] id The id of the object, polygon, ... (0 <= id < 1)
		 * @param[out] r The address of the red color
		 * @param[out] g The address of the green color
		 * @param[out] b The address of the blue color
		 */
		inline void cglSetColorID(double id, double& r, double& g, double& b) { crossGL::_funcs::cgl_0_1_get_object_color(id, &r, &g, &b); }
	}

    /**
     * This namespace contains important definitions and functions to handle events.
     */
	namespace events
	{
	    static const unsigned int CGL_EVENT_KEY_DOWN = crossGL::_funcs::CGL_0_1_EVENT_KEY_DOWN; /** Key pressed */
	    static const unsigned int CGL_EVENT_KEY_REPEAT = crossGL::_funcs::CGL_0_1_EVENT_KEY_REPEAT; /** Key repeated by the system */
	    static const unsigned int CGL_EVENT_KEY_UP = crossGL::_funcs::CGL_0_1_EVENT_KEY_UP; /** Key released */

	    static const int CGL_KEY_A = crossGL::_funcs::CGL_0_1_KEY_A; /** Key code for the key A */
	    static const int CGL_KEY_D = crossGL::_funcs::CGL_0_1_KEY_D; /** Key code for the key D */
	    static const int CGL_KEY_E = crossGL::_funcs::CGL_0_1_KEY_E; /** Key code for the key E */
	    static const int CGL_KEY_Q = crossGL::_funcs::CGL_0_1_KEY_Q; /** Key code for the key Q */
	    static const int CGL_KEY_R = crossGL::_funcs::CGL_0_1_KEY_R; /** Key code for the key R */
	    static const int CGL_KEY_S = crossGL::_funcs::CGL_0_1_KEY_S; /** Key code for the key S */
	    static const int CGL_KEY_W = crossGL::_funcs::CGL_0_1_KEY_W; /** Key code for the key W */

	    static const int CGL_KEY_SPACE = crossGL::_funcs::CGL_0_1_KEY_SPACE; /** Key code for the key Space */

	    static const int CGL_KEY_F1 = crossGL::_funcs::CGL_0_1_KEY_F1; /** Key code for the key F1 */
	    static const int CGL_KEY_F2 = crossGL::_funcs::CGL_0_1_KEY_F2; /** Key code for the key F2 */
	    static const int CGL_KEY_F3 = crossGL::_funcs::CGL_0_1_KEY_F3; /** Key code for the key F3 */
	    static const int CGL_KEY_F4 = crossGL::_funcs::CGL_0_1_KEY_F4; /** Key code for the key F4 */
	    static const int CGL_KEY_F5 = crossGL::_funcs::CGL_0_1_KEY_F5; /** Key code for the key F5 */
	    static const int CGL_KEY_F6 = crossGL::_funcs::CGL_0_1_KEY_F6; /** Key code for the key F6 */
	    static const int CGL_KEY_F7 = crossGL::_funcs::CGL_0_1_KEY_F7; /** Key code for the key F7 */
	    static const int CGL_KEY_F8 = crossGL::_funcs::CGL_0_1_KEY_F8; /** Key code for the key F8 */
	    static const int CGL_KEY_F9 = crossGL::_funcs::CGL_0_1_KEY_F9; /** Key code for the key F9 */
	    static const int CGL_KEY_F10 = crossGL::_funcs::CGL_0_1_KEY_F10; /** Key code for the key F10 */
	    static const int CGL_KEY_F11 = crossGL::_funcs::CGL_0_1_KEY_F11; /** Key code for the key F11 */
	    static const int CGL_KEY_F12 = crossGL::_funcs::CGL_0_1_KEY_F12; /** Key code for the key F12 */

	    static const unsigned int CGL_EVENT_MOUSE_DOWN = crossGL::_funcs::CGL_0_1_EVENT_MOUSE_BUTTON_DOWN; /** Mouse button was pressed */
	    static const unsigned int CGL_EVENT_MOUSE_UP = crossGL::_funcs::CGL_0_1_EVENT_MOUSE_BUTTON_UP; /** Mouse button was released */

	    static const unsigned int CGL_MOUSE_LEFT = crossGL::_funcs::CGL_0_1_EVENT_MOUSE_CLICK_LEFT; /** Left mouse button */
	    static const unsigned int CGL_MOUSE_CENTER = crossGL::_funcs::CGL_0_1_EVENT_MOUSE_CLICK_CENTER; /** Left mouse button */
	    static const unsigned int CGL_MOUSE_RIGHT = crossGL::_funcs::CGL_0_1_EVENT_MOUSE_CLICK_RIGHT; /** Left mouse button */

	    /**
	     * The function type that should be used when you want to set the window close event.
	     */
	    typedef void (*EventCloseWindow)();
	    /**
	     * The function type that should be used when you want to set the key event.
	     * @param[in] key_code The number of the key
	     * @param[in] action What was happen with this key
	     */
	    typedef void (*EventKeys)(int key_code, unsigned int action);
	    /**
	     * The function type that should be used when you want to set the mouse event.
	     * @param[in] x The x position of the cursor in pixel (from the left)
	     * @param[in] y The y position of the cursor in pixel (from the top)
	     * @param[in] button The button that was pressed
	     * @param[in] action If the button was pressed or released
	     */
	    typedef void (*EventMouse)(unsigned int x, unsigned int y, unsigned int button, unsigned int action);
	    /**
	     * The function type that should be used when you want to set the mouse move event.
	     * @param[in] x The relative x direction movement
	     * @param[in] y The relative y direction movement
	     */
	    typedef void (*EventMouseMove)(double x, double y);
        /**
         * The function type that should be used when you want to set the window resize event.
         * @param[in] x The x size of the window in pixel
         * @param[in] y The y size of the window in pixel
         */
	    typedef void (*EventWindowResize)(unsigned int x, unsigned int y);

        /**
         * Set the window close event.
         * @param[in] call_back The function that should be called when the window will be closed. You can deactivate it with nullptr
         */
	    inline void cglEventSetCloseWindow(EventCloseWindow call_back) { crossGL::_funcs::cgl_0_1_event_set_close_window(call_back); }
	    /**
	     * Set the key event.
	     * @param[in] call_back The function that should be called when a key is pressed, repeated by the system or released
	     */
	    inline void cglEventSetKeys(EventKeys call_back) { crossGL::_funcs::cgl_0_1_event_set_keys(call_back); }
	    /**
	     * Set the mouse event.
	     * @param[in] call_back The function that should be called when a mouse button is pressed or released
	     */
	    inline void cglEventSetMouse(EventMouse call_back) { crossGL::_funcs::cgl_0_1_event_set_mouse(call_back); }
	    /**
	     * Set the mouse move event
	     * @param[in] call_back The function that should be called when the mouse is moved
	     */
	    inline void cglEventSetMouseMove(EventMouseMove call_back) { crossGL::_funcs::cgl_0_1_event_set_mouse_move(call_back); }
	    /**
	     * Set the window resize event.
	     * @param[in] call_back The function that should be called when the window will be resized
	     */
	    inline void cglEventSetResizeWindow(EventWindowResize call_back) { crossGL::_funcs::cgl_0_1_event_set_window_resize(call_back); }

        /**
         * Trigger a window close event.
         */
	    inline void cglEventCallCloseWindow() { crossGL::_funcs::cgl_0_1_event_call_close_window(); }
	    /**
	     * Trigger a key event.
	     * @param[in] key Which key
	     * @param[in] action What happened to the key
	     */
	    inline void cglEventCallKeys(int key, unsigned int action) { crossGL::_funcs::cgl_0_1_event_call_keys(key, action); }
	    /**
	     * Trigger a mouse event.
	     * @param[in] x The x position of the mouse in pixel (from left)
	     * @param[in] y The y position of the mouse in pixel (from top)
	     * @param[in] button The button that was pressed (virtual)
	     * @param[in] action If the button was pressed/released (virtual)
	     */
	    inline void cglEventCallMouse(unsigned int x, unsigned int y, unsigned int button, unsigned int action) { crossGL::_funcs::cgl_0_1_event_call_mouse(x, y, button, action); }
	    /**
	     * Trigger a mouse move event.
	     * @param[in] x The x movement
	     * @param[in] y The y movement
	     */
	    inline void cglEventCallMouseMove(double x, double y) { crossGL::_funcs::cgl_0_1_event_call_mouse_move(x, y); }
	    /**
	     * Trigger a window resize event.
	     * @param[in] x The x size of the window
	     * @param[in] y The y size of the window
	     */
	    inline void cglEventCallResizeWindow(unsigned int x, unsigned int y) { crossGL::_funcs::cgl_0_1_event_call_window_resize(x, y); }

	    /**
	     * Return the current cursor position.
	     * @param[out] x The x position
	     * @param[out] y The y position
	     */
	    inline void cglGetMousePos(unsigned int& x, unsigned int& y) { crossGL::_funcs::cgl_0_1_get_mouse_pos(&x, &y); }
	}

	/**
	 * This namespace contains importers for textures and models.
	 */
	namespace imports
	{
	    typedef crossGL::_funcs::cgl_0_1_font Font; /** A Font */

	    /**
	     * Load a image from the filesystem.
	     * @param[in] file The file name
	     * @return The texture or nullptr when something went wrong
	     */
	    inline crossGL::render::Texture* cglImportImage(std::string file) { return crossGL::_funcs::cgl_0_1_import_image(file); }

	    /**
	     * Load a wavefront object from the filesystem
	     * @param[in] file The file name
	     * @return The mash or nullptr when something went wrong
	     */
	    inline crossGL::render::ObjectMash* cglImportWavefront(std::string file) { return crossGL::_funcs::cgl_0_1_import_wavefront(file); }

	    /**
	     * Import a TrueTypeFont.
	     * @param[in] file The source file
	     * @return The font object
	     */
        inline Font* cglImportTTF(std::string file){ return crossGL::_funcs::cgl_0_1_import_ttf(file); }
	}

	/**
	 * This namespace should help with math operations.
	 */
	namespace math
	{
	    /**
	     * Roat a point with the angles x, y and z.
	     * @param[in,out] vertex The vertex which should be rotated
	     * @param[in] x The x angle
	     * @param[in] y The y angle
	     * @param[in] z The z angle
	     */
	    inline void cglRoatVertex(crossGL::render::Vertex* vertex, double x, double y, double z) { crossGL::_funcs::cgl_0_1_math_roat_vertex(vertex, x, y, z); }
	    /**
	     * Scale a mash.
	     * @param[in,out] mash The mash that should be scaled
	     * @param[in,out] x The x size
	     * @param[in,out] y The y size
	     * @param[in,out] z The z size
	     */
        inline void cglScaleMash(crossGL::render::ObjectMash* mash, double x, double y, double z) { crossGL::_funcs::cgl_0_1_math_scale_mash(mash, x, y, z); }
	}
}


#endif /* SCREEN_H_ */
