#include <GL/glew.h>
#include <GL/gl.h>
#include <FreeImage.h>
#include <iostream>

#include <crossGL/_real.h>

using namespace crossGL::_funcs;
using namespace std;

extern unsigned int window_x;
extern unsigned int window_y;

void crossGL::_funcs::cgl_0_1_commit_texture(cgl_0_1_texture* texture)
{
    if(texture == nullptr)
        return;

    unsigned int& textureID = texture->id;
    if(textureID == 0)
        glGenTextures(1, &textureID);

    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture->width, texture->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture->colorbits);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, texture->repeatX ? GL_REPEAT : GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, texture->repeatY ? GL_REPEAT : GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glGenerateMipmap(GL_TEXTURE_2D);
	glFlush();
}


void crossGL::_funcs::cgl_0_1_remove_texture(cgl_0_1_texture* texture)
{
    if(texture == nullptr)
        return;
    if(texture->id == 0)
        return
    glDeleteTextures(1, &(texture->id));
}

double crossGL::_funcs::cgl_0_1_get_pixel_color(unsigned int x, unsigned int y)
{
    glFlush();
    glFinish();

    glPixelStoref(GL_PACK_ALIGNMENT,1);
    unsigned char* data = new unsigned char[3];
    glReadPixels(x, window_y - y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, data);
    double back = -1;
    if((data[0] == 255) && (data[2] == 0))
        // 0 <= id <= 1
        back = (double)(data[1]) / 255.0f;
    else if((data[1] == 255) && (data[2] == 0))
        // 1 < id <= 2
        back = 2.0f - ((double)(data[0]) / 255.0f);
    else if((data[1] == 255) && (data[0] == 0))
        // 2 < id <= 3
        back = (double)(data[2]) / 255.0f + 2.0f;
    else if((data[2] == 255) && (data[0] == 0))
        // 3 < id <= 4
        back = 4.0f - ((double)(data[1]) / 255.0f);
    else if((data[2] == 255) && (data[1] == 0))
        // 4 < id <= 5
        back = (double)(data[0]) / 255.0f + 4.0f;
    else if((data[0] == 255) && (data[1] == 0))
        // 5 < id < 6
        back = 6.0f - ((double)(data[2]) / 255.0f);
    delete[] data;
    if(back == -1)
        return -1;
    return back / 6.0;
}

void crossGL::_funcs::cgl_0_1_get_object_color(double id, double* r, double* g, double* b)
{
    id *= 6.0f;
    if(id <= 1)
    {
        (*r) = 1.0f;
        (*g) = id;
        (*b) = 0.0f;
    }
    else if(id <= 2)
    {
        (*r) = 2.0f - id;
        (*g) = 1.0f;
        (*b) = 0.0f;
    }
    else if(id <= 3)
    {
        (*r) = 0.0f;
        (*g) = 1.0f;
        (*b) = id - 2.0f;
    }
    else if(id <= 4)
    {
        (*r) = 0.0f;
        (*g) = 4.0f - id;
        (*b) = 1.0f;
    }
    else if(id <= 5)
    {
        (*r) = id - 4.0f;
        (*g) = 0.0f;
        (*b) = 1.0f;
    }
    else if(id <= 6)
    {
        (*r) = 1.0f;
        (*g) = 0.0f;
        (*b) = 6.0f - id;
    }
    else
    {
        (*r) = 0.0f;
        (*g) = 0.0f;
        (*b) = 0.0f;
    }
}

cgl_0_1_texture* crossGL::_funcs::cgl_0_1_import_image(std::string file)
{
    FIBITMAP* bitmap = FreeImage_Load(FreeImage_GetFileType(file.c_str()), file.c_str(), PNG_DEFAULT);
    if(bitmap == nullptr)
        return nullptr;
    FIBITMAP *pImage = FreeImage_ConvertTo32Bits(bitmap);
    FreeImage_Unload(bitmap);

    cgl_0_1_texture* back = new cgl_0_1_texture();
    back->width = FreeImage_GetWidth(pImage);
    back->height = FreeImage_GetHeight(pImage);
    cout << back->width << "|" << back->height << endl;
    delete[] back->colorbits;

    unsigned char* colorbits = (unsigned char*)FreeImage_GetBits(pImage);
    unsigned int j = 4 * back->width * back->height;
    back->colorbits = new unsigned char[j];
    for(unsigned int i = 0; i < j; i += 4)
    {
        back->colorbits[i] = colorbits[i + 2];
        back->colorbits[i + 1] = colorbits[i + 1];
        back->colorbits[i + 2] = colorbits[i];
        back->colorbits[i + 3] = colorbits[i + 3];
    }

    FreeImage_Unload(pImage);

    return back;
}
