#include <crossGL/_real.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <freetype2/ft2build.h>
#include <freetype2/freetype/freetype.h>
#include <freetype2/freetype/ftglyph.h>

using namespace std;
using namespace crossGL::_funcs;

extern FT_Library ft_library;

double string_to_double(std::string s)
{
    stringstream i(s);
    double x;
    if (!(i >> x))
        return 0.0f;
    return x;
}

int string_to_int(std::string s)
{
    stringstream i(s);
    int x;
    if (!(i >> x))
        return 0;
    return x;
}

cgl_0_1_objectmash* crossGL::_funcs::cgl_0_1_import_wavefront(string file)
{
    string file_content = "";
	char fchar;
	ifstream input_stream;
	input_stream.open(file, ios::in | ios::binary);
	if(!input_stream.is_open())
	{
		cerr << "Can not load " << file << "." << endl;
		return nullptr;
	}
	while(!input_stream.eof())
	{
		fchar = input_stream.get();
		file_content += fchar;
	}
	input_stream.close();

    stringstream memstream(file_content);

    cgl_0_1_polygon* points = new cgl_0_1_polygon();
    cgl_0_1_polygon* uv_points = new cgl_0_1_polygon();
    cgl_0_1_objectmash* back = new cgl_0_1_objectmash();

    string line;
    while(!memstream.eof())
    {
        getline(memstream, line);
        if(line.size() > 2)
        {
            if(line[0] == 'v')
            {
                if(line[1] == 't')
                {
                    int pos1 = line.find(" ", 3);
                    double x = string_to_double(line.substr(3, pos1 - 3));
                    double y = string_to_double(line.substr(pos1 + 1));
                    uv_points->add(new cgl_0_1_vertex(x, y, 0));
                }
                else
                {
                    int pos1 = line.find(" ", 2);
                    double x = string_to_double(line.substr(2, pos1 - 2));
                    int pos2 = line.find(" ", pos1 + 1);
                    double y = string_to_double(line.substr(pos1 + 1, pos2 - pos1 - 2));
                    double z = string_to_double(line.substr(pos2 + 1));
                    points->add(new cgl_0_1_vertex(x, y, z));
                }
            }
            else if(line[0] == 'f')
            {
                string sub_line = line.substr(2);
                string::size_type pos = 0;
                int** poses = new int*[0];
                unsigned int length = 0;
                do
                {
                    if(pos != 0)
                        pos++;
                    string::size_type last;
                    if((last = sub_line.find(" ", pos)) == string::npos)
                        last = string::npos;
                    else
                        last = last - pos;
                    string sub_sub_line = sub_line.substr(pos,  last);
                    int pos2 = sub_sub_line.find("/", 0);
                    int v = string_to_int(sub_sub_line.substr(0, pos2));
                    int vt = string_to_int(sub_sub_line.substr(pos2 + 1));

                    int** poses2 = new int*[length + 1];
                    for(unsigned int i = 0; i < length; i++)
                        poses2[i] = poses[i];
                    delete[] poses;
                    poses = poses2;
                    poses[length] = new int[2];
                    poses[length][0] = v;
                    poses[length][1] = vt;
                    length++;
                } while((pos = sub_line.find(" ", pos)) != string::npos);
                cgl_0_1_polygon* polygon_ = new cgl_0_1_polygon();
                for(unsigned int i = 0; i < length; i++)
                {
                    cgl_0_1_vertex* vertex = points->content[poses[i][0] - 1];
                    cgl_0_1_vertex* tex_vertex = uv_points->content[poses[i][1] - 1];
                    polygon_->add(new cgl_0_1_vertex(vertex->x, vertex->y, vertex->z, tex_vertex->x, tex_vertex->y));
                    delete[] poses[i];
                }
                delete[] poses;
                back->add(polygon_);
            }
        }
    }
    delete uv_points;
    delete points;

    return back;
}

class cgl_FT_Font : public cgl_0_1_font
{
    protected:
        unsigned char** datas;
        unsigned int* width;
        unsigned int* height;
        unsigned int* top;
        unsigned int length;
        unsigned int space;

        virtual void draw_bitmap(FT_Bitmap* bitmap, int top, int left)
        {
            if(left > 0)
                left = -left;

            unsigned int x = bitmap->width - left;
            unsigned int y = bitmap->rows;
            unsigned char* data;
            if((x == 0) && (y == 0))
            {
                data = new unsigned char[this->space];
                for(unsigned int i = 0; i < this->space; i++)
                    data[i] = 0;
            }
            else
                data = new unsigned char[x * y];


            unsigned char** datas2 = new unsigned char*[this->length + 1];
            unsigned int* width2 = new unsigned int[this->length + 1];
            unsigned int* height2 = new unsigned int[this->length + 1];
            unsigned int* top2 = new unsigned int[this->length + 1];
            for(unsigned int i = 0; i < this->length; i++)
            {
                datas2[i] = this->datas[i];
                width2[i] = this->width[i];
                height2[i] = this->height[i];
                top2[i] = this->top[i];
            }
            datas2[this->length] = data;
            if((x == 0) && (y == 0))
            {
                width2[this->length] = this->space;
                height2[this->length] = 1;
            }
            else
            {
                width2[this->length] = x;
                height2[this->length] = y;
            }
            top2[this->length] = top;
            delete[] this->datas;
            delete[] this->width;
            delete[] this->height;
            delete[] this->top;
            this->datas = datas2;
            this->width = width2;
            this->height = height2;
            this->top = top2;
            this->length++;

            for(unsigned int i = 0; i < y; i++)
                if((int)i >= bitmap->rows)
                    for(unsigned int j = 0; j < x; j++)
                        data[i * x + j] = 0;
                else
                    for(unsigned int j = 0; j < x; j++)
                        if((int)j < -left)
                            data[i * x + j] = 0;
                        else
                            data[i * x + j] = bitmap->buffer[i * bitmap->width + (j + left)];
        }

        virtual cgl_0_1_texture* renderText()
        {
            unsigned int x = 0;
            unsigned int max_top = 0;
            for(unsigned int i = 0; i < this->length; i++)
            {
                x += this->width[i];
                if(max_top < this->top[i])
                    max_top = this->top[i];
            }
            unsigned int max_y = 0;
            for(unsigned int i = 0; i < this->length; i++)
            {
                if(max_y < this->height[i] + max_top - this->top[i])
                    max_y = this->height[i] + max_top - this->top[i];
            }

            cgl_0_1_texture* back = new cgl_0_1_texture();
            back->width = x;
            back->height = max_y;
            delete[] back->colorbits;
            unsigned int j = x * max_y * 4;
            back->colorbits = new unsigned char[j];
            for(unsigned int i = 0; i < j; i += 4)
            {
                back->colorbits[i] = 255;
                back->colorbits[i + 1] = 255;
                back->colorbits[i + 2] = 255;
                back->colorbits[i + 3] = 0;
            }

            unsigned int pos_x = 0;
            for(unsigned int i = 0; i < this->length; i++)
            {
                for(unsigned int j = 0; j < this->height[i]; j++)
                {
                    for(unsigned int k = 0; k < this->width[i]; k++)
                        if(this->datas[i][j * this->width[i] + k] > 0)
                            cout << "#";
                        else
                            cout << ".";
                    cout << endl;
                }
                cout << endl;

                for(unsigned int j = 0; j < this->height[i]; j++)
                    for(unsigned int k = 0; k < this->width[i]; k++)
                        back->colorbits[4 * ((j + max_top - this->top[i]) * x + k + pos_x) + 3] = this->datas[i][j * this->width[i] + k];
                pos_x += this->width[i];
            }

            for(unsigned int i = 0; i < max_y; i++)
            {
                for(unsigned int j = 0; j < x; j++)
                    if(back->colorbits[4 * (i * x + j) + 3] > 0)
                        cout << "#";
                    else
                        cout << ".";
                cout << endl;
            }

            return back;
        }

    public:
        FT_Face face;

        cgl_FT_Font(){}
        virtual ~cgl_FT_Font()
        {
            FT_Done_Face(this->face);
        }

        virtual cgl_0_1_texture* createTexture(std::string text, unsigned int size)
        {
            if(size == 0)
                return nullptr;
            if(FT_Set_Pixel_Sizes(this->face, size, size))
                return nullptr;

            FT_GlyphSlot slot = this->face->glyph;
            this->datas = new unsigned char*[0];
            this->width = new unsigned int[0];
            this->height = new unsigned int[0];
            this->top = new unsigned int[0];
            this->length = 0;
            this->space = size / 3;

            unsigned int j = text.size();
            for(unsigned int i = 0; i < j; i++)
            {
                FT_Load_Char(face, text[i], FT_LOAD_RENDER);
                this->draw_bitmap(&slot->bitmap, slot->bitmap_top, slot->bitmap_left);
            }

            cgl_0_1_texture* back = this->renderText();

            for(unsigned int i = 0; i < this->length; i++)
                delete[] this->datas[i];
            delete[] this->datas;
            delete[] this->width;
            delete[] this->height;
            delete[] this->top;

            return back;
        }
};

cgl_0_1_font* crossGL::_funcs::cgl_0_1_import_ttf(string file)
{
    if(file == "")
        return nullptr;
    cgl_FT_Font* back = new cgl_FT_Font();
    if(FT_New_Face(ft_library, file.c_str(), 0, &back->face))
    {
        delete back;
        return nullptr;
    }
    return back;
}
