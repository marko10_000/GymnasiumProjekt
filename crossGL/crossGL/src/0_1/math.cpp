#define GLM_FORCE_RADIANS

#include <crossGL/_real.h>
#include <math.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

using namespace crossGL::_funcs;
using namespace glm;
using namespace std;

void crossGL::_funcs::cgl_0_1_math_roat_vertex(cgl_0_1_vertex* vertex, double x, double y, double z)
{
    float x_ = x * M_PI / 180;
    float y_ = y * M_PI / 180;
    float z_ = z * M_PI / 180;

    quat quaternion = quat(vec3(x_, y_, z_));
    mat4 matrix = toMat4(quaternion);
    vec4 point = vec4(vertex->x, vertex->y, vertex->z, 0.0f) * matrix;

    vertex->x = point.x;
    vertex->y = point.y;
    vertex->z = point.z;
}

void crossGL::_funcs::cgl_0_1_math_scale_mash(cgl_0_1_objectmash* mash, double x, double y, double z)
{
    if(mash == nullptr)
        return;
    for(unsigned int i = 0; i < mash->length; i++)
    {
        cgl_0_1_polygon* polygon = mash->content[i];
        if(polygon == nullptr)
            continue;
        for(unsigned int j = 0; j < polygon->length; j++)
        {
            cgl_0_1_vertex* vertex = polygon->content[j];
            if(vertex == nullptr)
                continue;
            vertex->x *= x;
            vertex->y *= y;
            vertex->z *= z;
        }
    }
}
