#define GLM_FORCE_RADIANS

#include <GL/glew.h>
#include <GL/gl.h>
#include <math.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include <crossGL/_real.h>

using namespace crossGL::_funcs;
using namespace glm;

int program;
int object_color;
int object_pos;
int object_roat;
int object_scale;
int object_texture;

int world_pos;
int world_roat;
int world_scale;

int view_matrix;

extern unsigned int* programs;
extern unsigned int programs_length;

extern int* programs_object_color;
extern int* programs_object_pos;
extern int* programs_object_roat;
extern int* programs_object_scale;
extern int* programs_object_texture;

extern int* programs_world_pos;
extern int* programs_world_roat;
extern int* programs_world_scale;

extern int* programs_view_matrix;

float* world_pos_data = (float*)((float[3]){0.0f, 0.0f, 0.0f});
float* world_roat_data = (float*)((float[3]){0.0f, 0.0f, 0.0f});
float* world_roat_shader = (float*)((float[9]){1.0f, 0.0f, 0.0f,
                                                 0.0f, 1.0f, 0.0f,
                                                 0.0f, 0.0f, 1.0f});
float* world_scale_data = (float*)((float[3]){1.0f, 1.0f, 1.0f});

float* view_matrix_data = (float*)((float[16]){1.0f, 0.0f, 0.0f, 0.0f,
                                               0.0f, 1.0f, 0.0f, 0.0f,
                                               0.0f, 0.0f, 1.0f, 0.0f,
                                               0.0f, 0.0f, 0.0f, 1.0f});

unsigned int* VAOs = new unsigned int[0];
unsigned int* elements = new unsigned int[0];
unsigned int bufferLength = 0;

cgl_0_1_renderobject* crossGL::_funcs::cgl_0_1_create_render_object(cgl_0_1_objectmash* object, unsigned int type)
{
	if(object == nullptr)
		return nullptr;

	unsigned int count = 0;
	for(unsigned int i = 0; i < object->length; i++)
	{
		cgl_0_1_polygon* polygon = object->content[i];
		if(polygon != nullptr)
		{
			unsigned int count2 = 0;
			for(unsigned int j = 0; j < polygon->length; j++)
				if(polygon->content[j] != nullptr)
					count2++;
			if(count2 > 2)
				count += count2 - 2;
		}
	}

	double* vertexes = new double[9 * count];
	double* colors = new double[12 * count];
	double* texturuvs = new double[6 * count];

	unsigned int count2;
	unsigned int pos = 0;

	double x1 = 0, y1 = 0, z1 = 0, u1 = 0, v1 = 0;
	double x2 = 0, y2 = 0, z2 = 0, u2 = 0, v2 = 0;

	for(unsigned int i = 0; i < object->length; i++)
	{
		count2 = 0;
		cgl_0_1_polygon* polygon = object->content[i];
		for(unsigned int j = 0; j < polygon->length; j++)
		{
			cgl_0_1_vertex* vertex = polygon->content[j];
			if((count2 == 0) && (vertex != nullptr))
			{
				x1 = vertex->x; y1 = vertex->y; z1 = vertex->z; u1 = vertex->u; v1 = vertex->v;
				count2 = 1;
			}
			else if((count2 == 1) && (vertex != nullptr))
			{
				x2 = vertex->x; y2 = vertex->y; z2 = vertex->z; u2 = vertex->u; v2 = vertex->v;
				count2 = 2;
			}
			else if((count2 == 2) && (vertex != nullptr))
			{
				vertexes[pos * 9] = x1; vertexes[pos * 9 + 1] = y1; vertexes[pos * 9 + 2] = z1;
				vertexes[pos * 9 + 3] = x2; vertexes[pos * 9 + 4] = y2; vertexes[pos * 9 + 5] = z2;
				vertexes[pos * 9 + 6] = vertex->x; vertexes[pos * 9 + 7] = vertex->y; vertexes[pos * 9 + 8] = vertex->z;
				colors[pos * 12] = polygon->r; colors[pos * 12 + 1] = polygon->g; colors[pos * 12 + 2] = polygon->b; colors[pos * 12 + 3] = polygon->a;
				colors[pos * 12 + 4] = polygon->r; colors[pos * 12 + 5] = polygon->g; colors[pos * 12 + 6] = polygon->b; colors[pos * 12 + 7] = polygon->a;
				colors[pos * 12 + 8] = polygon->r; colors[pos * 12 + 9] = polygon->g; colors[pos * 12 + 10] = polygon->b; colors[pos * 12 + 11] = polygon->a;
				texturuvs[pos * 6] = u1; texturuvs[pos * 6 + 1] = v1;
				texturuvs[pos * 6 + 2] = u2; texturuvs[pos * 6 + 3] = v2;
				texturuvs[pos * 6 + 4] = vertex->u; texturuvs[pos * 6 + 5] = vertex->v;
				x2 = vertex->x; y2 = vertex->y; z2 = vertex->z; u2 = vertex->u; v2 = vertex->v;
				pos++;
			}
		}
	}

	unsigned int vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, count * 9 * sizeof(double), vertexes, GL_STATIC_DRAW);
	delete[] vertexes;

	unsigned int colorbuffer;
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, count * 12 * sizeof(double), colors, GL_STATIC_DRAW);
	delete[] colors;

	unsigned int texturuvbuffer;
	glGenBuffers(1, &texturuvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, texturuvbuffer);
	glBufferData(GL_ARRAY_BUFFER, count * 6 * sizeof(double), texturuvs, GL_STATIC_DRAW);
	delete[] texturuvs;

    unsigned int vao;
    glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0);
	//glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer[object->object->object_id]);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_DOUBLE,          // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	// second attribute buffer : vertices
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glVertexAttribPointer(
		1,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		4,                  // size
		GL_DOUBLE,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, texturuvbuffer);
	glVertexAttribPointer(
		2,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		2,                  // size
		GL_DOUBLE,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glBindVertexArray(0);

	unsigned int id = bufferLength;
	bufferLength++;
	unsigned int* VAOs2 = new unsigned int[bufferLength];
	unsigned int* elements2 = new unsigned int[bufferLength];
	for(unsigned int i = 0; i < id; i++)
	{
		VAOs2[i] = VAOs[i];
		elements2[i] = elements[i];
	}
	VAOs2[id] = vao;
	elements2[id] = count;
	delete[] VAOs;
	delete[] elements;
	VAOs = VAOs2;
	elements = elements2;

	cgl_0_1_renderobject* back = new cgl_0_1_renderobject(object, type, id);
	return back;
}

void crossGL::_funcs::cgl_0_1_clear()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0, 0, 0, 0);
	glClearDepth(1);
}

void crossGL::_funcs::cgl_0_1_render(cgl_0_1_object* object)
{
    if(object == nullptr)
        return;
	if(object->object == nullptr)
		return;

	glUseProgram(program);

	float x_ = object->roat->x * M_PI / 180;
    float y_ = object->roat->y * M_PI / 180;
    float z_ = object->roat->z * M_PI / 180;

    quat quaternion = quat(vec3(x_, y_, z_));
    mat4 matrix = toMat4(quaternion);
    float* data = value_ptr(matrix);

	float* rotate = new float[9];
	rotate[0] = data[0];
	rotate[1] = data[1];
	rotate[2] = data[2];
	rotate[3] = data[4];
	rotate[4] = data[5];
	rotate[5] = data[6];
	rotate[6] = data[8];
	rotate[7] = data[9];
	rotate[8] = data[10];

	// Draw the triangle !
	glBindVertexArray(VAOs[object->object->object_id]);

    glActiveTexture(GL_TEXTURE0);
    if(object->texture == nullptr)
        glBindTexture(GL_TEXTURE_2D, 0);
    else
        glBindTexture(GL_TEXTURE_2D, object->texture->id);
    glUniform1i(object_texture, 0);

    glUniform4f(object_color, object->r, object->g, object->b, object->a);
    glUniformMatrix3fv(object_roat, 1, false, rotate);
    glUniform3f(object_pos, object->pos->x, object->pos->y, object->pos->z);
    glUniform3f(object_scale, object->scale->x, object->scale->y, object->scale->z);

    glUniform3f(world_pos, world_pos_data[0], world_pos_data[1], world_pos_data[2]);
    glUniformMatrix3fv(world_roat, 1, false, world_roat_shader);
    glUniform3f(world_scale, world_scale_data[0], world_scale_data[1], world_scale_data[2]);

    glUniformMatrix4fv(view_matrix, 1, false, view_matrix_data);

	glDrawArrays(GL_TRIANGLES, 0, elements[object->object->object_id] * 3); // 3 indices starting at 0 -> 1 triangle

	glBindVertexArray(0);

	delete[] rotate;
}

void crossGL::_funcs::cgl_0_1_world_get_pos(double* x, double* y, double* z)
{
    (*x) = world_pos_data[0];
    (*y) = world_pos_data[1];
    (*z) = world_pos_data[2];
}
void crossGL::_funcs::cgl_0_1_world_get_roat(double* x, double* y, double* z)
{
    (*x) = world_roat_data[0];
    (*y) = world_roat_data[1];
    (*z) = world_roat_data[2];
}
void crossGL::_funcs::cgl_0_1_world_get_scale(double* x, double* y, double* z)
{
    (*x) = world_scale_data[0];
    (*y) = world_scale_data[1];
    (*z) = world_scale_data[2];
}

void crossGL::_funcs::cgl_0_1_world_set_pos(double x, double y, double z)
{
    world_pos_data[0] = x;
    world_pos_data[1] = y;
    world_pos_data[2] = z;
}
void crossGL::_funcs::cgl_0_1_world_set_roat(double x, double y, double z)
{
    world_roat_data[0] = x;
    world_roat_data[1] = y;
    world_roat_data[2] = z;

    float x_ = x * M_PI / 180;
    float y_ = y * M_PI / 180;
    float z_ = z * M_PI / 180;

    quat quaternion = quat(vec3(x_, y_, z_));
    mat4 matrix = toMat4(quaternion);
    float* data = value_ptr(matrix);

    world_roat_shader[0] = data[0];
    world_roat_shader[1] = data[1];
    world_roat_shader[2] = data[2];
    world_roat_shader[3] = data[4];
    world_roat_shader[4] = data[5];
    world_roat_shader[5] = data[6];
    world_roat_shader[6] = data[8];
    world_roat_shader[7] = data[9];
    world_roat_shader[8] = data[10];
}
void crossGL::_funcs::cgl_0_1_world_set_scale(double x, double y, double z)
{
    world_scale_data[0] = x;
    world_scale_data[1] = y;
    world_scale_data[2] = z;
}

void crossGL::_funcs::cgl_0_1_view_port(unsigned int x, unsigned int y, unsigned int width, unsigned int height)
{
    glViewport(x, y, width, height);
}

void crossGL::_funcs::cgl_0_1_view_ortho(double x, double y, double front, double back)
{
    view_matrix_data[0] = 1.0f / x;
    view_matrix_data[1] = 0.0f;
    view_matrix_data[2] = 0.0f;
    view_matrix_data[3] = 0.0f;

    view_matrix_data[4] = 0.0f;
    view_matrix_data[5] = 1.0f / y;
    view_matrix_data[6] = 0.0f;
    view_matrix_data[7] = 0.0f;

    view_matrix_data[8] = 0.0f;
    view_matrix_data[9] = 0.0f;
    view_matrix_data[10] = 2.0f / (front - back);
    view_matrix_data[11] = (front + back) / (back - front);

    view_matrix_data[12] = 0.0f;
    view_matrix_data[13] = 0.0f;
    view_matrix_data[14] = 0.0f;
    view_matrix_data[15] = 1.0f;
}

void crossGL::_funcs::cgl_0_1_view_frustum(double x, double y, double front, double back)
{
    view_matrix_data[0] = front / x;
    view_matrix_data[1] = 0.0f;
    view_matrix_data[2] = 0.0f;
    view_matrix_data[3] = 0.0f;

    view_matrix_data[4] = 0.0f;
    view_matrix_data[5] = front / y;
    view_matrix_data[6] = 0.0f;
    view_matrix_data[7] = 0.0f;

    view_matrix_data[8] = 0.0f;
    view_matrix_data[9] = 0.0f;
    view_matrix_data[10] = (front + back) / (front - back);
    view_matrix_data[11] = (2.0f * front * back) / (front - back);

    view_matrix_data[12] = 0.0f;
    view_matrix_data[13] = 0.0f;
    view_matrix_data[14] = -1.0f;
    view_matrix_data[15] = 0.0f;
}

void crossGL::_funcs::cgl_0_1_set_render_program(unsigned int type)
{
    unsigned int id;
    if(type == CGL_0_1_RENDER_FUNC_NORMAL)
        id = 0;
    else if(type == CGL_0_1_RENDER_FUNC_OBJECT_COLOR)
        id = 1;
    else if(type == CGL_0_1_RENDER_FUNC_POLYGON_COLOR)
        id = 2;
    else
        return;

    program = programs[id];

    object_color = programs_object_color[id];
    object_pos = programs_object_pos[id];
    object_roat = programs_object_roat[id];
    object_scale = programs_object_scale[id];
    object_texture = programs_object_texture[id];

    world_pos = programs_world_pos[id];
    world_roat = programs_world_roat[id];
    world_scale = programs_world_scale[id];

    view_matrix = programs_view_matrix[id];
}
