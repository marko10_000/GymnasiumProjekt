#include <GLFW/glfw3.h>
#include <crossGL/_real.h>

using namespace crossGL::_funcs;

extern GLFWwindow* window_glfw;

extern unsigned int window_x;
extern unsigned int window_y;

static void (*event_close_window)() = nullptr;
static void (*event_keys)(int key, unsigned int type) = nullptr;
static void (*event_mouse)(unsigned int x, unsigned int y, unsigned int button, unsigned int action) = nullptr;
static void (*event_mouse_move)(double x, double y) = nullptr;
static void (*event_window_resize)(unsigned int x, unsigned int y) = nullptr;

static void event_linker_close_window(GLFWwindow* window)
{
    if(window != window_glfw)
        return;
    if(event_close_window != nullptr)
        event_close_window();
}

static void event_linker_keys(GLFWwindow* window, int key_code, int system_key, int action, int mods)
{
    if(window != window_glfw)
        return;
    if(event_keys == nullptr)
        return;
    if(key_code == -1)
        return;
    if(action == GLFW_PRESS)
        event_keys(key_code, CGL_0_1_EVENT_KEY_DOWN);
    else if(action == GLFW_RELEASE)
        event_keys(key_code, CGL_0_1_EVENT_KEY_UP);
    else if(action == GLFW_REPEAT)
        event_keys(key_code, CGL_0_1_EVENT_KEY_REPEAT);
}

static void event_linker_mouse(GLFWwindow* window, int button, int action, int mods)
{
    if(window != window_glfw)
        return;
    if(event_mouse == nullptr)
        return;
    double x, y;
    glfwGetCursorPos(window, &x, &y);

    unsigned int action_;
    if(action == GLFW_PRESS)
        action_ = CGL_0_1_EVENT_MOUSE_BUTTON_DOWN;
    else if(action == GLFW_RELEASE)
        action_ = CGL_0_1_EVENT_MOUSE_BUTTON_UP;
    else
        return;

    if(button == GLFW_MOUSE_BUTTON_LEFT)
        event_mouse(x, y, CGL_0_1_EVENT_MOUSE_CLICK_LEFT, action_);
    else if(button == GLFW_MOUSE_BUTTON_RIGHT)
        event_mouse(x, y, CGL_0_1_EVENT_MOUSE_CLICK_RIGHT, action_);
    else if(button == GLFW_MOUSE_BUTTON_MIDDLE)
        event_mouse(x, y, CGL_0_1_EVENT_MOUSE_CLICK_CENTER, action_);
}

static double cursor_last_x = 0;
static double cursor_last_y = 0;

static void event_linker_mouse_move(GLFWwindow* window, double x, double y)
{
    if(window != window_glfw)
        return;
    if(event_mouse_move != nullptr)
        event_mouse_move(x - cursor_last_x, y - cursor_last_y);
    cursor_last_x = x;
    cursor_last_y = y;
}

static void event_linker_window_resize(GLFWwindow* window, int x, int y)
{
    if(window != window_glfw)
        return;
    window_x = x;
    window_y = y;
    if(event_window_resize != nullptr)
        event_window_resize(x, y);
}

void setEvents()
{
    glfwSetWindowCloseCallback(window_glfw, event_linker_close_window);
    glfwSetKeyCallback(window_glfw, event_linker_keys);
    glfwSetMouseButtonCallback(window_glfw, event_linker_mouse);
    glfwSetCursorPosCallback(window_glfw, event_linker_mouse_move);
    glfwSetWindowSizeCallback(window_glfw, event_linker_window_resize);
}

void crossGL::_funcs::cgl_0_1_event_set_close_window(void(*call_back)())
{
    event_close_window = call_back;
}

void crossGL::_funcs::cgl_0_1_event_set_window_resize(void(*call_back)(unsigned int x, unsigned int y))
{
    event_window_resize = call_back;
}

void crossGL::_funcs::cgl_0_1_event_call_close_window()
{
    if(event_close_window != nullptr)
        event_close_window();
}

int crossGL::_funcs::cgl_0_1_event_get_key_code(std::string name)
{
    if(name == "A")
        return GLFW_KEY_A;
    else if(name == "D")
        return GLFW_KEY_D;
    else if(name == "E")
        return GLFW_KEY_E;
    else if(name == "Q")
        return GLFW_KEY_Q;
    else if(name == "R")
        return GLFW_KEY_R;
    else if(name == "S")
        return GLFW_KEY_S;
    else if(name == "W")
        return GLFW_KEY_W;
    else if(name == "SPACE")
        return GLFW_KEY_SPACE;
    else if(name == "F1")
        return GLFW_KEY_F1;
    else if(name == "F2")
        return GLFW_KEY_F2;
    else if(name == "F3")
        return GLFW_KEY_F3;
    else if(name == "F4")
        return GLFW_KEY_F4;
    else if(name == "F5")
        return GLFW_KEY_F5;
    else if(name == "F6")
        return GLFW_KEY_F6;
    else if(name == "F7")
        return GLFW_KEY_F7;
    else if(name == "F8")
        return GLFW_KEY_F8;
    else if(name == "F9")
        return GLFW_KEY_F9;
    else if(name == "F10")
        return GLFW_KEY_F10;
    else if(name == "F11")
        return GLFW_KEY_F11;
    else if(name == "F12")
        return GLFW_KEY_F12;
    else
        return GLFW_KEY_UNKNOWN;
}

void crossGL::_funcs::cgl_0_1_event_set_keys(void(*call_back)(int key, unsigned int action))
{
    event_keys = call_back;
}

void crossGL::_funcs::cgl_0_1_event_call_keys(int key, unsigned int action)
{
    if(event_keys != nullptr)
        event_keys(key, action);
}

void crossGL::_funcs::cgl_0_1_event_set_mouse(void(*call_back)(unsigned int x, unsigned int y, unsigned int button, unsigned int action))
{
    event_mouse = call_back;
}

void crossGL::_funcs::cgl_0_1_event_call_mouse(unsigned int x, unsigned int y, unsigned button, unsigned int action)
{
    if(event_mouse != nullptr)
        event_mouse(x, y, button, action);
}

void crossGL::_funcs::cgl_0_1_event_set_mouse_move(void(*call_back)(double, double))
{
    event_mouse_move = call_back;
}

void crossGL::_funcs::cgl_0_1_event_call_mouse_move(double x, double y)
{
    if(event_mouse_move != nullptr)
        event_mouse_move(x, y);
}

void crossGL::_funcs::cgl_0_1_event_call_window_resize(unsigned int x, unsigned int y)
{
    if(event_window_resize != nullptr)
        event_window_resize(x, y);
}

void crossGL::_funcs::cgl_0_1_get_mouse_pos(unsigned int* x, unsigned int* y)
{
    double x_, y_;
    glfwGetCursorPos(window_glfw, &x_, &y_);
    (*x) = (unsigned int)x_;
    (*y) = (unsigned int)y_;
}
