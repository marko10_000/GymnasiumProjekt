/*
 * screen.cpp
 *
 *  Created on: 12.02.2014
 *      Author: marko
 */

#include<crossGL/_real.h>
#include<GL/glew.h>
#include<GLFW/glfw3.h>
#include<iostream>
#include<vector>
#include<fstream>
#include <freetype2/ft2build.h>
#include <freetype2/freetype/freetype.h>

using namespace std;
using namespace crossGL::_funcs;

GLFWwindow* window_glfw = nullptr;
double last_update = 0;
FT_Library ft_library;

static bool (*windowOpenFunc)(unsigned int width, unsigned int height, std::string title, bool fullscreen) = nullptr;

extern void setEvents();

unsigned int* programs = new unsigned int[0];
unsigned int programs_length = 0;

int* programs_object_color = new int[0];
int* programs_object_pos = new int[0];
int* programs_object_roat = new int[0];
int* programs_object_scale = new int[0];
int* programs_object_texture = new int[0];

int* programs_world_pos = new int[0];
int* programs_world_roat = new int[0];
int* programs_world_scale = new int[0];

int* programs_view_matrix = new int[0];

unsigned int window_x;
unsigned int window_y;

bool checkShaderCompile(unsigned int id)
{
    GLint compiled = 0;
	GLint length = 0;

	glGetShaderiv(id, GL_COMPILE_STATUS, &compiled);

	if(compiled == 0)
	{
        std::cerr << "ERROR: The shader´s compilation fail" << std::endl;
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
        if(length > 1)
        {
            std::string log(length, '\0');
            glGetShaderInfoLog(id, length, &length, &log[0]);
            std::cerr << log << std::endl;
        }
        return false;
	}
    std::cout << "The shader´s compilation was successfully" << std::endl;
    return true;
}

bool checkProgrammCompile(unsigned int id)
{
    GLint compiled = 0;
    GLint length = 0;
    glGetProgramiv(id, GL_LINK_STATUS, &compiled);

    if(compiled == 0)
    {
        std::cerr << "ERROR: linking fail" << std::endl;
        glGetProgramiv(id, GL_INFO_LOG_LENGTH, &length);
        if(length > 1)
        {
            std::string log(length, '\0');
            glGetProgramInfoLog(id, length, &length, &log[0]);
            std::cerr << log << std::endl;
        }
        return false;
    }
    else
	    std::cout << "linkage was successfully" << std::endl;
    return true;
}

int getVarLocation(unsigned int programm, const char* name)
{
    int pos = glGetUniformLocation(programm, name);
    if(pos == -1)
        cerr << "ERROR: Can not find var \"" << name << "\" in the programm." << endl;
    return pos;
}

template<class t>
unsigned int arrayAdd(t*& array, unsigned int length, t add)
{
    t* back = new t[length + 1];
    for(unsigned int i = 0; i < length; i++)
        back[i] = array[i];
    back[length] = add;
    delete[] array;
    array = back;
    return length - 1;
}

bool compileShader_opengl(string vs_file, string fs_file)
{
	unsigned int vertex_shader, fragment_shader;

	string vertex_shader_source;
	ifstream input_stream;
	input_stream.open("shader/" + vs_file, ios::in | ios::binary);
	if(!input_stream.is_open())
	{
		cerr << "Can not load shader/" << vs_file << "." << endl;
		return false;
	}
	char schar = input_stream.get();
	while(!input_stream.eof())
	{
		vertex_shader_source += schar;
		schar = input_stream.get();
	}
	input_stream.close();

	string fragment_shader_source;
	input_stream.open("shader/" + fs_file, ios::in | ios::binary);
	if(!input_stream.is_open())
	{
		cerr << "Can not load shader/" << fs_file << "." << endl;
		return false;
	}
	schar = input_stream.get();
	while(!input_stream.eof())
	{
		fragment_shader_source += schar;
		schar = input_stream.get();
	}
	input_stream.close();

	const char* fragment_shader_source2 = fragment_shader_source.c_str();
	const char* vertex_shader_source2 = vertex_shader_source.c_str();

	// Create and compile vertex shader
	vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_shader_source2, NULL);
	glCompileShader(vertex_shader);
	if(!checkShaderCompile(vertex_shader))
        return false;

	// Create and compile fragment shader
	fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_shader_source2, NULL);
	glCompileShader(fragment_shader);
    if(!checkShaderCompile(fragment_shader))
        return false;

	// Create programm, attach shaders to it, and link it
	unsigned int program = glCreateProgram();
	glAttachShader(program, vertex_shader);
	glAttachShader(program, fragment_shader);
	glLinkProgram(program);
	if(!checkProgrammCompile(program))
        return false;

	// Get Adress pos
	arrayAdd<int>(programs_object_color, programs_length, getVarLocation(program, "objectColor"));
	arrayAdd<int>(programs_object_pos, programs_length, getVarLocation(program, "objectPos"));
	arrayAdd<int>(programs_object_roat, programs_length, getVarLocation(program, "objectRoat"));
	arrayAdd<int>(programs_object_scale, programs_length, getVarLocation(program, "objectScale"));
	arrayAdd<int>(programs_object_texture, programs_length, getVarLocation(program, "objectTexture"));

	arrayAdd<int>(programs_world_pos, programs_length, getVarLocation(program, "worldPos"));
	arrayAdd<int>(programs_world_roat, programs_length, getVarLocation(program, "worldRoat"));
	arrayAdd<int>(programs_world_scale, programs_length, getVarLocation(program, "worldScale"));

	arrayAdd<int>(programs_view_matrix, programs_length, getVarLocation(program, "viewMatrix"));

    arrayAdd<unsigned int>(programs, programs_length, program);

	programs_length++;

	// Delete the shaders as the program has them now
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);

	return true;
}

static bool windowOpenFunc_glfw(unsigned int width, unsigned int height, std::string title, bool fullscreen)
{
	// Try to open window
	if(window_glfw != nullptr)
	{
		cerr << "Window is still opened." << endl;
		return false;
	}
	window_glfw = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
	if(window_glfw == nullptr)
	{
		cerr << "Can not open window." << endl;
		return false;
	}
	glfwMakeContextCurrent(window_glfw);

	// Now init glew
	glewExperimental = true;
	if(glewInit() != GLEW_OK)
	{
		cerr << "Can not init GLEW." << endl;
		return false;
	}

	if(!compileShader_opengl("vertex_shader.txt", "fragment_shader.txt"))
        return false;

    if(!compileShader_opengl("vertex_shader_object_color.txt", "fragment_shader_object_color.txt"))
        return false;

    if(!compileShader_opengl("vertex_shader_polygon_color.txt", "fragment_shader_polygon_color.txt"))
        return false;

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    setEvents();

    window_x = width;
    window_y = height;

	return true;
}

static void glfw_error(int error, const char* description)
{
	cerr << "Error: " << error << " " << description << endl;
}

bool crossGL::_funcs::cgl_0_1_init(unsigned int id)
{
    // Init FT2
    if(FT_Init_FreeType(&ft_library))
    {
        cerr << "Can not init FreeType2." << endl;
        return false;
    }

	last_update = glfwGetTime();
	if((id == CGL_0_1_RENDER_DEFAULT) || (id == CGL_0_1_RENDER_OPENGL) || (id == CGL_0_1_RENDER_OPENGL_3_3))
	{
		// Try to init glfw
		glfwSetErrorCallback(glfw_error);
		if(!glfwInit())
		{
			cerr << "Can not init GLFW." << endl;
			return false;
		}

		// Set context to OpenGL 3.3
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		// Set functions
		windowOpenFunc = windowOpenFunc_glfw;

		// Return
		return true;
	}
	cerr << "Can not find suitable render mode." << endl;
	return false;
}
void crossGL::_funcs::cgl_0_1_terminate()
{
    glfwTerminate();
}

void crossGL::_funcs::cgl_0_1_get_screen_size(unsigned int* x, unsigned int* y)
{
     const GLFWvidmode* vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
     (*x) = vidmode->width;
     (*y) = vidmode->height;
}

bool crossGL::_funcs::cgl_0_1_window_open(unsigned int width, unsigned int height, std::string title, bool fullscreen)
{
	return windowOpenFunc(width, height, title, fullscreen);
}

void crossGL::_funcs::cgl_0_1_window_get_size(unsigned int* x, unsigned int* y)
{
    int _x, _y;
    glfwGetWindowSize(window_glfw, &_x, &_y);
    (*x) = _x;
    (*y) = _y;
}

void crossGL::_funcs::cgl_0_1_window_set_size(unsigned int x, unsigned int y)
{
    window_x = x;
    window_y = y;
    glfwSetWindowSize(window_glfw, x, y);
}

double crossGL::_funcs::cgl_0_1_window_update()
{
	glfwSwapBuffers(window_glfw);
	glfwPollEvents();
	double time = glfwGetTime();
	double take = time - last_update;
	last_update = time;
	return take;
}

void crossGL::_funcs::cgl_0_1_window_close()
{
    glfwDestroyWindow(window_glfw);
}
